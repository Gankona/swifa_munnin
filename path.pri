CONFIG += c++14

isEmpty(path.pri){
    SRC_PATH = $$PWD

    win32 {
        CONFIG(debug,   debug|release): BIN_PATH = $$PWD/../../desktop_debug
        CONFIG(release, debug|release): BIN_PATH = E:/SwifaMunnin
    }
    linux:!android {
        CONFIG(release, debug|release): BIN_PATH = /home/andreyfelenko/Develop/swifa_munnin_app/Release
        CONFIG(debug,   debug|release): BIN_PATH = /home/andreyfelenko/Develop/swifa_munnin_app/Debug
    }
    android {
        CONFIG(debug,   debug|release): BIN_PATH = $$PWD/../../mobile_debug
        CONFIG(release, debug|release): BIN_PATH = $$PWD/../../mobile_release
    }
}
