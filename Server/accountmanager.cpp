#include "accountmanager.h"

AccountManager::AccountManager(QObject *parent) : QObject(parent)
{}

AccountInfo AccountManager::getAccountInfo(QString login)
{
    if (testLoginOrEmail(login)){
        for (auto a : accountList)
            if (a->email == login){
                login = a->login;
                break;
            }
        QDir dir = currentDir;
        if (! dir.cd(login)){
            //send error
            qDebug() << "cannot open directory";
            return AccountInfo{ SM_AccountInfo(), "", SM::Msg::directoryDoNotOpen };
        }
        QFile file(dir.absoluteFilePath(login + ".stat"));
        if (file.open(QIODevice::ReadOnly)){
            //read account info from file
            QDataStream stream(&file);
            SM_AccountInfo account;
            QString password;
            stream >> account >> password;
            file.close();

            return AccountInfo{ account, password, SM::Msg::fileSuccessfullSend };
        }
    }
    return AccountInfo{ SM_AccountInfo(), "", SM::Msg::accountNotPresent };
}

void AccountManager::setAppDir(QDir dir)
{
    currentDir = dir;
    readLoginStat();
}

bool AccountManager::readLoginStat()
{
    for (auto *a : accountList){
        try {
            delete a;
        }
        catch (...){}
    }
    accountList.clear();

    QFile file(currentDir.absoluteFilePath("accountManager.stat"));
    if (file.exists() && file.open(QIODevice::ReadOnly)){
        //read file and decrypt content
        QDataStream stream(&file);

        //set files stats
        int col = 0;
        stream >> col;
        for (int i = 0; i < col; i++){
            QString login;
            QString email;
            QDateTime lastEdit;
            stream >> login >> email >> lastEdit;
            qDebug() << col << " test new " << login << email;
            if (login != "" || email != "")
                accountList.push_back(new AccountPoint{login, email, lastEdit});
        }
        file.close();
        return true;
    }
    return false;
}

bool AccountManager::saveLoginStat()
{
    QFile file(currentDir.absoluteFilePath("accountManager.stat"));
    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate)){
        //collect data to byte array
        QDataStream stream(&file);
        stream << accountList.length();
        for (auto a : accountList)
            stream << a->login << a->email << a->lastEdit;

        //save to file;
        file.close();
        return true;
    }
    return false;
}

QDateTime AccountManager::getLastEditAccount(QString login)
{
    for (auto a : accountList)
        if (a->login == login
                || a->email == login)
            return a->lastEdit;
    return QDateTime();
}

SM::Msg AccountManager::tryAuthorizate(QString login,
                                   QString password)
{
    if (testLoginOrEmail(login)){
        for (auto a : accountList)
            if (a->email == login){
                login = a->login;
                break;
            }
        QDir dir = currentDir;
        if (! dir.cd(login)){
            //send error
            qDebug() << "cannot open directory";
            return SM::Msg::directoryDoNotOpen;
        }
        QFile file(dir.absoluteFilePath(login + ".stat"));
        if (file.open(QIODevice::ReadOnly)){
            //read account info from file
            QDataStream stream(&file);
            SM_AccountInfo account;
            QString passwordFromFile;
            stream >> account >> passwordFromFile;
            file.close();

            //try catch login
            if (passwordFromFile == password)
                return SM::Msg::successfullAuthtorization;
            else
                return SM::Msg::passwordIncorrect;
        }
    }
    return SM::Msg::accountNotPresent;
}

SM::Msg AccountManager::editAccountInfo(SM_AccountInfo account,
                                        QString password)
{
    SM::Msg result = tryAuthorizate(account.login, password);
    if (result == SM::Msg::successfullAuthtorization){
        QDir dir = currentDir;
        dir.cd(account.login);
        QFile file(dir.absoluteFilePath(account.login + ".stat"));
        if (file.open(QIODevice::WriteOnly)){
            QDataStream stream(&file);
            stream << account << password;
            file.close();
            return SM::Msg::accountSuccessfullChange;
        }
        // file not open
        return SM::Msg::fileNotFound;
    }
    return result;
}

SM::Msg AccountManager::registrateNewAccount(SM_AccountInfo account, QString password)
{
    qDebug() << account.login << account.email << password << testLoginOrEmail(account.login)
             << "\n start_accountList";
    for (auto a : accountList)
        qDebug() << a->email << a->login;
    qDebug() << "end_accountList";
    if (testLoginOrEmail(account.login))
        return SM::Msg::loginPresent;
    if (testLoginOrEmail(account.email))
        return SM::Msg::emailPresent;
    QDir dir = currentDir;
    dir.mkdir(account.login);
    dir.cd(account.login);
    QFile file(dir.absoluteFilePath(account.login + ".stat"));
    if (file.open(QIODevice::ReadWrite)){
        QDataStream stream(&file);
        stream << account << password;
        file.close();
        accountList.push_back(new AccountPoint{account.login, account.email, QDateTime::currentDateTime()});
        qDebug() << "saveLoginnstat" << saveLoginStat() << account.login << account.email;
        return SM::Msg::successfullRegistration;
    }
    // file not open
    return SM::Msg::fileNotFound;
}

/*void AccountManager::editPassword(QString lastPassword,
                                  QString newPassword)
{
    if (currentAccount.password != lastPassword){
        //send error
        appLog->setLog(SM::TypeMsg::resultIncorrect, "client::account_manager::edit_password",
                       "password wrong", "settings::edit_account::password");
        return;
    }

    QDir dir = currentDir;
    if (! dir.cd(currentAccount.login)){
        //send error
        appLog->setLog(SM::TypeMsg::warning, "client::account_manager::edit_password",
                       "account directory cannot open");
        return;
    }
    QFile file(dir.absoluteFilePath(currentAccount.login + ".stat"));
    if (file.open(QIODevice::Truncate)){
        //read file and decrypt content
        currentAccount.password = newPassword;
        QByteArray tempByte;
        QDataStream stream(tempByte);
        stream << currentAccount;
        tempByte =  SM_Encryption::encryptFile(tempByte,
                                               currentAccount.login
                                               + "_loginManager");
        stream.setDevice(&file);
        stream << tempByte;
        file.close();
        //send complete message
        appLog->setLog(SM::TypeMsg::resultCorrect, "client::account_manager::edit_password",
                       "password successfull change", "settings::edit_account");
        return;
    }
    //send error
    appLog->setLog(SM::TypeMsg::warning, "client::account_manager::edit_password",
                   "file account cannot open cause " + file.errorString());
}*/

bool AccountManager::testLoginOrEmail(QString login_email)
{
    for (auto a : accountList){
        qDebug() << login_email << a->login << a->email;
        if (a->email == login_email
                || a->login == login_email)
            return true;
    }
    return false;
}

/*void AccountManager::setNewLogin(SM_AccountInfo account,
                                 QString password)
{
    QByteArray tempByte;
    QDataStream stream(&tempByte, QIODevice::ReadWrite);
    AccountInfo *tempAccount = new AccountInfo(account, password);
    stream << *tempAccount;
    delete tempAccount;
    tempByte = SM_Encryption::encryptFile(tempByte,
                                          currentAccount.login
                                          + "_loginManager");
    QDir dir = currentDir;
    if (!dir.cd(account.login)){
        dir.mkdir(account.login);
        if (dir.cd(account.login)){
            // send error
            return;
        }
    }
    QFile file(dir.absoluteFilePath(account.login + ".stat"));
    stream.setDevice(&file);

    // rewrite info
    if (testLocalLoginOrEmail(account.login)){
        for (auto *a : accountList)
            if (a->login == account.login){
                a->email = account.email;
                break;
            }
        if (! file.open(QIODevice::Truncate)){
            // send error
        }
    }
    // set new account info
    else {
        accountList.push_back(new AccountInfoManager(account.email,
                                                     account.login,
                                                     currentAccount.login == account.login));
        if (! file.open(QIODevice::WriteOnly)){
            // send error
        }
    }

    // save info to file
    stream << tempByte;
    file.close();
}*/
