#include <QCoreApplication>
#include "servermanager.h"

int main(int argc, char** argv){
    QCoreApplication app(argc, argv);

    ServerManager *server = new ServerManager;
    server->start();

    return app.exec();
}
