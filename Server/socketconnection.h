#ifndef SOCKETCONNECTION_H
#define SOCKETCONNECTION_H

#include <QtNetwork/QTcpSocket>
#include <QtCore/QDataStream>
#include "sm_namespace.h"
#include "sm_accountinfo.h"
#include "accountmanager.h"

class SocketConnection : public QObject
{
    Q_OBJECT
private:
    QDataStream stream;
    QTcpSocket *socket;

    void readyRead();

public:
    SocketConnection(QTcpSocket *socket, QObject *parent = nullptr);

signals:
    SM::Msg tryAuthorizate(QString login, QString password);
    SM::Msg editAccountInfo(SM_AccountInfo account, QString password);
    bool testLoginOrEmail(QString login_email);
    AccountInfo getAccountInfo(QString login);
    QDateTime getLastEditAccount(QString login);
    SM::Msg registrateNewAccount(SM_AccountInfo account, QString password);
};

#endif // SOCKETCONNECTION_H
