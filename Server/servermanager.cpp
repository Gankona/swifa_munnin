#include "servermanager.h"

ServerManager::ServerManager(QObject *parent)
    : QObject(parent)
{
    clientList.clear();
    accountManager = new AccountManager(this);
    fileManager = new FileManager(this);
    QObject::connect(fileManager, &FileManager::setAppDir, accountManager, &AccountManager::setAppDir);
}

void ServerManager::start()
{
    fileManager->findAppDirectory();
    qDebug() << "server start";
    server = new QTcpServer(this);
    server->listen(QHostAddress("127.0.0.1"), 35553);
    QObject::connect(server, &QTcpServer::newConnection, this, [=](){
        qDebug() << "new login add";
        clientList.push_back(new SocketConnection(server->nextPendingConnection(), this));
        QObject::connect(clientList.last(), &SocketConnection::editAccountInfo,
                         accountManager,      &AccountManager::editAccountInfo);

        QObject::connect(clientList.last(), &SocketConnection::getAccountInfo,
                         accountManager,      &AccountManager::getAccountInfo);

        QObject::connect(clientList.last(), &SocketConnection::testLoginOrEmail,
                         accountManager,      &AccountManager::testLoginOrEmail);

        QObject::connect(clientList.last(), &SocketConnection::tryAuthorizate,
                         accountManager,      &AccountManager::tryAuthorizate);

        QObject::connect(clientList.last(), &SocketConnection::getLastEditAccount,
                         accountManager,      &AccountManager::getLastEditAccount);

        QObject::connect(clientList.last(), &SocketConnection::registrateNewAccount,
                         accountManager,      &AccountManager::registrateNewAccount);
    });
}
