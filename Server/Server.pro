include($PWD/../../path.pri)

DESTDIR = $$BIN_PATH

SOURCES += \
    main.cpp \
    servermanager.cpp \
    socketconnection.cpp \
    accountmanager.cpp \
    filemanager.cpp

QT -= gui
QT += network

CONFIG += c++14

INCLUDEPATH += $$PWD/../SvifaMunninCore
DEPENDPATH += $$PWD/../SvifaMunninCore

LIBS += -L$$BIN_PATH/lib/ -lSvifaMunninCore
INCLUDEPATH += $$SRC_PATH/SvifaMunninCore
DEPENDPATH  += $$SRC_PATH/SvifaMunninCore

HEADERS += \
    servermanager.h \
    socketconnection.h \
    accountmanager.h \
    filemanager.h
