#include "socketconnection.h"

SocketConnection::SocketConnection(QTcpSocket *socket, QObject *parent)
    : QObject(parent),
      socket(socket)
{
    stream.setDevice(socket);
    QObject::connect(this->socket, &QTcpSocket::readyRead, this, &SocketConnection::readyRead);
}

void SocketConnection::readyRead()
{
    QString randKey;
    SM::Key codeKey;
    stream >> randKey >> codeKey;

    switch (codeKey){
    case SM::Key::testLogin:{
        bool mustPresent;
        QString login;
        stream >> mustPresent >> login;
        stream << randKey << static_cast<int>(codeKey);
        if (testLoginOrEmail(login)){
            stream << static_cast<int>(SM::Msg::loginPresent) << mustPresent;
        }
        else if (mustPresent)
            stream << static_cast<int>(SM::Msg::loginNotPresent) << false;
        else
            stream << static_cast<int>(SM::Msg::loginFree) << true;
    } break;

    case SM::Key::testEmail:{
        bool mustPresent;
        QString email;
        stream >> mustPresent >> email;
        stream << randKey << static_cast<int>(codeKey);
        if (testLoginOrEmail(email)){
            stream << static_cast<int>(SM::Msg::emailPresent) << mustPresent;
        }
        else if (mustPresent)
            stream << static_cast<int>(SM::Msg::emailNotPresent) << false;
        else
            stream << static_cast<int>(SM::Msg::emailFree) << true;
    } break;

    case SM::Key::authorization:{
        QString login;
        QString password;
        stream >> login >> password;
        stream << randKey << static_cast<int>(codeKey)
               << static_cast<int>(tryAuthorizate(login, password))
               << login << getLastEditAccount(login);
    } break;

    case SM::Key::setAccountInfo:{
        SM_AccountInfo account;
        QString password;;
        stream >> account >> password;
        stream << randKey << static_cast<int>(codeKey) << static_cast<int>(editAccountInfo(account, password));
    } break;

    case SM::Key::getAccountInfo:{
        QDateTime lastEdit;
        QString login;
        stream >> login >> lastEdit;
        stream << randKey << static_cast<int>(codeKey);
        if (lastEdit < getLastEditAccount(login)){
            AccountInfo accountInfo = getAccountInfo(login);
            if (accountInfo.msg == SM::Msg::fileSuccessfullSend){
                stream << static_cast<int>(accountInfo.msg) << accountInfo.account << accountInfo.password;
            }
            else
                stream << static_cast<int>(accountInfo.msg);
        }
        else
            stream << static_cast<int>(SM::Msg::fileIsNewest);
    } break;

    case SM::Key::recoverPassword:{
        //send mail
        stream << randKey << static_cast<int>(codeKey) << true;
    } break;

    case SM::Key::registration:{
        SM_AccountInfo account;
        QString password;
        stream >> account >> password;
        qDebug() << "\n \n registration server start \n" << account.login << account.email << password;

        SM::Msg msg = registrateNewAccount(account, password);
        if (msg == SM::Msg::successfullRegistration)
            stream << randKey << static_cast<int>(codeKey) << static_cast<int>(msg)
                   << account << password;
        else
            stream << randKey << static_cast<int>(codeKey) << static_cast<int>(msg);
        qDebug() << QString::number(int(msg), 16) << "\n registration end\n \n";
    } break;
    default:;
    }
}
