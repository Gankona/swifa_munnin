#ifndef SERVERMANAGER_H
#define SERVERMANAGER_H

#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QHostAddress>
#include "socketconnection.h"
#include "filemanager.h"
#include "accountmanager.h"

class ServerManager : public QObject
{
    Q_OBJECT
private:
    QTcpServer *server;
    QList <SocketConnection*> clientList;
    AccountManager *accountManager;
    FileManager *fileManager;

public:
    ServerManager(QObject *parent = nullptr);

    void start();
};

#endif // SERVERMANAGER_H
