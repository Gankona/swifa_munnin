#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QObject>
#include <QDir>
#include <QDebug>

class FileManager : public QObject
{
    Q_OBJECT
private:

public:
    FileManager(QObject *parent = 0);

    void findAppDirectory();

signals:
    void setAppDir(QDir appDir);

public slots:
};

#endif // FILEMANAGER_H
