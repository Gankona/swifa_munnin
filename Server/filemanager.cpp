#include "filemanager.h"

FileManager::FileManager(QObject *parent)
    : QObject(parent)
{}

void FileManager::findAppDirectory()
{
    QDir dir = QDir::home();
    if (! dir.cd(".ServerData")){
        dir.mkdir(".ServerData");
        if (! dir.cd(".ServerData")){
            qDebug() << "can`t create Server dir";
            return;
        }
    }
    qDebug() << dir;
    emit setAppDir(dir);
}
