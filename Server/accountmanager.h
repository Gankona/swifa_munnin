#ifndef ACCOUNTMANAGER_H
#define ACCOUNTMANAGER_H

#include <QFile>
#include <QTimer>
#include <QDir>
#include <QDebug>
#include <QBuffer>
#include <memory>
#include "sm_accountinfo.h"
#include "sm_namespace.h"
#include "sm_staticcorefunction.h"
#include <QDebug>
#include <QObject>

struct AccountInfo {
    SM_AccountInfo account;
    QString password;
    SM::Msg msg = SM::Msg::unknown;
};

class AccountManager : public QObject
{
    Q_OBJECT
private:
    struct AccountPoint {
        QString login;
        QString email;
        QDateTime lastEdit;
    };

    QDir currentDir;
    QList <AccountPoint*> accountList;
    bool isLogoutProcess;

    inline bool readLoginStat();
    inline bool saveLoginStat();

public:
    explicit AccountManager(QObject *parent);

    void setAppDir(QDir dir);

signals:

public slots:
    SM::Msg tryAuthorizate(QString login, QString password);
    SM::Msg editAccountInfo(SM_AccountInfo account, QString password);
    SM::Msg registrateNewAccount(SM_AccountInfo account, QString password);
    //void editPassword(QString lastPassword, QString newPassword);
    //void setNewLogin(SM_AccountInfo account, QString password);
    bool testLoginOrEmail(QString login_email);
    AccountInfo getAccountInfo(QString login);
    QDateTime getLastEditAccount(QString login);
};

#endif // ACCOUNTMANAGER_H
