include($PWD/../../../path.pri)

DESTDIR = $$BIN_PATH

CONFIG += c++14
TARGET = "swifa munnin"

QT += core gui network widgets quick quickcontrols2 quickwidgets

LIBS += -L$$BIN_PATH/lib/ -lSvifaMunninCore
INCLUDEPATH += $$SRC_PATH/SvifaMunninCore
DEPENDPATH  += $$SRC_PATH/SvifaMunninCore

LIBS += -L$$BIN_PATH/lib/ -lSvifaMunninWidget
INCLUDEPATH += $$SRC_PATH/SvifaMunninWidget
DEPENDPATH  += $$SRC_PATH/SvifaMunninWidget

SOURCES += \
    View/Widgets/Settings/settingsbasic.cpp \
    View/Widgets/Account/registrationwidget.cpp \
    View/Widgets/Account/accountinfowidget.cpp \
    View/Widgets/Account/loginwidget.cpp \
    View/Widgets/Simple/logwidget.cpp \
    View/Widgets/Simple/homewidget.cpp \
    View/Widgets/Simple/hotkeywidget.cpp \
    View/Widgets/Settings/settingswidget.cpp \
    View/Widgets/Account/accountmanagerwidget.cpp \
    View/Widgets/Simple/uppanelwidget.cpp \
    View/Widgets/Account/recoverpasswordwidget.cpp \
    View/Widgets/Settings/settingsmodule.cpp \
    View/Widgets/Settings/settingsinterface.cpp \
    View/Widgets/Settings/settingsaccount.cpp \
    View/Widgets/Settings/settingsmultimedia.cpp \
    View/Widgets/Settings/settingshotkey.cpp \
    View/Widgets/Settings/settingsbasic.h \
    View/Widgets/Info/infowidget.cpp \
    View/mainwindow.cpp \
    View/tray.cpp \
    Managers/accountinfo.cpp \
    Managers/filestat.cpp \
    Managers/accountinfomanager.cpp \
    Managers/modulemanager.cpp \
    Managers/logmessage.cpp \
    Managers/servermanager.cpp \
    Managers/palettemanager.cpp \
    Managers/logmanager.cpp \
    Managers/accountmanager.cpp \
    Managers/filemanager.cpp \
    Managers/settingsmanager.cpp \
    maincontrol.cpp \
    main.cpp

HEADERS += \
    View/Widgets/Account/accountmanagerwidget.h \
    View/Widgets/Account/accountinfowidget.h \
    View/Widgets/Account/loginwidget.h \
    View/Widgets/Account/recoverpasswordwidget.h \
    View/Widgets/Account/registrationwidget.h \
    View/Widgets/Simple/logwidget.h \
    View/Widgets/Simple/homewidget.h \
    View/Widgets/Simple/hotkeywidget.h \
    View/Widgets/Simple/uppanelwidget.h \
    View/Widgets/Settings/settingswidget.h \
    View/Widgets/Settings/settingsmodule.h \
    View/Widgets/Settings/settingsinterface.h \
    View/Widgets/Settings/settingsaccount.h \
    View/Widgets/Settings/settingsmultimedia.h \
    View/Widgets/Settings/settingshotkey.h \
    View/Widgets/Settings/settingsbasic.h \
    View/Widgets/Info/infowidget.h \
    View/mainwindow.h \
    View/tray.h \
    Managers/accountinfo.h \
    Managers/filestat.h \
    Managers/accountinfomanager.h \
    Managers/modulemanager.h \
    Managers/logmessage.h \
    Managers/palettemanager.h \
    Managers/servermanager.h \
    Managers/filemanager.h \
    Managers/logmanager.h \
    Managers/accountmanager.h \
    Managers/settingsmanager.h \
    maincontrol.h

RESOURCES += \
    resource.qrc
