#include "maincontrol.h"

MainControl::MainControl()
{
    screen = new QSplashScreen(QPixmap("://Images/crow4.png"));
    screen->show();

    accountManager = new AccountManager(this);
    fileManager = new FileManager(this);
    logManager = new LogManager(this);
    serverManager = new ServerManager(this);
    moduleManager = new ModuleManager(this);
    settingsManager = new SettingsManager(this);

    QObject::connect(accountManager, &AccountManager::signalGetAccountInfo,
                     serverManager,   &ServerManager::getAccountInfo);

    QObject::connect(accountManager, &AccountManager::setLoginDataToServer,
                     serverManager,   &ServerManager::setLoginDataToServer);

    QObject::connect(moduleManager,     &ModuleManager::getModuleList,
                     settingsManager, &SettingsManager::getModuleList);

    QObject::connect(moduleManager, &ModuleManager::getDataByte,
                     fileManager,     &FileManager::getDataFromFile);

    QObject::connect(moduleManager, &ModuleManager::setDataByte,
                     fileManager,     &FileManager::setDataToFile);

    QObject::connect(moduleManager, &ModuleManager::getNotificationFiles,
                     fileManager,     &FileManager::getNotificationFiles);

    QObject::connect(serverManager,   &ServerManager::setAuthorizate,
                     accountManager, &AccountManager::setAuthorizate);

    QObject::connect(serverManager,   &ServerManager::editAccountInfo,
                     accountManager, &AccountManager::editAccountInfo);

    QObject::connect(serverManager,   &ServerManager::setNewLogin,
                     accountManager, &AccountManager::setNewLogin);

    QObject::connect(accountManager, &AccountManager::isConnectedToServer,
                     serverManager,   &ServerManager::isConnected);

    QObject::connect(fileManager, &FileManager::setAppDir, this, [=](QDir dir){
        logManager->readLog(dir);
        accountManager->setAppDir(dir);
        settingsManager->setAppDir(dir);
    });
}

void MainControl::startApp()
{
    fileManager->findAppDirectory();
    //create view
    tray = new Tray;
    mainWindow = new MainWindow;
    //account
    accountManager->firstRead();
    mainWindow->show();
    screen->finish(mainWindow);

    QObject::connect(mainWindow,         &MainWindow::getAccountInfo,
                     accountManager, &AccountManager::getAccountInfo);

    screen->finish(mainWindow);
}
