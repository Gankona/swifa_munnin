#ifndef ACCOUNTINFO_H
#define ACCOUNTINFO_H

#include "sm_accountinfo.h"
#include <QDataStream>

class AccountInfo : public SM_AccountInfo
{
public:
    AccountInfo();
    AccountInfo(const AccountInfo &a);
    AccountInfo(const SM_AccountInfo &a,
                const QString password,
                QDateTime lastEdit = QDateTime::currentDateTime());

    QDateTime lastEdit;
    QString password;

    operator SM_AccountInfo();

    friend QDataStream& operator << (QDataStream &s, AccountInfo &a);
    friend QDataStream& operator >> (QDataStream &s, AccountInfo &a);
};

#endif // ACCOUNTINFO_H
