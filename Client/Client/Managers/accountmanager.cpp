#include "accountmanager.h"

AccountManager::AccountManager(QObject *parent) : QObject(parent)
{
    QObject::connect(appBind, &SM_BindManager::signalInternal, this, [=]
                    (QString sendKey,
                     QString message,
                     QString deliveryKey){
        if (SM_CF::getTier(sendKey) == "account"){
            if (SM_CF::getTier(sendKey, 2) == "must_present"){
                if (SM_CF::getTier(sendKey, 1) == "test_login"
                        || SM_CF::getTier(sendKey, 1) == "test_email"){
                    QString keyString;
                    QString msgKey;
                    SM_CF::getTier(sendKey, 1) == "test_email"
                            ? keyString = "email", msgKey = "email"
                            : keyString = "login", msgKey = "login";
                    if (isConnectedToServer())
                        appBind->setSignal(SM_CF::replaceTier(sendKey, "server"),
                                           SM_CF::unite({"client", "account_manager", "app_log"}),
                                           message,
                                           SM_CF::unite({deliveryKey, keyString}));
                    else {
                        bool isCorrect = testLocalLoginOrEmail(message);
                        QString msg;
                        isCorrect ? msg = msgKey + " correct" : msg = msgKey + " not present";
                        appBind->setResult(isCorrect,
                                            SM_CF::unite({deliveryKey, keyString}),
                                            SM_CF::unite({"client", "account_manager", "app_log"}),
                                            msg);
                    }
                }
            }

            else if (SM_CF::getTier(sendKey, 1) == "try_authorizate"){
                QString login = SM_CF::getTier(message);
                QString password = SM_CF::getTier(message, 1);
                if (login != "NoName" && password == ""){
                    //send error
                    appBind->setError(SM_CF::unite({"widget", "account_answer"}),
                                      SM_CF::unite({"client", "account_manager", "try_authorizate"}),
                                      "password not enter");
                    return;
                }
                if (isConnectedToServer() && ! isLogoutProcess){
                    appBind->setSignal(SM_CF::unite({sendKey, "server"}),
                                       SM_CF::unite({"client", "account_manager", "catch_log"}),
                                       message, deliveryKey);
                }
                else {
                    tryAuthorizate(SM_CF::getTier(message, 0),
                                   SM_CF::getTier(message, 1));
                }
            }

            else if (SM_CF::getTier(sendKey, 1) == "set_logout"){
                this->logout();
            }

            else if (SM_CF::getTier(sendKey, 1) == "set_authorizate"){
                setAuthorizate(message);
            }
            else if (SM_CF::getTier(sendKey, 1) == "get_current_account"){
                if (SM_CF::getTier(sendKey, 2) == "full")
                    appBind->setAright(deliveryKey,
                                       SM_CF::unite({"client", "account_manager", "catch_log"}),
                                       SM_CF::unite({currentAccount.login,
                                                    currentAccount.firstName,
                                                    currentAccount.lastName,
                                                    currentAccount.email,
                                                    currentAccount.country,
                                                    currentAccount.telephon}));
                else
                    appBind->setAright(deliveryKey,
                                       SM_CF::unite({"client", "account_manager", "catch_log"}),
                                       currentAccount.login);
            }
        }
    });
}

void AccountManager::firstRead()
{
    if (!readLoginStat() || ! testLocalLoginOrEmail("NoName")){
        //create NoName
        appBind->setMessage(SM::TypeMsg::warning,
                            SM_CF::unite({"client", "account_manager", "first_read"}),
                            "NoName not present");
        setNewLogin(SM_AccountInfo("NoName", "", "", "", "", "universe"), "");
        saveLoginStat();
        firstRead();
        return;
    }
    if (! testLocalLoginOrEmail("NoName")){
        appBind->setMessage(SM::TypeMsg::critical,
                            SM_CF::unite({"client", "account_manager", "first_read"}),
                            "NoName cannot create");
        return;
    }
    for (auto a : accountList)
        if (a->isAuthorizate){
            setAuthorizate(a->login);
            return;
        }
    logout();
}

SM_AccountInfo AccountManager::getAccountInfo()
{
    return currentAccount;
}

void AccountManager::setAppDir(QDir dir)
{
    currentDir = dir;
}

bool AccountManager::readLoginStat()
{
    QFile file(currentDir.absoluteFilePath("accountManager.stat"));
    if (file.open(QIODevice::ReadOnly)){
        QDataStream stream(&file);
        for (auto *a : accountList){
            try {
                delete a;
            }
            catch (...){}
        }
        accountList.clear();

        while ( !stream.atEnd()){
            accountList.push_back(new AccountInfoManager);
            stream >> *accountList.last();
            qDebug() << "read " << accountList.last()->login << accountList.last()->email << accountList.last()->isAuthorizate;
        }
        file.close();
        appBind->setMessage(SM::TypeMsg::info,
                            SM_CF::unite({"client", "account_manager", "read_login_stat"}),
                            "login stat successfull read");
        return true;
    }
    appBind->setMessage(SM::TypeMsg::warning,
                        SM_CF::unite({"client", "account_manager", "read_login_stat"}),
                        "login stat not read cause " + file.errorString());
    return false;
}

bool AccountManager::saveLoginStat()
{
    QFile file(currentDir.absoluteFilePath("accountManager.stat"));
    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate)){
        QDataStream stream(&file);
        for (auto a : accountList){
            stream << *a;
            qDebug() << "save " << a->email << a->login << a->isAuthorizate;
        }
        file.close();
        appBind->setMessage(SM::TypeMsg::info,
                            SM_CF::unite({"client", "account_manager", "save_login_stat"}),
                            "login stat successfull save");
        return true;
    }
    appBind->setMessage(SM::TypeMsg::warning,
                        SM_CF::unite({"client", "account_manager", "save_login_stat"}),
                        "login stat not save cause " + file.errorString());
    return false;
}

void AccountManager::logout()
{
    appBind->setSignal(SM_CF::unite({"account", "logout"}),
                       SM_CF::unite({"client", "account_manager", "logout"}),
                       "logout from " + currentAccount.login);
    isLogoutProcess = true;
    tryAuthorizate("NoName", "");
    isLogoutProcess = false;
}

void AccountManager::setAuthorizate(QString login)
{
    QDir dir = currentDir;
    if (! dir.cd(login)){
        //send error
        appBind->setMessage(SM::TypeMsg::warning,
                            SM_CF::unite({"client", "account_manager", "set_authorizate"}),
                            "cannot open account directory");
        appBind->setSignal(SM_CF::unite({"server", "get_account_info"}),
                           SM_CF::unite({"client", "account_manager", "set_authorizate"}),
                           login,
                           SM_CF::unite({"account", "set_authorizate"}));
        return;
    }
    QFile file(dir.absoluteFilePath(login + ".stat"));
    if (file.open(QIODevice::ReadOnly)){
        //read account info from file
        QDataStream stream(&file);
        AccountInfo accountInfo;
        stream >> accountInfo;

        //try catch login
        emit setLogin(login);
        for (auto a : accountList)
            a->isAuthorizate = a->login == login;
        saveLoginStat();
        currentAccount = accountInfo;
        //send good result
        appBind->setAright(SM_CF::unite({"widget", "show_info"}),
                           SM_CF::unite({"client", "account_manager", "set_authorizate"}),
                           "successfull authorizate");
        appBind->setSignal(SM_CF::unite({"account", "login"}),
                           SM_CF::unite({"client", "account_manager", "set_authorizate"}),
                           SM_CF::unite({currentAccount.login, currentAccount.firstName}));
    }
    else {
        appBind->setMessage(SM::TypeMsg::warning,
                            SM_CF::unite({"client", "account_manager:", "set_authorizate"}),
                            "account " + login + "-file not exist");
        appBind->setSignal(SM_CF::unite({"server", "get_account_info"}),
                           SM_CF::unite({"client", "account_manager", "set_authorizate"}),
                           login,
                           SM_CF::unite({"account", "set_authorizate"}));
    }
}

void AccountManager::tryAuthorizate(QString login,
                                   QString password)
{
    if (testLocalLoginOrEmail(login)){
        for (auto a : accountList)
            if (a->email == login){
                login = a->login;
                break;
            }
        QDir dir = currentDir;
        if (! dir.cd(login)){
            //send error
            appBind->setMessage(SM::TypeMsg::warning,
                                SM_CF::unite({"client", "account_manager", "try_authorizate"}),
                                "cannot open account directory");
            return;
        }
        QFile file(dir.absoluteFilePath(login + ".stat"));
        if (file.open(QIODevice::ReadOnly)){
            //read account info from file
            QDataStream stream(&file);
            AccountInfo accountInfo;
            stream >> accountInfo;
            file.close();

            //try catch login
            if (accountInfo.password == password){
                emit setLogin(login);
                for (auto a : accountList)
                    a->isAuthorizate = a->login == login;
                saveLoginStat();
                currentAccount = accountInfo;
                //send good result
                appBind->setAright(SM_CF::unite({"widget", "show_info"}),
                                   SM_CF::unite({"client", "account_manager", "try_authorizate"}),
                                   "successfull authorizate");
                appBind->setSignal(SM_CF::unite({"account", "login"}),
                                   SM_CF::unite({"client", "account_manager", "try_authorizate"}),
                                   SM_CF::unite({currentAccount.login, currentAccount.firstName}));
            }
            else {
                //send error avtorizate
                //password wrong
                appBind->setError(SM_CF::unite({"widget", "show_info"}),
                                  SM_CF::unite({"client", "account_manager", "try_authorizate"}),
                                  "password wrong");
            }

            //send result ?
            return;
        }
        //send error
        appBind->setSignal(SM_CF::unite({"widget", "show_info"}),
                           SM_CF::unite({"client", "account_manager", "try_authorizate"}),
                           "account info file deleted, connect to server and try again");
    }
    else {}
}

void AccountManager::editAccountInfo(SM_AccountInfo account,
                                     QString password)
{
    if (currentAccount.password == password){
        QDir dir = currentDir;
        dir.cd(currentAccount.login);
        QFile file(dir.absoluteFilePath(currentAccount.login + ".stat"));
        if (file.open(QIODevice::WriteOnly | QIODevice::Truncate)){
            QDataStream stream(&file);
            stream << new AccountInfo(account, password);
            file.close();
            emit setLoginDataToServer(account, password);
            appBind->setAright(SM_CF::unite({"settings", "edit_account"}),
                               SM_CF::unite({"client", "account_manager", "edit_account_info"}),
                               "successfull edit account");
            return;
        }
        // file not open
        appBind->setError(SM_CF::unite({"settings", "edit_account", "password"}),
                          SM_CF::unite({"client", "account_manager", "edit_account_info"}),
                          QString("account file " + account.login + " not open cause " + file.errorString()));
    }
    else
        // password uncorrect
        appBind->setError(SM_CF::unite({"settings", "edit_account", "password"}),
                          SM_CF::unite({"client", "account_manager", "edit_account_info"}),
                          "password wrong");
}

void AccountManager::editPassword(QString lastPassword,
                                  QString newPassword)
{
    if (currentAccount.password != lastPassword){
        //send error
        appBind->setError(SM_CF::unite({"settings", "edit_account", "password"}),
                          SM_CF::unite({"client", "account_manager", "edit_password"}),
                          "password wrong");
        return;
    }

    QDir dir = currentDir;
    if (! dir.cd(currentAccount.login)){
        //send error
        appBind->setMessage(SM::TypeMsg::warning,
                            SM_CF::unite({"client", "account_manager", "edit_password"}),
                            "account directory cannot open");
        return;
    }
    QFile file(dir.absoluteFilePath(currentAccount.login + ".stat"));
    if (file.open(QIODevice::Truncate)){
        //read file and decrypt content
        currentAccount.password = newPassword;
        QDataStream stream(&file);
        stream << currentAccount;
        file.close();
        //send complete message
        appBind->setAright(SM_CF::unite({"settings", "edit_account"}),
                           SM_CF::unite({"client", "account_manager", "edit_password"}),
                           "password successfull change");
        return;
    }
    //send error
    appBind->setMessage(SM::TypeMsg::warning,
                        SM_CF::unite({"client", "account_manager", "edit_password"}),
                        "file account cannot open cause " + file.errorString());
}

bool AccountManager::testLocalLoginOrEmail(QString login_email)
{
    for (auto a : accountList)
        if (a->email == login_email
                || a->login == login_email)
            return true;
    return false;
}

void AccountManager::setNewLogin(SM_AccountInfo account,
                                 QString password)
{
    QDir dir = currentDir;
    if (!dir.cd(account.login)){
        dir.mkdir(account.login);
        if (dir.cd(account.login)){
            // send error
            return;
        }
    }
    QFile file(dir.absoluteFilePath(account.login + ".stat"));
    QDataStream stream(&file);

    // rewrite info
    if (testLocalLoginOrEmail(account.login)){
        for (auto *a : accountList)
            if (a->login == account.login){
                a->email = account.email;
                break;
            }
        if (! file.open(QIODevice::WriteOnly | QIODevice::Truncate)){
            // send error
            appBind->setMessage(SM::TypeMsg::warning,
                                SM_CF::unite({"client", "account_manager", "set_new_login"}),
                                "file present but not open to rewrite");
            return;
        }
    }
    // set new account info
    else {
        accountList.push_back(new AccountInfoManager(account.email,
                                                     account.login,
                                                     currentAccount.login == account.login));
        saveLoginStat();
        if (! file.open(QIODevice::WriteOnly)){
            // send error
            appBind->setMessage(SM::TypeMsg::warning,
                                SM_CF::unite({"client", "account_manager", "set_new_login"}),
                                "can`t create new file");
            return;
        }
    }

    // save info to file
    AccountInfo *tempAccount = new AccountInfo(account, password);
    stream << *tempAccount;
    delete tempAccount;
    file.close();
    appBind->setMessage(SM::TypeMsg::info,
                        SM_CF::unite({"client", "account_manager", "set_new_login"}),
                        "file successfull create");
}

void AccountManager::deleteAccountFromLocal(QString password)
{
    if (currentAccount.password != password){
        //send error
        appBind->setError(SM_CF::unite({"settings", "edit_account"}),
                          SM_CF::unite({"client", "account_manager", "deleteAccountFromLocal"}),
                          "password wrong");
        return;
    }
    currentDir.rmdir(currentAccount.login);
    for (auto *a : accountList)
        if (a->login == currentAccount.login)
            accountList.removeOne(a);
    saveLoginStat();
    logout();
}
