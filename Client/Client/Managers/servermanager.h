#ifndef SERVERMANAGER_H
#define SERVERMANAGER_H

#include "ApplicationBind"
#include "sm_accountinfo.h"
#include "sm_staticcorefunction.h"
#include "sm_namespace.h"
#include <QSettings>
#include <QDir>
#include <QTimer>
#include <QTimer>
#include <QDataStream>
#include <QtNetwork/QTcpSocket>
#include <QDebug>

class ServerManager : public QObject
{
    Q_OBJECT
private:
    QDataStream stream;
    QTcpSocket *socket;
    QSettings setting;
    bool isConnectToServer;

    struct RequestQueue {
        QString deliveryKey;
        QDateTime time;
    };
    QTimer *queueTimer;
    QTimer *connectTimer;

    QMap <QString, RequestQueue> queue;
    inline void updateServerConnected(QString host, int port);
    void catchSignal(QString sendKey, QString message, QString deliveryKey);

public:
    ServerManager(QObject* parent = nullptr);

signals:
    void setServerConnectStatus(bool isConnect);
    void setAuthorizate(QString login);
    void editAccountInfo(SM_AccountInfo account, QString password);
    void setNewLogin(SM_AccountInfo account, QString password);

private slots:
    //server
    void readyRead();

public slots:
    bool isConnected();
    void getAccountInfo(QString login, QDateTime time);
    void setLoginDataToServer(SM_AccountInfo account, QString password);
};

#endif // SERVERMANAGER_H
