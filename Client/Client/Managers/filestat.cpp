#include "filestat.h"

FileStat::FileStat(QString title,
                   QString fileName,
                   QString module,
                   QStringList tags,
                   bool isDate,
                   bool isEncrypt,
                   QDateTime createTme,
                   QDateTime lastEditTime)
    : title(title),
      fileName(fileName),
      module(module),
      tags(tags),
      isDate(isDate),
      isEncrypt(isEncrypt),
      createTime(createTme),
      lastEditTime(lastEditTime)
{}

FileStat::FileStat(QString title,
                   QString fileName,
                   QString module,
                   bool isDate,
                   bool isEncrypt,
                   QDateTime createTime,
                   QDateTime lastEditTime)
    : FileStat(title,
               fileName,
               module,
               QStringList{},
               isDate,
               isEncrypt,
               createTime,
               lastEditTime)
{}

QDataStream& operator << (QDataStream &d, FileStat &f)
{
    d << f.createTime << f.fileName << f.isDate << f.isEncrypt
      << f.lastEditTime << f.module << f.tags << f.title;
    return d;
}

QDataStream& operator >> (QDataStream &d, FileStat &f)
{
    d >> f.createTime >> f.fileName >> f.isDate >> f.isEncrypt
      >> f.lastEditTime >> f.module >> f.tags >> f.title;
    return d;
}
