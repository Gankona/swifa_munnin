#ifndef PALETTEMANAGER_H
#define PALETTEMANAGER_H

#include <QObject>

#include "sm_apppalette.h"

class PaletteManager : public SM_AppPalette
{
    Q_OBJECT
public:
    explicit PaletteManager(QObject *parent = 0);
};

#endif // PALETTEMANAGER_H
