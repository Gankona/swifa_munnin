#ifndef MODULEMANAGER_H
#define MODULEMANAGER_H

#include "ApplicationBind"
#include <QPluginLoader>
#include <QObject>
#include <QDir>
#include <QString>

#include "sm_namespace.h"
#include "sm_module.h"
#include "sm_staticcorefunction.h"

class ModuleManager : public QObject
{
    Q_OBJECT
private:
    QStringList directoryModuleList;
    QMap <QString, SM::Module> accountModuleMap;
    QMap <QString, SM_Module*> moduleLoadList;

    SM::Module loadModule(QString moduleName);
    void testModulePresent();

public:
    explicit ModuleManager(QObject *parent = 0);

    void reloadModules();
    const QMap<QString, SM::Module> getModuleMap();

signals:
    QStringList getModuleList();

    //from modules -- start
    bool setDataByte(QString title,
                     QByteArray byteData,
                     QString module,
                     QString keyEncrypt,
                     QString newTitle,
                     bool isDate,
                     QStringList tags);

    bool getDataByte(QString titles,
                     QString module,
                     QString keyEncrypt = "");

    void changeStatus(SM::Module status,
                      QString module);
    void createWidget(QWidget *widget,
                      QString module);
    QStringList getNotificationFiles(QString moduleName);
    void setIcon(QIcon icon, QString moduleName);
    //from modules -- end
};

#endif // MODULEMANAGER_H
