#include "logmessage.h"

LogMessage::LogMessage(SM::TypeMsg type,
                       QString message,
                       QString sendKey,
                       QString deliveryKey,
                       QString senderModuleName,
                       QString receiverModuleName,
                       QDateTime time)
                : type(type),
                  message(message),
                  sendKey(sendKey),
                  deliveryKey(deliveryKey),
                  senderModuleName(senderModuleName),
                  receiverModuleName(receiverModuleName),
                  time(time){}

LogMessage::LogMessage(const LogMessage &m)
    : LogMessage(m.type,
                 m.message,
                 m.sendKey,
                 m.deliveryKey,
                 m.senderModuleName,
                 m.receiverModuleName,
                 m.time){}

QDataStream& operator << (QDataStream &s, LogMessage &m)
{
    s << m.time << m.type << m.message << m.sendKey << m.deliveryKey
      << m.senderModuleName << m.receiverModuleName;
    return s;
}

QDataStream& operator >> (QDataStream &s, LogMessage &m)
{
    s >> m.time >> m.type >> m.message >> m.sendKey >> m.deliveryKey
      >> m.senderModuleName >> m.receiverModuleName;
    return s;
}
