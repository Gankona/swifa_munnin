#ifndef SETTINGS_H
#define SETTINGS_H

#include <QtCore/QObject>
#include <QFile>
#include <QDataStream>
#include <QMap>
#include <QDir>
#include <QSize>
#include <QVariant>
#include "ApplicationBind"
#include "ApplicationSettings"

class SettingsManager : public QObject
{
    Q_OBJECT
private:
    QDir currentDir;
    QString currentLogin;
    QStringList moduleList;
    bool isWaitFileFromServer;

    void setLogin(QString login);
    void readSetting();
    void writeSetting();

    void setDefaultSettings(QString login);

public:
    SettingsManager(QObject *parent = nullptr);

    QStringList getModuleList();
    void setAppDir(QDir dir);
    void setSettingFromServer(QByteArray &b);
};

#endif // SETTINGS_H
