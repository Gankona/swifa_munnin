#ifndef ACCOUNTMANAGER_H
#define ACCOUNTMANAGER_H

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QObject>
#include <QtCore/QTimer>

#include "sm_accountinfo.h"
#include "sm_staticcorefunction.h"

#include "accountinfo.h"
#include "accountinfomanager.h"
#include "ApplicationBind"

class AccountManager : public QObject
{
    Q_OBJECT
private:
    QDir currentDir;
    AccountInfo currentAccount;
    QList <AccountInfoManager*> accountList;
    bool isLogoutProcess;

    inline bool readLoginStat();
    inline bool saveLoginStat();

public:
    AccountManager(QObject *parent = nullptr);

    void setAppDir(QDir dir);
    void firstRead();

signals:
    //local signal
    void setLogin(QString login);
    //server signal
    void signalGetAccountInfo(QString login, QDateTime time);
    void setLoginDataToServer(SM_AccountInfo account, QString password);
    bool isConnectedToServer();

public slots:
    void logout();
    void tryAuthorizate(QString login, QString password);
    void setAuthorizate(QString login);
    void editAccountInfo(SM_AccountInfo account, QString password);
    void editPassword(QString lastPassword, QString newPassword);

    bool testLocalLoginOrEmail(QString login_email);
    void setNewLogin(SM_AccountInfo account, QString password);
    void deleteAccountFromLocal(QString password);
    SM_AccountInfo getAccountInfo();
};

#endif // ACCOUNTMANAGER_H
