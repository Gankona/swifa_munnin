#include "servermanager.h"

ServerManager::ServerManager(QObject *parent)
    : QObject(parent),
      setting(),
      isConnectToServer(false)
{
    socket = new QTcpSocket(this);
    queueTimer = new QTimer(this);
    connectTimer = new QTimer(this);
    connectTimer->start(5000);
    updateServerConnected("127.0.0.1", 35553);

    QObject::connect(socket, &QAbstractSocket::connected, this, [=](){
        stream.setDevice(socket);
        isConnectToServer = true;
        emit setServerConnectStatus(true);
        queue.clear();
        queueTimer->start(2500);
    });

    QObject::connect(connectTimer, &QTimer::timeout, this, [=](){
        if (! isConnectToServer)
            updateServerConnected("127.0.0.1", 35553);
    });

//    QObject::connect(queueTimer, &QTimer::timeout, this, [=](){
//        for (auto t : queue.keys())
//            if (QDateTime::currentDateTime().secsTo(queue.value(t).time) >= 10){
//                appLog->setLog(SM::TypeMsg::resultIncorrect, "client::server_manager::timer",
//                               "problem with connected to server", queue.value(t).deliveryKey);
//                queue.remove(t);
//            }
//    });

    QObject::connect(socket,  &QTcpSocket::readyRead,
                     this, &ServerManager::readyRead);

    QObject::connect(appBind, &SM_BindManager::signalInternal,
                     this,     &ServerManager::catchSignal);
}

void ServerManager::catchSignal(QString sendKey,
                                QString message,
                                QString deliveryKey)
{
    QString randKey = SM_CF::getRandomKey(6);

    if (SM_CF::getTier(sendKey) == "server"){
        if (isConnectToServer)
            queue.insert(randKey, RequestQueue{deliveryKey, QDateTime::currentDateTime()});
        else {
            appBind->setResult(false,
                               deliveryKey,
                               SM_CF::unite({"client", "server_manager", sendKey}),
                               "action is not available, as there is no communication with the server");
            return;
        }
    }
    else return;

    //send query to stream
    //
    if (SM_CF::getTier(sendKey) == "test_login"
        && SM_CF::getTier(sendKey, 2) == "server"){
        bool mustPresent = SM_CF::getTier(sendKey, 1) == "mustPresent";
        stream << randKey << int(SM::Key::testLogin) << mustPresent << message;
    }
    //
    else if (SM_CF::getTier(sendKey) == "test_email"
             && SM_CF::getTier(sendKey, 2) == "server"){
        bool mustPresent = SM_CF::getTier(sendKey, 1) == "mustPresent";
        stream << randKey << int(SM::Key::testEmail) << mustPresent << message;
    }
    //
    else if (sendKey == "try_registrate"){
        SM_AccountInfo account = SM_AccountInfo::getAccountFromTier(message);
        stream << randKey << int(SM::Key::registration)
               << account
               << SM_CF::getTier(message, 6);
    }
    //
    else if (SM_CF::getTier(sendKey) == "try_authorizate"
             && SM_CF::getTier(sendKey, 1) == "server"){
        QString randKey = SM_CF::getRandomKey(6);
        queue.insert(randKey, RequestQueue{ deliveryKey, QDateTime::currentDateTime() });
        stream << randKey << int(SM::Key::authorization) << SM_CF::getTier(message)
               << SM_CF::getTier(message, 1);
    }
    //
    else if (sendKey == "send_recover_password"){
        stream << randKey << int(SM::Key::recoverPassword) << message;
    }
    else if (sendKey == "get_account_info"){
        queue.insert(randKey, RequestQueue{ deliveryKey, QDateTime::currentDateTime() });
        stream << randKey << static_cast<int>(SM::Key::getAccountInfo) << message << QDateTime();
    }
    else if (sendKey == "get_settings"){
        queue.insert(randKey, RequestQueue{ deliveryKey, QDateTime::currentDateTime() });
        stream << randKey << static_cast<int>(SM::Key::getSettings) << message << QDateTime();
    }
}

void ServerManager::updateServerConnected(QString host, int port)
{
    socket->connectToHost(host, port);
}

void ServerManager::readyRead(){
    QString randKey;
    SM::Key codeKey;
    stream >> randKey >> codeKey;
    switch (codeKey){
    case SM::Key::testLogin:
    case SM::Key::testEmail:{
        SM::Msg msg;
        bool isCorrect;
        stream >> msg >> isCorrect;
        appBind->setResult(isCorrect,
                           queue.value(randKey).deliveryKey,
                           SM_CF::unite({"server", "ready_read", "test_email"}),
                           SM::getMsgToString(msg));
    } break;

    case SM::Key::authorization:{
        SM::Msg msg;
        QString login;
        QDateTime lastEditTime;
        stream >> msg >> login >> lastEditTime;
        //проверяем на локале нужно ли обновить файл аккаунта
        appBind->setResult(msg == SM::Msg::successfullAuthtorization,
                           queue.value(randKey).deliveryKey,
                           SM_CF::unite({"server", "ready_read", "authorizate"}),
                           SM::getMsgToString(msg));
        if (msg == SM::Msg::successfullAuthtorization){
            setAuthorizate(login);
        }
    } break;

    case SM::Key::setAccountInfo:{
        SM::Msg msg;
        stream >> msg;
        appBind->setResult(msg == SM::Msg::accountSuccessfullChange,
                           queue.value(randKey).deliveryKey,
                           SM_CF::unite({"server", "ready_read", "set_account_info"}),
                           SM::getMsgToString(msg));
    } break;

    case SM::Key::getAccountInfo:{
        SM::Msg msg;
        stream >> msg;
        if (msg == SM::Msg::fileSuccessfullSend){
            SM_AccountInfo account;
            QString password;
            stream >> account >> password;
            emit setNewLogin(account, password);
            if (queue.value(randKey).deliveryKey == "set_authorizate")
                appBind->setSignal(queue.value(randKey).deliveryKey,
                                   SM_CF::unite({"server", "ready_read", "get_account_info"}),
                                   account.login);
            else
                appBind->setResult(true,
                                   queue.value(randKey).deliveryKey,
                                   SM_CF::unite({"server", "ready_read", "get_account_info"}),
                                   SM::getMsgToString(msg));
        }
        else
            appBind->setResult(false,
                               queue.value(randKey).deliveryKey,
                               SM_CF::unite({"server", "ready_read", "get_account_info"}),
                               SM::getMsgToString(msg));
    } break;


    case SM::Key::recoverPassword:{
        bool isSend;
        stream >> isSend;
        appBind->setResult(true,
                           queue.value(randKey).deliveryKey,
                           SM_CF::unite({"server", "ready_read", "recover_password"}));
    } break;

    case SM::Key::registration:{
        SM::Msg msg;
        stream >> msg;
        if (msg == SM::Msg::successfullRegistration){
            SM_AccountInfo account;
            QString password;
            stream >> account >> password;
            editAccountInfo(account, password);
        }
        appBind->setResult(msg == SM::Msg::successfullRegistration,
                           queue.value(randKey).deliveryKey,
                           SM_CF::unite({"server", "ready_read", "registrate"}),
                           SM::getMsgToString(msg));
    } break;
    default:;
    }
    queue.remove(randKey);
}

bool ServerManager::isConnected()
{
    return isConnectToServer;
}

void ServerManager::getAccountInfo(QString login, QDateTime time)
{
    if (isConnectToServer){
        QString randKey = SM_CF::getRandomKey(6);
        queue.insert(randKey, RequestQueue{ "log", QDateTime::currentDateTime() });
        stream << int(SM::Key::getAccountInfo) << login << time;
    }
    else
        appBind->setResult(false,
                           "", //тут что то я вно нужно написать вместо пустого
                           SM_CF::unite({"client", "server_manager", "get_accoun_info"}),
                           "registration is not possible, connect to the server and try again");
}

void ServerManager::setLoginDataToServer(SM_AccountInfo account, QString password)
{
    if (isConnectToServer){
        QString randKey = SM_CF::getRandomKey(6);
        queue.insert(randKey, RequestQueue{"log", QDateTime::currentDateTime()});
        stream << randKey << int(SM::Key::setAccountInfo) << account << password;
    }
    else
        appBind->setResult(false,
                           "", //тут что то я вно нужно написать вместо пустого
                           SM_CF::unite({"client", "server_manager", "get_accoun_info"}),
                           "registration is not possible, connect to the server and try again");
}
