#include "palettemanager.h"

PaletteManager::PaletteManager(QObject *parent)
    : SM_AppPalette(300, parent)
{
    this->setLineColor(0x0e, 0x1d, 0x01);

    this->getMainPanel()->setTextColor(0xd9, 0xd7, 0xca);
    this->getMainPanel()->setBackgroundColor(0x3b, 0x35, 0x35);
    this->getMainPanel()->setButtonColor(0xd9, 0xd7, 0xca);

//    this->getOtherPanel()->setTextColor();
//    this->getOtherPanel()->setBackgroundColor();
//    this->getOtherPanel()->setButtonColor();

//    this->getMainPalette()->setTextColor();
//    this->getMainPalette()->setBackgroundColor();
//    this->getMainPalette()->setButtonColor();

//    this->getSecondPalette()->setTextColor();
//    this->getSecondPalette()->setBackgroundColor();
//    this->getSecondPalette()->setButtonColor();
}
