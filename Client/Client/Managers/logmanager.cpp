#include "logmanager.h"

LogManager::LogManager(QObject *parent) : QObject(parent)
{
    timer.start(120000);
    QObject::connect(&timer, &QTimer::timeout, this, &LogManager::saveLog);
    QObject::connect(appBind, &SM_BindManager::logUpdate, this, [=](
                     SM::TypeMsg type,
                     QString message,
                     QString sendKey,
                     QString sender,
                     QString receiver){
        logList.push_back(new LogMessage(type, message, sendKey, sender, receiver));
    });
}

void LogManager::saveLog()
{
    logList.clear();
    QFile file(dir.absoluteFilePath("log.stat"));
    if (file.open(QIODevice::Truncate)){
        QDataStream stream(&file);
        stream << logList.length();
        for (auto a : logList)
            stream << a;
        file.close();
        return;
    }
    appBind->setMessage(SM::TypeMsg::warning,
                        SM_CF::unite({"client", "log_manager", "save_log"}),
                        "canot save Log");
}

void LogManager::readLog(QDir dir)
{
    this->dir = dir;
    logList.clear();
    QFile file(dir.absoluteFilePath("log.stat"));
    if (file.open(QIODevice::ReadOnly)){
        QDataStream stream(&file);
        int count;
        stream >> count;
        for (int i = 0; i < count; i++){
            logList.push_back(new LogMessage);
            stream >> *logList.last();
        }
        file.close();
        return;
    }
    appBind->setMessage(SM::TypeMsg::critical,
                        SM_CF::unite({"client", "log_manager", "read_log"}),
                        "cannot read the Log");
}

void LogManager::setDir(QDir dir)
{
    this->dir = dir;
    readLog(dir);
}

const QList<LogMessage> LogManager::getLogList()
{
    QList<LogMessage> ret;
    for (auto a : logList)
        ret.push_front(*a);
    return ret;
}

