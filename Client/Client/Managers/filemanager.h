#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QObject>
#include <QDir>
#include <QBuffer>
#include "filestat.h"
#include "ApplicationBind"
#include "sm_staticcorefunction.h"

class FileManager : public QObject
{
    Q_OBJECT
private:
    QDir currentAccountDir;
    QString currentLogin;
    QList <FileStat*> fileStatList;

    QString inventFileName();
    inline bool readStat();
    inline bool saveStat();

public:
    FileManager(QObject *parent = nullptr);

    void setCurrentAccount(QDir dir, QString login);
    void findAppDirectory();

signals:
    void returnDataFromFile(QString title, QByteArray byteData);
    void setAppDir(QDir dir);

public slots:
    bool getDataFromFile(QString title,
                         QString module = "client"/*,
                         QString keyEncrypt = ""*/);

    QMap <QString/*title*/, QDateTime/*lastEdit*/> getEditTimeMap();
    QStringList getNotificationFiles(QString moduleName);

    bool setDataToFile(QString title,
                       QByteArray byteData,
                       QString module,
                       QString keyEncrypt = "",
                       QString newTitle = "",
                       bool isDate = false,
                       QStringList tags = QStringList{});
};

#endif // FILEMANAGER_H
