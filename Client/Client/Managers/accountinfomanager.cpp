#include "accountinfomanager.h"

AccountInfoManager::AccountInfoManager(QString email,
                                       QString login,
                                       bool isAuthorizate)
    : email(email),
      login(login),
      isAuthorizate(isAuthorizate)
{}

QDataStream& operator << (QDataStream &s, AccountInfoManager &a)
{
    s << a.email << a.isAuthorizate << a.login;
    return s;
}

QDataStream& operator >> (QDataStream &s, AccountInfoManager &a)
{
    s >> a.email >> a.isAuthorizate >> a.login;
    return s;
}
