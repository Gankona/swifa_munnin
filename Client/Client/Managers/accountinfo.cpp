#include "accountinfo.h"

AccountInfo::AccountInfo()
    : SM_AccountInfo("", "", "", "")
{}

AccountInfo::AccountInfo(const AccountInfo &a)
    : SM_AccountInfo(a.login,
                     a.firstName,
                     a.lastName,
                     a.email,
                     a.telephon,
                     a.country),
      lastEdit(QDateTime::currentDateTime()),
      password(a.password)
{
    //password = a.password;
}

AccountInfo::operator SM_AccountInfo()
{
    return SM_AccountInfo(login,
                          firstName,
                          lastName,
                          email,
                          telephon,
                          country);
}

AccountInfo::AccountInfo(const SM_AccountInfo &a,
                         const QString password,
                         QDateTime lastEdit)
    : SM_AccountInfo(a),
      lastEdit(lastEdit)
{
    this->password = password;
}

QDataStream& operator << (QDataStream &s, AccountInfo &a)
{
    s << a.country << a.email << a.firstName << a.lastEdit
      << a.lastName << a.login << a.password << a.telephon;
    return s;
}

QDataStream& operator >> (QDataStream &s, AccountInfo &a)
{
    s >> a.country >> a.email >> a.firstName >> a.lastEdit
      >> a.lastName >> a.login >> a.password >> a.telephon;
    return s;
}
