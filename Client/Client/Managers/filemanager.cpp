#include "filemanager.h"

FileManager::FileManager(QObject *parent) : QObject(parent)
{}

void FileManager::findAppDirectory()
{
    QDir dir = QDir::home();
    if (! dir.cd(".svifaMunnin")){
        dir.mkdir(".svifaMunnin");
        if (! dir.cd(".svifaMunnin")){
            appBind->setMessage(SM::TypeMsg::critical,
                                SM_CF::unite({"client", "file_manager", "find_app_diretory"}),
                                "cannot create app directory");
            return;
        }
    }
    emit setAppDir(dir);
}

QString FileManager::inventFileName()
{
    bool isPresent;
    QString temp;
    do {
        isPresent = false;
        temp = "";
        for (int i = 0; i < 12; i++)
            temp += char(rand()%26+97);
        for (auto f : fileStatList)
            if (f->fileName == temp)
                isPresent = true;
    } while(isPresent);
    return temp;
}

void FileManager::setCurrentAccount(QDir dir, QString login)
{
    currentLogin = login;
    currentAccountDir = dir;
    dir.cd(login);
    readStat();
}

bool FileManager::readStat()
{
    QFile file(currentAccountDir.absoluteFilePath(currentLogin+".stat"));
    if (file.open(QIODevice::ReadOnly)){
        //read file and decrypt content
        //set files stats
        for (auto *a : fileStatList)
            delete a;
        fileStatList.clear();

        int count;
        QDataStream stream(&file);
        stream >> count;
        for (int i = 0; i < count; i++){
            fileStatList.push_back(new FileStat("", "", ""));
            stream >> *fileStatList.last();
        }
        file.close();
        return true;
    }
    return false;
}

bool FileManager::saveStat()
{
    QFile file(currentAccountDir.absoluteFilePath(currentLogin + ".stat"));
    if (file.open(QIODevice::Truncate)){
        //collect list
        QDataStream stream(&file);
        stream << fileStatList.length();
        for (auto *f : fileStatList)
            stream << f;

        //save to file
        file.close();
        return true;
    }
    return false;
}

bool FileManager::getDataFromFile(QString title, QString module/*, QString keyEncrypt*/)
{
    for (auto f : fileStatList)
        if (f->title == title){
            QDir dir(currentAccountDir);
            if (!dir.cd(module)){
                // error message
                return false;
            }

            QFile file(dir.absoluteFilePath(f->fileName + ".smfd"));
            if (!file.open(QIODevice::ReadOnly)){
                // error message
                return false;
            }

            QByteArray byteData = file.readAll();
            if (f->isEncrypt){
                /// тут проверка на правильность ключа
                /// но пока тут всегда правильно
                emit returnDataFromFile(title, byteData);
            }
            else {
                // просто читем файл, но это скучно
                // ПС тут еще сигнал что все хорошо считало
                emit returnDataFromFile(title, byteData);
            }
            return true;
        }
    // значит мы не нашли в списке такое название
    return false;
}

bool FileManager::setDataToFile(QString title,
                                QByteArray byteData,
                                QString module,
                                QString keyEncrypt,
                                QString newTitle,
                                bool isDate,
                                QStringList tags)
{
    QDir dir(currentAccountDir);
    if (! dir.cd(module)){
        // error message
        return false;
    }
    bool isFilePresent(false);
    for (auto f : fileStatList)
        if (f->title == title)
            isFilePresent = true;
    QFile file;
    QString fileName;
    if (isFilePresent){
        for (auto f : fileStatList)
            if (f->title == title)
                file.setFileName(f->fileName + ".smfd");
        file.open(QIODevice::Truncate);
    }
    else {
        fileName = inventFileName();
        file.setFileName(fileName + ".smfd");
        file.open(QIODevice::WriteOnly);
    }
//    if (keyEncrypt != "")
//        byteData = SM_Encryption::encryptFile(byteData, keyEncrypt);
    file.write(byteData);
    file.close();

    //write in stat
    for (auto f : fileStatList)
        if (f->title == title){
            f->lastEditTime = QDateTime::currentDateTime();
            f->isDate = isDate;
            f->isEncrypt = keyEncrypt != "";
            f->module = module;
            f->tags = tags;
            if (newTitle != "")
                f->title = newTitle;
            break;
        }
    if (! isFilePresent)
        fileStatList.push_back(new FileStat(title, fileName, module,
                                            tags, isDate, keyEncrypt != ""));
    saveStat();
    return true;
}

QMap <QString, QDateTime> FileManager::getEditTimeMap()
{
    QMap <QString, QDateTime> ret;
    for (auto f : fileStatList)
        ret.insert(f->title, f->lastEditTime);
    return ret;
}

QStringList FileManager::getNotificationFiles(QString moduleName)
{
    QStringList ret;
    for (auto f : fileStatList)
        if (f->module == moduleName)
            if (f->isDate)
                ret.push_back(f->title);
    return ret;
}
