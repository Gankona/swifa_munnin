#ifndef LOGMANAGER_H
#define LOGMANAGER_H

#include <QtCore/QDir>
#include <QtCore/QTimer>

#include "logmessage.h"
#include "ApplicationBind"

class LogManager : public QObject
{
    Q_OBJECT
private:
    QDir dir;
    QTimer timer;

    QList <LogMessage*> logList;

    void saveLog();

public:
    explicit LogManager(QObject *parent = nullptr);

    void setDir(QDir dir);
    void readLog(QDir dir);
    const QList <LogMessage> getLogList();
};

#endif // LOGMANAGER_H
