#ifndef ACCOUNTINFOMANAGER_H
#define ACCOUNTINFOMANAGER_H

#include <QString>
#include <QDataStream>

class AccountInfoManager
{
public:
    QString email;
    QString login;
    bool isAuthorizate;

    AccountInfoManager(QString email = "",
                       QString login = "",
                       bool isAuthorizate = false);

    friend QDataStream& operator << (QDataStream &s, AccountInfoManager &a);
    friend QDataStream& operator >> (QDataStream &s, AccountInfoManager &a);
};

#endif // ACCOUNTINFOMANAGER_H
