#include "modulemanager.h"

ModuleManager::ModuleManager(QObject *parent) : QObject(parent)
{

}

const QMap <QString, SM::Module> ModuleManager::getModuleMap()
{
    return accountModuleMap;
}

void ModuleManager::testModulePresent()
{
    directoryModuleList.clear();
    QDir dir(QDir::current());
    if (dir.cd("module")){
        dir.mkdir("module");
        dir.cd("module");
        return;
    }
#ifdef Q_OS_WIN
    QFileInfoList dirContent = dir.entryInfoList(QStringList{"*.dll"}, QDir::Files);
#endif
#ifdef Q_OS_LINUX
    QFileInfoList dirContent = dir.entryInfoList(QStringList{"*.so"}, QDir::Files);
#endif
    for (auto f : dirContent)
        directoryModuleList.push_back(f.fileName());
}

void ModuleManager::reloadModules()
{
    for (auto s : accountModuleMap.keys())
        if (accountModuleMap.value(s) == SM::Module::moduleSuccessfullLoad){
            QPluginLoader pluginLoader;

#ifdef Q_OS_WIN
            pluginLoader.setFileName(QDir::current().absoluteFilePath(s));
#endif
#ifdef Q_OS_LINUX
            pluginLoader.setFileName(QDir::current().absoluteFilePath(s));
#endif

            pluginLoader.unload();
    }
    moduleLoadList.clear();
    accountModuleMap.clear();
    testModulePresent();

    QStringList moduleList = getModuleList();

    for (auto s : moduleList)
        accountModuleMap.insert(s, loadModule(s));
    for (auto s : directoryModuleList)
        if (! moduleList.contains(s))
            accountModuleMap.insert(s, SM::Module::moduleNotLoad);
}

SM::Module ModuleManager::loadModule(QString moduleName)
{
    QPluginLoader pluginLoader;
    QObject *plugin;
    QFile file;
#ifdef Q_OS_WIN
    file.setFileName(QDir::current().absoluteFilePath("module/" + moduleName + ".dll"));
#endif
#ifdef Q_OS_LINUX
    file.setFileName(QDir::current().absoluteFilePath("module/lib" + moduleName + ".so"));
#endif
    if (! file.exists())
        return SM::Module::moduleNotPresent;

#ifdef Q_OS_WIN
    pluginLoader.setFileName(QDir::current().absoluteFilePath(file.fileName()));
#endif
#ifdef Q_OS_LINUX
    pluginLoader.setFileName(QDir::current().absoluteFilePath(file.fileName()));
#endif
    plugin = pluginLoader.instance();
    if (plugin) {
        SM_Module *module = qobject_cast<SM_Module*>(plugin);
        moduleLoadList.insert(moduleName, module);
        emit setIcon(module->getIcon(), module->getModuleName());
//        QObject::connect(module, &SM_Module::setLog, this, [=]
//                         (SM::TypeMsg type,
//                         QString sender,
//                         QString message,
//                         QString sendKey,
//                         QString deliveryKey,
//                         QString receiver){
//            appLog->setLog(type,
//                           SM_CF::unite({moduleName, sender}),
//                           message,
//                           sendKey,
//                           SM_CF::unite({moduleName, deliveryKey}),
//                           receiver);
//        });
        QObject::connect(module, &SM_Module::setDataByte, this, [=]
                         (QString title,
                          QByteArray byteData,
                          QString keyEncrypt,
                          QString newTitle,
                          bool isDate,
                          QStringList tags){
            return setDataByte(title, byteData, module->getModuleName(),
                               keyEncrypt, newTitle, isDate, tags);
        });
        QObject::connect(module, &SM_Module::getNotificationFiles, this, [=](){
            return getNotificationFiles(module->getModuleName());
        });
        QObject::connect(module, &SM_Module::getDataByte, this, [=]
                         (QString title,
                          QString keyEncrypt){
            return getDataByte(title, module->getModuleName(), keyEncrypt);
        });
        QObject::connect(module, &SM_Module::changeStatus, this, [=]
                         (SM::Module status){
            emit changeStatus(status, module->getModuleName());
        });
        QObject::connect(module, &SM_Module::createWidget, this, [=]
                         (QWidget *widget){
            emit createWidget(widget, module->getModuleName());
        });

//        baseList.push_back(new ModuleList(module,
//                                          view->getPanelBar()->setNewButton(
//                                              true,
//                                              module->getMainIcon(),
//                                              module->getModuleName())));
        return SM::Module::moduleSuccessfullLoad;
    }
    return SM::Module::moduleErrorLoad;
}
