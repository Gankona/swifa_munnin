#include "settingsmanager.h"

SettingsManager::SettingsManager(QObject *parent)
    : QObject(parent),
      isWaitFileFromServer(false)
{
//    QObject::connect(appSettings, &SM_Settings::setValue, this, [=]
//                     (QString key,
//                      QVariant value){
//        settingsMap.insert(appSettings->getFullKey(key), value);
//    });
//    QObject::connect(appSettings, &SM_Settings::value, this, [=]
//                     (QString key,
//                      QVariant defaultValue){
//        return settingsMap.value(appSettings->getFullKey(key), defaultValue);
//    });

    QObject::connect(appBind, &SM_BindManager::signalInternal, this, [=]
                    (QString sendKey,
                     QString message){
        if (SM_CF::getTier(sendKey) == "account"){
            if (SM_CF::getTier(sendKey, 1) == "login"){
                qDebug() << "read call";
                setLogin(SM_CF::getTier(message));
                readSetting();
            }
            else if (SM_CF::getTier(sendKey, 1) == "logout")
                writeSetting();
        }
        else if (SM_CF::getTier(sendKey) == "settings"){
            if (SM_CF::getTier(sendKey, 1) == "save")
                writeSetting();
        }
    });
    QObject::connect(appBind, &SM_BindManager::result, this, [=]
                     (bool result,
                      QString deliveryKey,
                      QString message){
        Q_UNUSED(message)
        if (SM_CF::getTier(deliveryKey) == "settings")
            if (SM_CF::getTier(deliveryKey, 1) == "get_settings")
                if (! result)
                    setDefaultSettings(SM_CF::getTier(deliveryKey, 2));
    });
}

void SettingsManager::setLogin(QString login)
{
    currentLogin = login;
    readSetting();
}

void SettingsManager::setAppDir(QDir dir)
{
    currentDir = dir;
}

void SettingsManager::readSetting()
{
    QDir dir(currentDir);
    if (!dir.cd(currentLogin)){
        appBind->setMessage(SM::TypeMsg::warning,
                            SM_CF::unite({"client", "settings_manager", "write_setting"}),
                            "cannot open directory " + currentDir.absolutePath() + currentLogin);
        return;
    }
    QFile file(dir.absoluteFilePath(currentLogin + "_setting.stat"));
    if (file.open(QIODevice::ReadOnly)){
        QDataStream stream(&file);
        moduleList.clear();
        QMap <QString, QVariant> settingsMap;
        stream >> moduleList >> settingsMap;
        file.close();
        appSettings->clear();
        for (auto s : settingsMap.keys())
            appSettings->setValue(s, settingsMap.value(s));
        appBind->setGlobalSignal("update_setting",
                                 SM_CF::unite({"client", "settings_manager", "read_setting"}));
    }
    else {
        isWaitFileFromServer = true;
        appBind->setSignal(SM_CF::unite({"server", "get_settings"}),
                           SM_CF::unite({"client", "settings_manager", "read_setting"}),
                           "",
                           SM_CF::unite({"settings", "get_settings", currentLogin}));
    }
}

void SettingsManager::setDefaultSettings(QString login)
{
    Q_UNUSED(login)

    appSettings->clear();
    appSettings->setValue("Interface/minimumSize", QSize(400, 300));
    appSettings->setValue("Interface/startFixedWindowSize", QSize(600, 400));
    appSettings->setValue("Interface/rateblyWindowScretch", 75);
    appSettings->setValue("Interface/isFixedSizeWindow", false);
    appSettings->setValue("Interface/isChangeSizeRateably", false);
    appSettings->setValue("Interface/isAlwaysShowWindow", false);
    appSettings->setValue("Interface/isFloatLoginWindow", true);
    appSettings->setValue("Interface/isFixedStartWindowSize", false);
    appSettings->setValue("Interface/startFixedWindowSize", QSize(600, 500));
#if defined(Q_OS_LINUX) || defined(Q_OS_WIN)
    appSettings->setValue("Interface/isWidgetInterface", true);
#else
    appSettings->setValue("Interface/isWidgetInterface", false);
#endif
    writeSetting();
    appBind->setGlobalSignal("update_setting",
                             SM_CF::unite({"client", "settings_manager", "set_default_settings"}));
}

QStringList SettingsManager::getModuleList()
{
    if (isWaitFileFromServer)
        return QStringList();
    return moduleList;
}

void SettingsManager::setSettingFromServer(QByteArray &b)
{
    QDir dir(currentDir);
    if (!dir.cd(currentLogin)){
        appBind->setMessage(SM::TypeMsg::warning,
                            SM_CF::unite({"client", "settings_manager", "write_setting"}),
                            "cant open directory " + currentDir.absolutePath() + currentLogin);
        return;
    }
    QDataStream stream(b);
    QMap <QString, QVariant> settingsMap;
    stream >> settingsMap;
    appSettings->clear();
    for (auto s : settingsMap.keys())
        appSettings->setValue(s, settingsMap.value(s));

    QFile file(dir.absoluteFilePath(currentLogin + "_setting.stat"));
    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate)){
        file.write(b);
        file.close();
    }
    isWaitFileFromServer = false;
}

void SettingsManager::writeSetting()
{
    QDir dir(currentDir);
    if (!dir.cd(currentLogin)){
        appBind->setMessage(SM::TypeMsg::warning,
                            SM_CF::unite({"client", "settings_manager", "write_setting"}),
                            "cant open directory " + currentDir.absolutePath() + currentLogin);
        return;
    }

    QFile file(dir.absoluteFilePath(currentLogin + "_setting.stat"));
    file.remove();
    file.open(QIODevice::WriteOnly);
    QDataStream stream(&file);
    QMap <QString, QVariant> settingsMap;
    for (auto s : appSettings->getAllKeys())
        settingsMap.insert(s, appSettings->value(s));
    stream << moduleList << settingsMap;
    file.close();
}
