#ifndef FILESTAT_H
#define FILESTAT_H

#include <QtCore/QDataStream>
#include <QtCore/QDateTime>
#include <QtCore/QString>
#include <QtCore/QStringList>

class FileStat
{
public:
    FileStat(QString title,
             QString fileName,
             QString module,
             QStringList tags,
             bool isDate = false,
             bool isEncrypt = false,
             QDateTime createTme = QDateTime::currentDateTime(),
             QDateTime lastEditTime = QDateTime::currentDateTime());

    FileStat(QString title,
             QString fileName,
             QString module,
             bool isDate = false,
             bool isEncrypt = false,
             QDateTime createTime = QDateTime::currentDateTime(),
             QDateTime lastEditTime = QDateTime::currentDateTime());

    QString title;
    QString fileName;
    QString module;
    QStringList tags;
    bool isDate;
    bool isEncrypt;
    QDateTime createTime;
    QDateTime lastEditTime;

    friend QDataStream& operator << (QDataStream &d, FileStat &f);
    friend QDataStream& operator >> (QDataStream &d, FileStat &f);
};

#endif // FILESTAT_H
