#ifndef SM_MESSAGE_H
#define SM_MESSAGE_H

#include <QtCore/QDataStream>
#include <QtCore/QDateTime>
#include <QtCore/QObject>
#include <QtCore/QString>

#include "sm_namespace.h"

class LogMessage : public QObject
{
    Q_OBJECT
public:
    LogMessage(SM::TypeMsg type =  SM::TypeMsg::info,
               QString message = "",
               QString sendKey = "",
               QString deliveryKey = "",
               QString senderModuleName = "",
               QString receiverModuleName = "",
               QDateTime time = QDateTime::currentDateTime());

    SM::TypeMsg type;
    QString message;
    QString sendKey;
    QString deliveryKey;
    QString senderModuleName;
    QString receiverModuleName;
    QDateTime time;

    LogMessage(const LogMessage &m);

    friend QDataStream& operator << (QDataStream &s, LogMessage &m);

    friend QDataStream& operator >> (QDataStream &s, LogMessage &m);
};

#endif // SM_MESSAGE_H
