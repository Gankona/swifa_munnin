#include "sm_bindmanager.h"
#include "sm_settings.h"
#include "Managers/palettemanager.h"
#include "maincontrol.h"

SM_BindManager *appBind = new SM_BindManager;
PaletteManager *appPalette = new PaletteManager;
SM_Settings *appSettings = new SM_Settings;

int main(int argc, char** argv){
    QApplication app(argc, argv);
    app.setApplicationName("Swafi Munnin");
    app.setOrganizationName("Gankona");
    app.setQuitOnLastWindowClosed(false);
    app.setWindowIcon(QIcon("://Images/crow2.png"));

    MainControl *mainCtrl = new MainControl;
    appBind->setMessage(SM::TypeMsg::info,
                        SM_CF::unite({"client", "main"}), "start");
    mainCtrl->startApp();

    return app.exec();
}
