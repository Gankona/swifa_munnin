#ifndef MAINCONTROL_H
#define MAINCONTROL_H

#include <QtWidgets/QApplication>
#include <QtWidgets/QSplashScreen>

#include "ApplicationBind"
#include "Managers/accountmanager.h"
#include "Managers/filemanager.h"
#include "Managers/logmanager.h"
#include "Managers/modulemanager.h"
#include "Managers/servermanager.h"
#include "Managers/settingsmanager.h"

#include "View/mainwindow.h"
#include "View/tray.h"

class MainControl : public QObject
{
    Q_OBJECT
private:
    QSplashScreen *screen;
    AccountManager *accountManager;
    FileManager *fileManager;
    LogManager *logManager;
    ServerManager *serverManager;
    ModuleManager *moduleManager;
    SettingsManager *settingsManager;

    MainWindow *mainWindow;
    Tray *tray;

public:
    explicit MainControl();

    void startApp();
};

#endif // MAINCONTROL_H
