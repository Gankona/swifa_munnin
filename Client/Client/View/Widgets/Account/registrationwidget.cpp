#include "registrationwidget.h"

RegistrationWidget::RegistrationWidget(QWidget *parent)
    : QWidget(parent),
      isLoginCorrect(false),
      isEmailCorrect(false)
{
    titleLabel = SM_WF::label("Registration", Qt::AlignCenter, this, QSize(140, 30));
    loginLabel = SM_WF::label("login:", this, 180, 20);
    emailLabel = SM_WF::label("email:", this, 180, 20);
    firstNameLabel = SM_WF::label("name:", this, 180, 20);
    countryLabel = SM_WF::label("country:", this, 180, 20);
    lastNameLabel = SM_WF::label("surname:", this, 180, 20);
    telephonLabel = SM_WF::label("telephon:", this, 180, 20);
    passwordLabel = SM_WF::label("password:", this, 180, 20);
    rPasswordLabel = SM_WF::label("repeat password:", this, 180, 20);

    showPasswordBox = SM_WF::checkBox("show password", 180, 20, this, 10);
    countryBox = new QComboBox(this);
    countryBox->setFixedSize(180, 25);
    countryBox->addItems(QStringList{"Afghanistan", "Albania", "Algeria", "Andorra", "Angola",
                                     "Antigua and Barbuda", "Argentina", "Armenia", "Aruba",
                                     "Australia", "Austria", "Azerbaijan", "Bahamas, The",
                                     "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium",
                                     "Belize", "Benin", "Bhutan", "Bolivia", "Bosnia and Herzegovina",
                                     "Botswana", "Brazil", "Brunei ", "Bulgaria", "Burkina Faso",
                                     "Burma", "Burundi", "Cambodia", "Cameroon", "Canada",
                                     "Cabo Verde", "Central African Republic", "Chad", "Chile",
                                     "China", "Colombia", "Comoros", "Congo, Democratic Republic of the",
                                     "Congo, Republic of the", "Costa Rica", "Cote d'Ivoire",
                                     "Croatia", "Cuba", "Curacao", "Cyprus", "Czech Republic",
                                     "Denmark", "Djibouti", "Dominica", "Dominican Republic",
                                     "East Timor", "Ecuador", "Egypt", "El Salvador",
                                     "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji",
                                     "Finland", "France", "Gabon", "Gambia", "Georgia", "Germany",
                                     "Ghana", "Greece", "Grenada", "Guatemala", "Guinea", "Guyana",
                                     "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India",
                                     "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy",
                                     "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati",
                                     "Korea", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia",
                                     "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein",
                                     "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar",
                                     "Malawi", "Malaysia", "Maldives", "Mali", "Malta",
                                     "Marshall Islands", "Mauritania", "Mauritius", "Mexico",
                                     "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro",
                                     "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands",
                                     "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman",
                                     "Pakistan", "Palau", "Palestinian", "Panama", "Papua New Guinea",
                                     "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Qatar",
                                     "Romania", "Russia", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia",
                                     "Saint Vincent and the Grenadines", "Samoa", "San Marino",
                                     "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia",
                                     "Seychelles", "Sierra Leone", "Singapore", "Slovakia",
                                     "Slovenia", "Somalia", "South Africa", "Spain", "Sri Lanka",
                                     "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland",
                                     "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand",
                                     "Timor-Leste", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia",
                                     "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine",
                                     "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan",
                                     "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe"});

    loginEdit = SM_WF::lineEdit(this, 180, 25);
    firstNameEdit = SM_WF::lineEdit(this, 180, 25);
    lastNameEdit = SM_WF::lineEdit(this, 180, 25);
    emailEdit = SM_WF::lineEdit(this, 180, 25);
    telephonEdit = SM_WF::lineEdit(this, 180, 25);
    passwordEdit = SM_WF::lineEdit(this, 180, 25);
    rPasswordEdit = SM_WF::lineEdit(this, 180, 25);

    enterButton = SM_WF::button("registrate", this, 180, 30);
    backButton = SM_WF::iconButton(style()->standardIcon(QStyle::SP_ArrowBack), "back", this, 30, 30, true);

    setFixedSize(200, 445);

    backButton->move(0, 0);
    titleLabel->move(30, 0);
    loginLabel->move(10, 30);
    loginEdit->move(10, 50);
    firstNameLabel->move(10, 75);
    firstNameEdit->move(10, 95);
    lastNameLabel->move(10, 120);
    lastNameEdit->move(10, 140);
    emailLabel->move(10, 165);
    emailEdit->move(10, 185);
    telephonLabel->move(10, 210);
    telephonEdit->move(10, 230);
    countryLabel->move(10, 255);
    countryBox->move(10, 275);
    showPasswordBox->move(10, 300);
    passwordLabel->move(10, 315);
    passwordEdit->move(10, 335);
    rPasswordLabel->move(10, 360);
    rPasswordEdit->move(10, 380);
    enterButton->move(10, 410);

    loginEdit->setFocus();
    passwordEdit->setEchoMode(QLineEdit::Password);
    rPasswordEdit->setEchoMode(QLineEdit::Password);

    QObject::connect(loginEdit,     &QLineEdit::returnPressed, firstNameEdit,
                     static_cast<void (QWidget::*)()>(&QWidget::setFocus));

    QObject::connect(firstNameEdit, &QLineEdit::returnPressed, lastNameEdit,
                     static_cast<void (QWidget::*)()>(&QWidget::setFocus));

    QObject::connect(lastNameEdit,  &QLineEdit::returnPressed, emailEdit,
                     static_cast<void (QWidget::*)()>(&QWidget::setFocus));

    QObject::connect(emailEdit,     &QLineEdit::returnPressed, telephonEdit,
                     static_cast<void (QWidget::*)()>(&QWidget::setFocus));

    QObject::connect(telephonEdit,  &QLineEdit::returnPressed, passwordEdit,
                     static_cast<void (QWidget::*)()>(&QWidget::setFocus));

    QObject::connect(passwordEdit,  &QLineEdit::returnPressed, rPasswordEdit,
                     static_cast<void (QWidget::*)()>(&QWidget::setFocus));

    QObject::connect(rPasswordEdit, &QLineEdit::returnPressed, enterButton, &QPushButton::click);

    QObject::connect(enterButton, &QPushButton::clicked, this, &RegistrationWidget::clickRegistrate);

    QObject::connect(appBind, &SM_BindManager::result, this, &RegistrationWidget::catchSignalResult);

    QObject::connect(backButton, &QPushButton::clicked, this, &RegistrationWidget::backToLogin);

    QObject::connect(loginEdit, &QLineEdit::textChanged, this, [=](QString text){
        appBind->setSignal(SM_CF::unite({"server", "test_login", "must_absent"}),
                           SM_CF::unite({"client", "registration_widget", "login_text_change"}),
                           text,
                           SM_CF::unite({"widget", "account_answer", "login"}));
    });

    QObject::connect(emailEdit, &QLineEdit::textChanged, this, [=](QString text){
        appBind->setSignal(SM_CF::unite({"server", "test_email", "must_absent"}),
                           SM_CF::unite({"client", "registration_widget", "email_text_change"}),
                           text,
                           SM_CF::unite({"widget", "account_answer", "email"}));
    });


    QObject::connect(showPasswordBox, &QCheckBox::toggled, this, [=](bool t){
        if (t){
            passwordEdit->setEchoMode(QLineEdit::Normal);
            rPasswordEdit->setEchoMode(QLineEdit::Normal);
        }
        else {
            passwordEdit->setEchoMode(QLineEdit::Password);
            rPasswordEdit->setEchoMode(QLineEdit::Password);
        }
    });
}

void RegistrationWidget::clickRegistrate()
{
    QString errorMsg("");
    if (emailEdit->text().isEmpty())
        errorMsg = "email is not enter";
    else if (!isEmailCorrect)
        errorMsg = "email is not correct";
    else if (loginEdit->text().isNull())
        errorMsg = "login is not enter";
    else if (!isLoginCorrect)
        errorMsg = "login is not correct";
    else if (firstNameEdit->text().isNull())
        errorMsg = "first name is not enter";
    else if (lastNameEdit->text().isNull())
        errorMsg = "last name is not enter";
    else if (passwordEdit->text().isNull())
        errorMsg = "password is not enter";
    else if (passwordEdit->text() != rPasswordEdit->text())
        errorMsg = "password do not match";

    if (errorMsg != "")
        appBind->setError(SM_CF::unite({"widget", "account_answer"}),
                          SM_CF::unite({"client", "registration_widget", "enter_button_click"}),
                          errorMsg);
    else
        appBind->setSignal(SM_CF::unite({"server", "try_registrate"}),
                           SM_CF::unite({"client", "registration_widget", "enter_button_click"}),
                           SM_CF::unite({
                               SM_AccountInfo::getTierString(SM_AccountInfo(loginEdit->text(),
                                                                            firstNameEdit->text(),
                                                                            lastNameEdit->text(),
                                                                            emailEdit->text(),
                                                                            telephonEdit->text(),
                                                                            countryBox->currentText())),
                               passwordEdit->text()
                               }),
                           SM_CF::unite({"widget", "account_answer"}));
}

void RegistrationWidget::catchSignalResult(bool isCorrect, QString key)
{
    if (SM_CF::getTier(key) == "widget"
            && SM_CF::getTier(key, 1) == "account_answer"){
        if (SM_CF::getTier(key, 2) == "login")
            isLoginCorrect = isCorrect;
        else if (SM_CF::getTier(key, 2) == "email")
            isEmailCorrect = isCorrect;
    }
}
