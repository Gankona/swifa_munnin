#include "accountinfowidget.h"

AccountInfoWidget::AccountInfoWidget(SM_AccountInfo accountInfo,
                                     QWidget *parent)
    : QFrame(parent)
{
    title = SM_WF::label("Account Info", Qt::AlignCenter, this);

    QString s;
    s += QString("login:").leftJustified(12, ' ');
    s += accountInfo.login;
    s += '\n';
    s += QString("email:").leftJustified(12, ' ');
    if (accountInfo.email.length() >= 20)
        s += "\n   ";
    s += accountInfo.email;
    s += '\n';
    s += QString("first name:").leftJustified(12, ' ');
    s += accountInfo.firstName;
    s += '\n';
    s += QString("last name:").leftJustified(12, ' ');
    s += accountInfo.lastName;
    s += '\n';
    s += QString("telephon:").leftJustified(12, ' ');
    s += accountInfo.telephon;
    int sLenght = 6;

    infoLabel = SM_WF::label(s, this);
    settingsButton = SM_WF::iconButton(QIcon(":/Images/settings.png"),"settings", this);
    logoutButton = SM_WF::iconButton(QIcon(":/Images/logout.png"),"logout", this);
    hotKeyButton = SM_WF::iconButton(QIcon(":/Images/hotkey.png"),"hot key", this);
    editProfileButton = SM_WF::iconButton(QIcon(":/Images/edit.png"),"edit profile", this);
    setFixedSize(200, 20 * sLenght + 80);

    title->setFixedSize(186, 30);
    title->move(7, 0);
    infoLabel->setFixedSize(190, 20 * sLenght);
    infoLabel->move(5, 32);
    settingsButton->setFixedSize(50, 50);
    settingsButton->move(150, 20 * sLenght + 30);
    logoutButton->setFixedSize(50, 50);
    logoutButton->move(100, 20 * sLenght + 30);
    hotKeyButton->setFixedSize(50, 50);
    hotKeyButton->move(50, 20 * sLenght + 30);
    editProfileButton->setFixedSize(50, 50);
    editProfileButton->move(0, 20 * sLenght + 30);

    QObject::connect(editProfileButton, &QPushButton::clicked, this, [=](){
        appBind->setLink(SM_CF::unite({"settings", "account"}),
                         SM_CF::unite({"client", "account_info_widget", "edit_profile_button_click"}));
        closeAccountWidget();
    });

    QObject::connect(settingsButton, &QPushButton::clicked, this, [=](){
        appBind->setLink(SM_CF::unite({"settings", "interface"}),
                         SM_CF::unite({"client", "account_info_widget", "settings_button_click"}));
        closeAccountWidget();
    });
    QObject::connect(hotKeyButton, &QPushButton::clicked, this, [=](){
        appBind->setLink("hot_key",
                         SM_CF::unite({"client", "account_info_widget", "hot_key_button_click"}));
        closeAccountWidget();
    });
    QObject::connect(logoutButton, &QPushButton::clicked, this, [=](){
        appBind->setSignal(SM_CF::unite({"account", "set_logout"}),
                           SM_CF::unite({"client", "account_info_widget", "logout_button_click"}));
        closeAccountWidget();
    });
}
