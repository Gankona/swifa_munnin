#include "accountmanagerwidget.h"

AccountManagerWidget::AccountManagerWidget(bool isFloatEffect,
                                           SM_AccountInfo account,
                                           QWidget *parent)
    : SM_OpacityWidget(0.85, parent),
      isFloatEffect(isFloatEffect),
      infoWidget(nullptr),
      loginWidget(nullptr),
      registrationWidget(nullptr),
      recoverWidget(nullptr),
      spaces(2)
{
    setAutoFillBackground(true);
    if (! isFloatEffect)
        this->setOpacity(1);
    errorLabel = new QLabel(this);
    errorLabel->setVisible(false);
    errorLabel->setFixedSize(180, 20);
    errorLabel->setWordWrap(true);
    QFont font = this->font();
    font.setPixelSize(11);
    errorLabel->setFont(font);

    appBind->setMessage(SM::TypeMsg::debug,
                        SM_CF::unite({"client", "account_manager_widget", "constructor", "login"}),
                        account.login);

    if (account.login == "NoName"){
       createLoginWidget();
    }
    else {
        infoWidget = new AccountInfoWidget(account, this);
        if (isFloatEffect)
            infoWidget->move(spaces, spaces);
        accountSize = infoWidget->size();
        QObject::connect(infoWidget, &AccountInfoWidget::closeAccountWidget,
                         this,    &AccountManagerWidget::closeAccountWidget);
    }
    QObject::connect(appBind, &SM_BindManager::result, this, [=]
                     (bool isCorrect,
                      QString key,
                      QString message){
        if (SM_CF::getTier(key) == "widget"
                && SM_CF::getTier(key, 1) == "account_answer"){
            errorLabel->setVisible(true);
            this->setFixedSize(accountSize.width(),
                               accountSize.height() + errorLabel->height());
            errorLabel->move(10, accountSize.height());
            errorLabel->setText(message);
            errorLabel->setFixedHeight((message.length()/35 + 1) * 20);
            emit repaintContent();
            QPalette palette;
            if (isCorrect){
                palette.setColor(QPalette::Foreground, Qt::green);
            }
            else {
                palette.setColor(QPalette::WindowText, Qt::darkGreen);
            }
            errorLabel->setPalette(palette);
        }
    }, Qt::AutoConnection);
}

void AccountManagerWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    if (! isFloatEffect)
        return;
    QPainter painter(this);
    painter.setPen(QPen(QColor::fromHsl(this->palette().base().color().hslHue(),
                                        this->palette().base().color().hslSaturation(),
                                        255 - this->palette().base().color().lightness()), spaces));
    QPolygon border;
    border << QPoint(spaces/2, spaces/2)
           << QPoint(this->width() - spaces/2, spaces/2)
           << QPoint(this->width() - spaces/2, this->height() - spaces/2)
           << QPoint(spaces/2, this->height() - spaces/2);
    painter.drawPolygon(border);
}

QSize AccountManagerWidget::getSize()
{
    if (isFloatEffect){
        if (errorLabel->isVisible())
            return QSize(accountSize.width()+spaces, accountSize.height() + errorLabel->height() + spaces);
        return accountSize + QSize(spaces, spaces);
    }
    if (errorLabel->isVisible())
        return QSize(accountSize.width(), accountSize.height() + errorLabel->height());
    return accountSize;
}

bool AccountManagerWidget::getFloatEffectFlag()
{
    return isFloatEffect;
}

void AccountManagerWidget::createLoginWidget()
{
    loginWidget = new LoginWidget(this);
    if (isFloatEffect)
        loginWidget->move(spaces, spaces);
    accountSize = loginWidget->size();
    errorLabel->setVisible(false);
    QObject::connect(loginWidget, &LoginWidget::openForgotPasswordWidget, this, [=](){
        loginWidget->deleteLater();
        recoverWidget = new RecoverPasswordWidget(this);
        if (isFloatEffect)
            recoverWidget->move(spaces, spaces);
        recoverWidget->show();
        errorLabel->setVisible(false);
        accountSize = recoverWidget->size();
        emit repaintContent();
        QObject::connect(recoverWidget, &RecoverPasswordWidget::backToLogin, this, [=](){
            recoverWidget->deleteLater();
            createLoginWidget();
        });
    });
    QObject::connect(loginWidget, &LoginWidget::openRegistrationWidget, this, [=](){
        loginWidget->deleteLater();
        registrationWidget = new RegistrationWidget(this);
        if (isFloatEffect)
            registrationWidget->move(spaces, spaces);
        registrationWidget->show();
        errorLabel->setVisible(false);
        accountSize = registrationWidget->size();
        emit repaintContent();
        QObject::connect(registrationWidget, &RegistrationWidget::backToLogin, this, [=](){
            createLoginWidget();
            registrationWidget->deleteLater();
        });
    });
    loginWidget->show();
    emit repaintContent();
}
