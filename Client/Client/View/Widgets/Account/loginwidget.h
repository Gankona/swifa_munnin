#ifndef LOGINWIDGET_H
#define LOGINWIDGET_H

#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include "sm_staticwidgetfunction.h"
#include "sm_staticcorefunction.h"
#include "ApplicationBind"

class LoginWidget : public QFrame
{
    Q_OBJECT
private:
    QLabel *titleLabel;
    QLabel *loginLabel;
    QLabel *passwordLabel;
    QLineEdit *loginEdit;
    QLineEdit *passwordEdit;
    QLabel *forgotPasswordLabel;
    QLabel *registrationLabel;
    QPushButton *enterButton;
    bool isLoginCorrect;

public:
    LoginWidget(QWidget *parent = nullptr);

signals:
    void openRegistrationWidget();
    void openForgotPasswordWidget();
};

#endif // LOGINWIDGET_H
