#include "recoverpasswordwidget.h"

RecoverPasswordWidget::RecoverPasswordWidget(QWidget *parent) : QWidget(parent)
{
    backButton = SM_WF::iconButton(style()->standardIcon(QStyle::SP_ArrowBack),
                                   "back", this, 30, 30, true, false, false);
    enterButton = SM_WF::button("send password", this, 180, 30);
    titleLabel = SM_WF::label("Recover password", Qt::AlignCenter, this, 140, 30);
    loginLabel = SM_WF::label("login or email:", this, 180, 20);
    loginEdit = SM_WF::lineEdit(this, 180, 25);

    setFixedSize(200, 120);

    backButton->move(0, 0);
    titleLabel->move(30, 0);
    loginLabel->move(10, 30);
    loginEdit->move(10, 50);
    enterButton->move(10, 85);

    QObject::connect(loginEdit, &QLineEdit::textChanged, this, [=](QString text){
        if (text.length() >= 4)
            appBind->setSignal(SM_CF::unite({"server", "test_login", "must_present"}),
                               SM_CF::unite({"client", "recover_widget", "login_text_change"}),
                               text,
                               SM_CF::unite({"widget", "account_answer"}));
        else
            appBind->setError(SM_CF::unite({"widget", "account_answer"}),
                              SM_CF::unite({"client", "recover_widget", "login_text_change"}),
                              "login or email must be long then 4 symbol");
    });
    QObject::connect(loginEdit, &QLineEdit::returnPressed, enterButton, &QPushButton::click);
    QObject::connect(enterButton, &QPushButton::clicked, this, [=](){
        appBind->setSignal(SM_CF::unite({"server", "send_recover_password"}),
                           SM_CF::unite({"client", "recover_widget", "enter_button_pressed"}),
                           loginEdit->text(),
                           SM_CF::unite({"widget", "account_answer"}));
    });

    QObject::connect(backButton, &QPushButton::clicked, this, &RecoverPasswordWidget::backToLogin);
}
