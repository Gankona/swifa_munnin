#ifndef ACCOUNTINFOWIDGET_H
#define ACCOUNTINFOWIDGET_H

#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include "sm_staticcorefunction.h"
#include "sm_staticwidgetfunction.h"
#include "sm_accountinfo.h"
#include "ApplicationBind"

class AccountInfoWidget : public QFrame
{
    Q_OBJECT
private:
    QLabel *title;
    QLabel *infoLabel;
    QPushButton *settingsButton;
    QPushButton *logoutButton;
    QPushButton *hotKeyButton;
    QPushButton *editProfileButton;

public:
    AccountInfoWidget(SM_AccountInfo accountInfo, QWidget *parent = nullptr);

signals:
    void closeAccountWidget();
};

#endif // ACCOUNTINFOWIDGET_H
