#ifndef RECOVERPASSWORDWIDGET_H
#define RECOVERPASSWORDWIDGET_H

#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include "sm_staticcorefunction.h"
#include "sm_staticwidgetfunction.h"
#include "sm_accountinfo.h"
#include "ApplicationBind"
#include <QStyle>

class RecoverPasswordWidget : public QWidget
{
    Q_OBJECT
private:
    QLabel *titleLabel;
    QLabel *loginLabel;
    QLabel *passwordLabel;
    QLineEdit *loginEdit;
    QLineEdit *passwordEdit;
    QPushButton *enterButton;
    QPushButton *backButton;

public:
    explicit RecoverPasswordWidget(QWidget *parent = nullptr);

signals:
    void backToLogin();

public slots:
};

#endif // RECOVERPASSWORDWIDGET_H
