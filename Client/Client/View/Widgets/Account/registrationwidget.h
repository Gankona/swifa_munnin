#ifndef REGISTRATIONWIDGET_H
#define REGISTRATIONWIDGET_H

#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QComboBox>
#include <QStyle>
#include "sm_staticwidgetfunction.h"
#include "sm_accountinfo.h"
#include "ApplicationBind"

class RegistrationWidget : public QWidget
{
    Q_OBJECT
private:
    QLabel *titleLabel;
    QLabel *loginLabel;
    QLabel *firstNameLabel;
    QLabel *lastNameLabel;
    QLabel *emailLabel;
    QLabel *telephonLabel;
    QLabel *countryLabel;
    QLabel *passwordLabel;
    QLabel *rPasswordLabel;
    QCheckBox *showPasswordBox;
    QLineEdit *loginEdit;
    QLineEdit *firstNameEdit;
    QLineEdit *lastNameEdit;
    QLineEdit *emailEdit;
    QLineEdit *telephonEdit;
    QLineEdit *passwordEdit;
    QLineEdit *rPasswordEdit;
    QPushButton *enterButton;
    QPushButton *backButton;
    QComboBox *countryBox;
    bool isLoginCorrect;
    bool isEmailCorrect;

    void clickRegistrate();
    void catchSignalResult(bool isCorrect, QString key);

public:
    explicit RegistrationWidget(QWidget *parent = nullptr);

signals:
    void backToLogin();
    //void tryRegistration(SM_AccountInfo account, QString password);
};

#endif // REGISTRATIONWIDGET_H
