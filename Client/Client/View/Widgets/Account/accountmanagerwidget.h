#ifndef ACCOUNTMANAGERWIDGET_H
#define ACCOUNTMANAGERWIDGET_H

#include "sm_accountinfo.h"
#include "sm_opacitywidget.h"
#include "accountinfowidget.h"
#include "loginwidget.h"
#include "registrationwidget.h"
#include "recoverpasswordwidget.h"
#include "ApplicationBind"
#include "sm_staticcorefunction.h"
#include <QResizeEvent>
#include <QPaintEvent>
#include <QPainter>
#include <QDebug>
#include <QTimer>

class AccountManagerWidget : public SM_OpacityWidget
{
    Q_OBJECT
private:
    bool isFloatEffect;

    AccountInfoWidget *infoWidget;
    LoginWidget *loginWidget;
    RegistrationWidget *registrationWidget;
    RecoverPasswordWidget *recoverWidget;

    QLabel *errorLabel;
    int spaces;

    QSize accountSize;

    void createLoginWidget();

protected:
    void paintEvent(QPaintEvent *event);

public:
    AccountManagerWidget(bool isFloatEffect,
                         SM_AccountInfo account,
                         QWidget *parent = nullptr);
    bool getFloatEffectFlag();
    QSize getSize();

signals:
    SM_AccountInfo getAccountInfo();
    void repaintContent();
    void closeAccountWidget();
};

#endif // ACCOUNTMANAGERWIDGET_H
