#include "loginwidget.h"

LoginWidget::LoginWidget(QWidget *parent)
    : QFrame(parent),
      isLoginCorrect(false)
{
    titleLabel = SM_WF::label("Authorizate", Qt::AlignCenter, this, QSize(200, 30));
    loginLabel = SM_WF::label("login or email:", this, 180, 20);
    passwordLabel = SM_WF::label("password:", this, 180, 20);
    loginEdit = SM_WF::lineEdit(this, 180, 25);
    passwordEdit = SM_WF::lineEdit(this, 180, 25);
    passwordEdit->setEchoMode(QLineEdit::Password);
    forgotPasswordLabel = SM_WF::label("<a href=\"forgot password\">forgot password?</a>", this, 180, 20);
    registrationLabel = SM_WF::label("<a href=\"registration\">registration</a>", this, 180, 20);
    enterButton = SM_WF::button("enter", this, 180, 30);

    setFixedSize(200, 200);

    titleLabel->move(0, 0);
    loginLabel->move(10, 30);
    loginEdit->move(10, 50);
    passwordLabel->move(10, 75);
    passwordEdit->move(10, 95);
    forgotPasswordLabel->move(10, 120);
    registrationLabel->move(10, 140);
    enterButton->move(10, 165);

    QObject::connect(forgotPasswordLabel, &QLabel::linkActivated, this, &LoginWidget::openForgotPasswordWidget);
    QObject::connect(registrationLabel,   &QLabel::linkActivated, this, &LoginWidget::openRegistrationWidget);
    QObject::connect(loginEdit, &QLineEdit::returnPressed, passwordEdit,
                     static_cast<void (QWidget::*)()>(&QWidget::setFocus));
    QObject::connect(loginEdit, &QLineEdit::textChanged, this, [=](QString text){
        isLoginCorrect = false;
        if (text.length() >= 4)
            appBind->setSignal(SM_CF::unite({"account", "test_login", "must_present"}),
                               SM_CF::unite({"client", "login_widget", "login_text_change"}),
                               text,
                               SM_CF::unite({"widget", "account_answer"}));
    });
    QObject::connect(passwordEdit, &QLineEdit::returnPressed, enterButton, &QPushButton::click);
    QObject::connect(enterButton, &QPushButton::clicked, this, [=](){
        if (isLoginCorrect){
            if (passwordEdit->text().isNull())
                appBind->setError(SM_CF::unite({"widget", "account_answer"}),
                                  SM_CF::unite({"client", "login_widget", "try_authorizate"}),
                                  "password not enter");
            else
                appBind->setSignal(SM_CF::unite({"account", "try_authorizate"}),
                                   SM_CF::unite({"client", "login_widget", "try_authorizate"}),
                                   SM_CF::unite({loginEdit->text(), passwordEdit->text()}),
                                   SM_CF::unite({"widget", "account_answer"}));
        }
        else
            appBind->setError(SM_CF::unite({"widget", "account_answer"}),
                              SM_CF::unite({"client", "login_widget", "try_authorizate"}),
                              "login incorrect");
    });
    QObject::connect(appBind, &SM_BindManager::result, this, [=]
                     (bool isCorrect,
                      QString key){
        if (SM_CF::getTier(key) == "widget")
            if (SM_CF::getTier(key, 1) == "account_answer")
                if (SM_CF::getTier(key, 2) == "login")
                    isLoginCorrect = isCorrect;
    });
}
