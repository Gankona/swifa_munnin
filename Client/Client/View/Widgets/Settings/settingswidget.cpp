#include "settingswidget.h"

SettingsWidget::SettingsWidget(QWidget *parent)
    : QFrame(parent),
      account(nullptr),
      basic(nullptr),
      hotKey(nullptr),
      interfaces(nullptr),
      module(nullptr),
      multimedia(nullptr),
      spaces(2)
{
    mainWidget = new SM_MarginWidget(this, 0.75);
    menu = new SM_DeployWidgetMenu(this);
    menu->setAutoFillBackground(true);
    updatePalette();

    QObject::connect(menu, &SM_DeployWidgetMenu::clickedMenu, this, &SettingsWidget::openSettingWindow);

    menu->addButtonToMenu(SM_WF::iconTextButton(QIcon(":/Images/settings.png"), "Basic", true), "Basic");
    menu->addButtonToMenu(SM_WF::iconTextButton(QIcon(":/Images/interface.png"), "Interface", true), "Interface");
    menu->addButtonToMenu(SM_WF::iconTextButton(QIcon(":/Images/module.png"), "Modules", true), "Module");
    menu->addButtonToMenu(SM_WF::iconTextButton(QIcon(":/Images/multimedia.png"), "Multimedia", true), "Multimedia");
    menu->addButtonToMenu(SM_WF::iconTextButton(QIcon(":/Images/account.png"), "Account", true), "Account");
    menu->addButtonToMenu(SM_WF::iconTextButton(QIcon(":/Images/hotkey.png"), "Hot key", true), "Hot key");
}

void SettingsWidget::updatePalette()
{
    /*
    QPalette pal;

    pal.setColor(QPalette::Button, appPalette->getOtherPanel()->button());
    pal.setColor(QPalette::ButtonText, appPalette->getOtherPanel()->text());
    pal.setColor(QPalette::Background, appPalette->getOtherPanel()->background());
    menu->setPalette(pal);

    pal.setColor(QPalette::Button, appPalette->getMainPalette()->button());
    pal.setColor(QPalette::ButtonText, appPalette->getMainPalette()->text());
    pal.setColor(QPalette::Background, appPalette->getMainPalette()->background());
    mainWidget->setPalette(pal);

    repaint();
    */
}

void SettingsWidget::resizeEvent(QResizeEvent *event)
{
    menu->setFixedSize(event->size().width(), 25);
    menu->move(0, 0);
    mainWidget->setFixedSize(event->size().width(),
                             event->size().height() - 25 - spaces);
    mainWidget->move(0, 25 + spaces);
    repaint();
}

void SettingsWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    QPainter painter(this);
    painter.setPen(QPen(QBrush(QColor::fromHsl(this->palette().base().color().hslHue(),
                                               this->palette().base().color().hslSaturation(),
                                               255 - this->palette().base().color().lightness())),
                        spaces));
    painter.drawLine(QPoint(0, 25 + spaces/2), QPoint(this->width(), 25 + spaces/2));
}

void SettingsWidget::openSettingWindow(QString key)
{
    if (account != nullptr){
        delete account;
        account = nullptr;
    }
    else if (basic != nullptr){
        delete basic;
        basic = nullptr;
    }
    else if (hotKey != nullptr){
        delete hotKey;
        hotKey = nullptr;
    }
    else if (interfaces != nullptr){
        delete interfaces;
        interfaces = nullptr;
    }
    else if (module != nullptr){
        delete module;
        module = nullptr;
    }
    else if (multimedia != nullptr){
        delete multimedia;
        multimedia = nullptr;
    }

    if (key == "Account"){
        account = new SettingsAccount(this);
        account->show();
        mainWidget->setWidget(static_cast<QWidget*>(account));
    }
    else if (key == "Basic"){
        basic = new SettingsBasic(this);
        basic->show();
        mainWidget->setWidget(static_cast<QWidget*>(basic));
    }
    else if (key == "Hot key"){
        hotKey = new SettingsHotKey(this);
        hotKey->show();
        mainWidget->setWidget(static_cast<QWidget*>(hotKey));
    }
    else if (key == "Module"){
        module = new SettingsModule(this);
        module->show();
        mainWidget->setWidget(static_cast<QWidget*>(module));
    }
    else if (key == "Interface"){
        interfaces = new SettingsInterface(this);
        interfaces->show();
        mainWidget->setWidget(static_cast<QWidget*>(interfaces));
    }
    else if (key == "Multimedia"){
        multimedia = new SettingsMultimedia(this);
        multimedia->show();
        mainWidget->setWidget(static_cast<QWidget*>(multimedia));
    }
}
