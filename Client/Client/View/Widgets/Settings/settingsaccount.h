#ifndef SETTINGSACCOUNT_H
#define SETTINGSACCOUNT_H

#include <QtCore/QSettings>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>

#include "ApplicationBind"

#include "sm_labeledit.h"
#include "sm_settingsbuttons.h"

class SettingsAccount : public QWidget
{
    Q_OBJECT
private:
    QSettings settings;
    QScrollArea *area;
    QWidget *widget;

    //profile
    QLabel *editProfileLabel;
    SM_LabelEdit *nameEdit;
    SM_LabelEdit *surnameEdit;
    SM_LabelEdit *phoneEdit;

    //server
    QLabel *serverEditLabel;
    SM_LabelEdit *ipServerEdit;
    SM_LabelEdit *portEdit;

    //password
    QLabel *passwordEditLabel;

    SM_SettingsButtons *downButtons;

    inline void createProfile();
    inline void createPassword();
    inline void createServer();
    void resizeContent(QSize size);

protected:
    void resizeEvent(QResizeEvent *event);

public:
    explicit SettingsAccount(QWidget *parent = nullptr);

signals:

public slots:
};

#endif // SETTINGSACCOUNT_H
