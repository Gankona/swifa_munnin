#include "settingsinterface.h"

SettingsInterface::SettingsInterface(QWidget *parent)
    : QWidget(parent)
{
    mapItems.clear();

    widget = new QWidget;
    area = new QScrollArea(this);
    area->setWidget(widget);
    area->setFrameShape(QFrame::NoFrame);
    area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    downButtons = new SM_SettingsButtons(widget);
    createPoint();
    createSlider();
    setValue(false);

    appBind->setSignal("set_current_way",
                       SM_CF::unite({"client", "settings_interface"}),
                       SM_CF::unite({"settings", "interface"}));

    QObject::connect(downButtons, &SM_SettingsButtons::clickApply, this, &SettingsInterface::saveSettings);
    QObject::connect(downButtons, &SM_SettingsButtons::clickCancel, this, [=](){ setValue(false); });
    QObject::connect(downButtons, &SM_SettingsButtons::clickDefault, this, [=](){ setValue(true); });
    QObject::connect(downButtons, &SM_SettingsButtons::clickBack, this, [=](){
        appBind->setLink("null", SM_CF::unite({"client", "settings_interface", "back_click"}));
    });
}

void SettingsInterface::createOnePoint(QString name, QString key)
{
    mapItems.insert(key, new SM_SwitcherButton(name, widget));
    QObject::connect(mapItems.value(key), &SM_SwitcherButton::valueChanged, this, &SettingsInterface::updatePoint);
}

void SettingsInterface::createPoint()
{
    createOnePoint(tr("Always visible main window"), "isAlwaysShowWindow");
    createOnePoint(tr("Fix window size"), "isFixedSizeWindow");
    createOnePoint(tr("Translucent login window"), "isFloatLoginWindow");
    createOnePoint(tr("Resize proportionately"), "isChangeSizeRateably");
    createOnePoint(tr("Fixed window size on startup"), "isFixedStartWindowSize");
}

void SettingsInterface::createSlider()
{
    appSettings->beginGroup("Interface");
    QSize minimumSize = appSettings->value("minimumSize", QSize(400, 300)).toSize();
    QSize startFixedWindowSize = appSettings->value("startFixedWindowSize", QSize(600, 400)).toSize();
    float rateablyWindowScretch = appSettings->value("rateblyWindowScretch", 75).toFloat();
    appSettings->endGroup();

    minimumSizeX = new QSlider(Qt::Orientation::Horizontal, widget);
    minimumSizeX->setRange(500, QApplication::desktop()->screen()->width());
    minimumSizeX->setValue(minimumSize.width());

    minimumSizeY = new QSlider(Qt::Orientation::Horizontal, widget);
    minimumSizeY->setRange(300, QApplication::desktop()->screen()->height());
    minimumSizeY->setValue(minimumSize.height());

    startFixedWindowSizeX = new QSlider(Qt::Orientation::Horizontal, widget);
    startFixedWindowSizeX->setRange(minimumSize.width(), QApplication::desktop()->screen()->width());
    startFixedWindowSizeX->setTickInterval(10);
    startFixedWindowSizeX->setValue(startFixedWindowSize.width());

    startFixedWindowSizeY = new QSlider(Qt::Orientation::Horizontal, widget);
    startFixedWindowSizeY->setRange(minimumSize.height(), QApplication::desktop()->screen()->width());
    startFixedWindowSizeY->setTickInterval(10);
    startFixedWindowSizeY->setValue(startFixedWindowSize.height());

    rateablySize = new QSlider(Qt::Orientation::Horizontal, widget);
    rateablySize->setRange(40, 250);
    rateablySize->setTickInterval(10);
    rateablySize->setValue(rateablyWindowScretch);
    rateablySize->setTickPosition(QSlider::TicksBelow);

    startFixedWindowSizeLabel = new QLabel(tr("The size of the window at startup"), widget);
    startFixedWindowSizeLabel->setAlignment(Qt::AlignLeft);
    minimumSizeLabel = new QLabel(tr("The minimum size of the window"), widget);
    minimumSizeLabel->setAlignment(Qt::AlignLeft);
    rateablySizeLabel = new QLabel(tr("Aspect ratio (x100)"), widget);
    rateablySizeLabel->setAlignment(Qt::AlignLeft);

    startFixedShowLabelX  = new QLabel(QString::number(startFixedWindowSize.width()), widget);
    startFixedShowLabelX->setAlignment(Qt::AlignRight);
    startFixedShowLabelY  = new QLabel(QString::number(startFixedWindowSize.height()), widget);
    startFixedShowLabelY->setAlignment(Qt::AlignLeft);
    startFixedShowLabelVS  = new QLabel("x", widget);
    startFixedShowLabelVS->setAlignment(Qt::AlignCenter);

    minimumSizeShowLabelX = new QLabel(QString::number(minimumSize.width()), widget);
    minimumSizeShowLabelX->setAlignment(Qt::AlignRight);
    minimumSizeShowLabelY = new QLabel(QString::number(minimumSize.height()), widget);
    minimumSizeShowLabelY->setAlignment(Qt::AlignLeft);
    minimumSizeShowLabelVS = new QLabel("x", widget);
    minimumSizeShowLabelVS->setAlignment(Qt::AlignCenter);

    rateablySizeShowLabel = new QLabel(QString::number(rateablyWindowScretch), widget);
    rateablySizeShowLabel->setAlignment(Qt::AlignRight);

    QObject::connect(rateablySize, &QSlider::valueChanged, rateablySizeShowLabel,
                     static_cast<void (QLabel::*)(int)>(&QLabel::setNum));
    QObject::connect(minimumSizeX, &QSlider::valueChanged, minimumSizeShowLabelX,
                     static_cast<void (QLabel::*)(int)>(&QLabel::setNum));
    QObject::connect(minimumSizeY, &QSlider::valueChanged, minimumSizeShowLabelY,
                     static_cast<void (QLabel::*)(int)>(&QLabel::setNum));

    QObject::connect(startFixedWindowSizeX, &QSlider::valueChanged, startFixedShowLabelX,
                     static_cast<void (QLabel::*)(int)>(&QLabel::setNum));
    QObject::connect(startFixedWindowSizeY, &QSlider::valueChanged, startFixedShowLabelY,
                     static_cast<void (QLabel::*)(int)>(&QLabel::setNum));

    QObject::connect(rateablySize, &QSlider::valueChanged, this, &SettingsInterface::updatePoint);
    QObject::connect(minimumSizeX, &QSlider::valueChanged, this, &SettingsInterface::updatePoint);
    QObject::connect(minimumSizeY, &QSlider::valueChanged, this, &SettingsInterface::updatePoint);

    QObject::connect(startFixedWindowSizeX, &QSlider::valueChanged, this, &SettingsInterface::updatePoint);
    QObject::connect(startFixedWindowSizeY, &QSlider::valueChanged, this, &SettingsInterface::updatePoint);
}

void SettingsInterface::saveSettings()
{
    appSettings->beginGroup("Interface");
    appSettings->setValue("isFixedSizeWindow", mapItems.value("isFixedSizeWindow")->getValue());
    appSettings->setValue("isChangeSizeRateably", mapItems.value("isChangeSizeRateably")->getValue());
    appSettings->setValue("isAlwaysShowWindow", mapItems.value("isAlwaysShowWindow")->getValue());
    appSettings->setValue("isFloatLoginWindow", mapItems.value("isFloatLoginWindow")->getValue());
    appSettings->setValue("isFixedStartWindowSize", mapItems.value("isFixedStartWindowSize")->getValue());

    appSettings->setValue("startFixedWindowSize", QSize(startFixedWindowSizeX->value(),
                                                    startFixedWindowSizeY->value()));
    appSettings->setValue("minimumSize", QSize(minimumSizeX->value(), minimumSizeY->value()));
    appSettings->setValue("rateablyWindowScretch", rateablySize->value());
    appSettings->endGroup();

    appBind->setGlobalSignal(SM_CF::unite({"settings", "save"}),
                             SM_CF::unite({"client", "settings_interface", "save_settings"}),
                             "save settings");
}

void SettingsInterface::setValue(bool isDefault)
{
    if (isDefault)
        appBind->setSignal(SM_CF::unite({"settings", "set_default"}),
                           SM_CF::unite({"client", "settings_interface", "set_default"}),
                           "Interface");
    appSettings->beginGroup("Interface");
    mapItems.value("isFixedSizeWindow")->setValue(appSettings->value("isFixedSizeWindow", false).toBool());
    mapItems.value("isChangeSizeRateably")->setValue(appSettings->value("isChangeSizeRateably", false).toBool());
    mapItems.value("isAlwaysShowWindow")->setValue(appSettings->value("isAlwaysShowWindow", false).toBool());
    mapItems.value("isFloatLoginWindow")->setValue(appSettings->value("isFloatLoginWindow", true).toBool());
    mapItems.value("isFixedStartWindowSize")->setValue(appSettings->value("isFixedStartWindowSize", false).toBool());
    QSize size = appSettings->value("startFixedWindowSize", QSize(600, 500)).toSize();
    startFixedWindowSizeX->setValue(size.width());
    startFixedWindowSizeY->setValue(size.height());
    size = appSettings->value("minimumSize", QSize(500, 500)).toSize();
    minimumSizeX->setValue(size.width());
    minimumSizeY->setValue(size.height());
    rateablySize->setValue(appSettings->value("rateablyWindowScretch", 75).toInt());
    appSettings->endGroup();
    updatePoint();
}

void SettingsInterface::updatePoint()
{
    minimumSizeY->setValue(10*minimumSizeX->value()/rateablySize->value());
    minimumSizeShowLabelY->setNum(minimumSizeY->value());
    startFixedWindowSizeY->setValue(10*startFixedWindowSizeX->value()/rateablySize->value());
    startFixedShowLabelY->setNum(startFixedWindowSizeY->value());
    //tempSize = QSize(tempSize.width(), (10*tempSize.width())/rateablySize->value());
    minimumSizeY->setValue(10*minimumSizeX->value()/rateablySize->value());
    minimumSizeShowLabelY->setNum(minimumSizeY->value());
    startFixedWindowSizeY->setValue(10*startFixedWindowSizeX->value()/rateablySize->value());
    startFixedShowLabelY->setNum(startFixedWindowSizeY->value());
    startFixedWindowSizeX->setMinimum(minimumSizeX->value());
    startFixedWindowSizeY->setMinimum(minimumSizeY->value());
    resizeContent(this->size());
}

void SettingsInterface::resizeContent(QSize size)
{
    int w = size.width();
    int h(3);
    for (auto s : mapItems){
        s->setFixedSize(w, 25);
        s->move(0, h);
        h += 30;
    }

    minimumSizeLabel->setFixedSize(w/2 - 5, 25);
    minimumSizeLabel->move(0, h);
    minimumSizeShowLabelX->setFixedSize(55, 25);
    minimumSizeShowLabelX->move(minimumSizeLabel->width() + 5, h);
    minimumSizeShowLabelVS->setFixedSize(10, 25);
    minimumSizeShowLabelVS->move(minimumSizeLabel->width() + 60, h);
    minimumSizeShowLabelY->setFixedSize(55, 25);
    minimumSizeShowLabelY->move(minimumSizeLabel->width() + 70, h);
    minimumSizeX->move(120 + minimumSizeLabel->width() + 5, h);

    if (mapItems.value("isChangeSizeRateably")->getValue()){
        minimumSizeX->setFixedSize(w/2 - 120, 25);
        minimumSizeY->setVisible(false);
        h += 30;
        rateablySize->setVisible(true);
        rateablySizeLabel->setVisible(true);
        rateablySizeShowLabel->setVisible(true);
        rateablySizeLabel->setFixedSize(w/2 - 5, 25);
        rateablySizeLabel->move(0, h);
        rateablySizeShowLabel->setFixedSize(50, 25);
        rateablySizeShowLabel->move(5 + rateablySizeLabel->width(), h);
        rateablySize->setFixedSize(w - (5 + rateablySizeLabel->width()+rateablySizeShowLabel->width()), 25);
        rateablySize->move(5 + rateablySizeLabel->width() + rateablySizeShowLabel->width(), h);
    }
    else {
        if (w/2 - 120 > 0){
            minimumSizeX->setFixedSize((w/2 - 120) / 2, 25);
            minimumSizeY->setFixedSize(minimumSizeX->size());
        }
        minimumSizeY->move(120 + minimumSizeLabel->width() + minimumSizeX->width() + 5, h);
        minimumSizeY->setVisible(true);
        rateablySize->setVisible(false);
        rateablySizeLabel->setVisible(false);
        rateablySizeShowLabel->setVisible(false);
    }

    h += 30;
    if (mapItems.value("isFixedStartWindowSize")->getValue()){
        startFixedShowLabelX->setVisible(true);
        startFixedShowLabelY->setVisible(true);
        startFixedShowLabelVS->setVisible(true);
        startFixedWindowSizeLabel->setVisible(true);
        startFixedWindowSizeX->setVisible(true);
        startFixedWindowSizeY->setVisible(true);

        startFixedWindowSizeLabel->setFixedSize(w/2 - 5, 25);
        startFixedWindowSizeLabel->move(0, h);
        startFixedShowLabelX->setFixedSize(55, 25);
        startFixedShowLabelX->move(minimumSizeLabel->width() + 5, h);
        startFixedShowLabelVS->setFixedSize(10, 25);
        startFixedShowLabelVS->move(minimumSizeLabel->width() + 60, h);
        startFixedShowLabelY->setFixedSize(55, 25);
        startFixedShowLabelY->move(minimumSizeLabel->width() + 70, h);
        if (mapItems.value("isChangeSizeRateably")->getValue()){
            startFixedWindowSizeX->setFixedSize(widget->width()/2 - 120, 25);
            startFixedWindowSizeY->setVisible(false);
        }
        else {
            startFixedWindowSizeX->setFixedSize((w/2 - 120) / 2, 25);
            startFixedWindowSizeY->setFixedSize(minimumSizeX->size());
            startFixedWindowSizeY->move(120 + startFixedWindowSizeLabel->width() + startFixedWindowSizeX->width() + 5, h);
            startFixedWindowSizeY->setVisible(true);
        }
        startFixedWindowSizeX->move(120 + minimumSizeLabel->width() + 5, h);
        h += 30;
    }
    else {
        startFixedShowLabelX->setVisible(false);
        startFixedShowLabelY->setVisible(false);
        startFixedShowLabelVS->setVisible(false);
        startFixedWindowSizeLabel->setVisible(false);
        startFixedWindowSizeX->setVisible(false);
        startFixedWindowSizeY->setVisible(false);
    }

    h += 30;
    downButtons->setFixedSize(w, 25);
    downButtons->move(0, h);
    h += 35;

    area->setFixedSize(size);
    widget->setFixedSize(w, h);
}

void SettingsInterface::resizeEvent(QResizeEvent *event)
{
    resizeContent(event->size());
}
