#include "settingsmodule.h"

SettingsModule::SettingsModule(QWidget *parent) : QWidget(parent)
{

    appBind->setSignal("set_current_way",
                       SM_CF::unite({"client", "settings_module"}),
                       SM_CF::unite({"settings", "module"}));
}
