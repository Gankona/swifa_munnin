#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QResizeEvent>
#include <QPaintEvent>
#include <QPainter>

#include "sm_marginwidget.h"
#include "sm_deploywidgetmenu.h"
#include "sm_staticwidgetfunction.h"

#include "ApplicationBind"
#include "settingsaccount.h"
#include "settingsbasic.h"
#include "settingshotkey.h"
#include "settingsinterface.h"
#include "settingsmodule.h"
#include "settingsmultimedia.h"

class SettingsWidget : public QFrame
{
    Q_OBJECT
private:
    SettingsAccount *account;
    SettingsBasic *basic;
    SettingsHotKey *hotKey;
    SettingsInterface *interfaces;
    SettingsModule *module;
    SettingsMultimedia *multimedia;

    SM_MarginWidget *mainWidget;
    SM_DeployWidgetMenu *menu;

    int spaces;

    void openSettingWindow(QString key);
    void updatePalette();

protected:
    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

public:
    explicit SettingsWidget(QWidget *parent = nullptr);
};

#endif // SETTINGSWIDGET_H
