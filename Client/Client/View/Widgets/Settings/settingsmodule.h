#ifndef SETTINGSMODULE_H
#define SETTINGSMODULE_H

#include <QWidget>
#include "ApplicationBind"
#include "sm_staticcorefunction.h"

class SettingsModule : public QWidget
{
    Q_OBJECT
public:
    explicit SettingsModule(QWidget *parent = nullptr);

signals:

public slots:
};

#endif // SETTINGSMODULE_H
