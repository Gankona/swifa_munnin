#include "settingsmultimedia.h"

SettingsMultimedia::SettingsMultimedia(QWidget *parent) : QWidget(parent)
{
    appBind->setSignal("set_current_way",
                       SM_CF::unite({"client", "settings_multimedia"}),
                       SM_CF::unite({"settings", "multimedia"}));
}
