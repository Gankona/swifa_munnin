#include "settingsaccount.h"

SettingsAccount::SettingsAccount(QWidget *parent)
    : QWidget(parent)
{
    widget = new QWidget;
    area = new QScrollArea(this);
    area->show();
    area->setWidget(widget);
    area->setFrameShape(QFrame::NoFrame);
    area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    downButtons = new SM_SettingsButtons(widget);

    createProfile();
    createPassword();
    createServer();

    appBind->setSignal("set_current_way",
                       SM_CF::unite({"client", "settings_account"}),
                       SM_CF::unite({"settings", "account"}));
}

void SettingsAccount::createProfile()
{
    editProfileLabel = new QLabel(QString("profile").leftJustified(150, '_'), widget);
    nameEdit = new SM_LabelEdit("name", "login", widget);
    surnameEdit = new SM_LabelEdit("surname", "surname", widget);
    phoneEdit = new SM_LabelEdit("phone", "phone", widget);
}

void SettingsAccount::createPassword()
{
    passwordEditLabel = new QLabel(QString("password").leftJustified(150, '_'), widget);
}

void SettingsAccount::createServer()
{
    serverEditLabel = new QLabel(QString("server").leftJustified(150, '_'), widget);
    ipServerEdit = new SM_LabelEdit("ip server", "127.0.0.1", widget);
    portEdit = new SM_LabelEdit("port", "36556", widget);
}

void SettingsAccount::resizeContent(QSize size)
{
    int h(0);
    editProfileLabel->setFixedSize(size.width(), 20);
    editProfileLabel->move(0, h);
    h += 22;
    nameEdit->setFixedSize(size.width(), 25);
    nameEdit->move(0, h);
    h += 27;
    surnameEdit->setFixedSize(size.width(), 25);
    surnameEdit->move(0, h);
    h += 27;
    phoneEdit->setFixedSize(size.width(), 25);
    phoneEdit->move(0, h);
    h += 37;

    passwordEditLabel->setFixedSize(size.width(), 20);
    passwordEditLabel->move(0, h);
    h += 32;

    serverEditLabel->setFixedSize(size.width(), 20);
    serverEditLabel->move(0, h);
    h += 22;
    ipServerEdit->setFixedSize(size.width(), 25);
    ipServerEdit->move(0, h);
    h += 27;
    portEdit->setFixedSize(size.width(), 25);
    portEdit->move(0, h);
    h += 47;

    downButtons->setFixedSize(size.width(), 25);
    downButtons->move(0, h);
    h += 27;

    area->setFixedSize(size);
    widget->setFixedSize(size.width(), h);
    widget->move(0, 0);
}

void SettingsAccount::resizeEvent(QResizeEvent *event)
{
    resizeContent(event->size());
}
