#ifndef SETTINGSHOTKEY_H
#define SETTINGSHOTKEY_H

#include <QtGui/QKeyEvent>
#include <QtGui/QResizeEvent>
#include <QtWidgets>

#include "ApplicationBind"

#include "sm_settingsbuttons.h"

class SettingsHotKey : public QWidget
{
    Q_OBJECT
private:
    QScrollArea *area;
    QWidget *widget;
    QLabel *titleLabel;
    QMap <QString, QLabel*> mapLabel;
    QMap <QString, QPushButton*> mapButton;
    QHBoxLayout *hBox;
    SM_SettingsButtons *downButtons;
    QString buttonTextBuffer;
    QPushButton *buttonBuffer;

    int currentRow;

    inline void createOneHotKey(QString name, QString nameKey, QString keyCombo);
    inline void createHotKey();

protected:
    void resizeEvent(QResizeEvent *event);
    void keyPressEvent(QKeyEvent *event);

public:
    explicit SettingsHotKey(QWidget *parent = nullptr);
};

#endif // SETTINGSHOTKEY_H
