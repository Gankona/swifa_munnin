#include "settingshotkey.h"

SettingsHotKey::SettingsHotKey(QWidget *parent)
    : QWidget(parent)
{
    buttonBuffer = nullptr;
    buttonTextBuffer = "";
    mapButton.clear();
    mapLabel.clear();

    widget = new QWidget;
    area = new QScrollArea(this);
    area->show();
    area->setWidget(widget);
    area->setFrameShape(QFrame::NoFrame);
    area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    downButtons = new SM_SettingsButtons(widget);

    createHotKey();
    appBind->setSignal("set_current_way",
                       SM_CF::unite({"client", "settings_hot_key"}),
                       SM_CF::unite({"settings", "hot key"}));
}

void SettingsHotKey::createOneHotKey(QString name, QString nameKey, QString keyCombo)
{
    mapLabel.insert(nameKey, new QLabel(name, widget));
    QPushButton *b = SM_WF::button(keyCombo, widget, true, true);
    mapButton.insert(nameKey, b);
    QObject::connect(b, &QPushButton::clicked, this, [=](){
        if (buttonBuffer != nullptr)
            buttonBuffer->setText(buttonTextBuffer);
        buttonBuffer = b;
        buttonTextBuffer = b->text();
        for (auto b : mapButton)
            if (b->isChecked())
                b->setChecked(false);
        b->setChecked(true);
        b->setText("");
    });
}

void SettingsHotKey::createHotKey()
{
    createOneHotKey(tr("Открыть дом"), "openHome", "asas");
    createOneHotKey(tr("Открыть настройки"), "openSetting", "efes");
    createOneHotKey(tr("Закрыть приложение"), "closeApp", "qdd");
}

void SettingsHotKey::resizeEvent(QResizeEvent *event)
{
    area->setFixedSize(event->size());
    widget->setFixedWidth(event->size().width());
    int i(0);
    foreach (QString k, mapButton.keys()){
        mapButton.value(k)->setFixedSize(135, 30);
        mapButton.value(k)->move(widget->width() - 140, i * 31);
        i++;
    }
    i = 0;
    foreach (QString k, mapLabel.keys()){
        mapLabel.value(k)->setFixedSize(widget->width() - 150, 30);
        mapLabel.value(k)->move(5, i * 31);
        i++;
    }
    downButtons->setFixedSize(widget->width(), 25);
    downButtons->move(0, i * 31 + 31);
    widget->setFixedHeight(i * 31 + 60);
    widget->move(0, 0);
}

void SettingsHotKey::keyPressEvent(QKeyEvent *event)
{
    qDebug() << "hot_key" << event;
    if (event->key() == Qt::Key_Escape || event->key() == Qt::Key_Return){
        buttonBuffer->setText(buttonTextBuffer);
        buttonBuffer = nullptr;
        buttonTextBuffer.clear();
        event->accept();
        return;
    }
    if (SM_CF::getKeyValue(event->key()) == "")
        return;
    for (auto k : mapButton)
        if (k->text() == SM_CF::getKeyValue(event->key(), event->modifiers())){
            event->accept();
            return;
        }
    qDebug() << "live";
    for (auto k : mapButton)
        if (k->isChecked()){
            buttonBuffer = nullptr;
            buttonTextBuffer = "";
            k->setChecked(false);
            k->setText(SM_CF::getKeyValue(event->key(), event->modifiers()));
            break;
        }
    event->accept();
}
