#include "settingsbasic.h"

SettingsBasic::SettingsBasic(QWidget *parent)
    : QWidget(parent)
{
    appBind->setSignal("set_current_way",
                       SM_CF::unite({"client", "settings_account"}),
                       SM_CF::unite({"settings", "account"}));
}
