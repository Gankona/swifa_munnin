#ifndef SETTINSGINTERFACE_H
#define SETTINSGINTERFACE_H

#include <QtCore/QSettings>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QWidget>

#include "sm_staticcorefunction.h"
#include "sm_settingsbuttons.h"
#include "sm_switcherbutton.h"

#include "ApplicationBind"
#include "ApplicationSettings"

class SettingsInterface : public QWidget
{
    Q_OBJECT
private:
    QScrollArea *area;
    QWidget *widget;
    SM_SettingsButtons *downButtons;
    QMap <QString, SM_SwitcherButton*> mapItems;

    QSlider *startFixedWindowSizeX;
    QSlider *startFixedWindowSizeY;
    QSlider *minimumSizeX;
    QSlider *minimumSizeY;
    QSlider *rateablySize;

    QLabel *rateablySizeLabel;
    QLabel *startFixedWindowSizeLabel;
    QLabel *minimumSizeLabel;
    QLabel *startFixedShowLabelX;
    QLabel *startFixedShowLabelY;
    QLabel *startFixedShowLabelVS;
    QLabel *minimumSizeShowLabelX;
    QLabel *minimumSizeShowLabelY;
    QLabel *minimumSizeShowLabelVS;
    QLabel *rateablySizeShowLabel;

    inline void createOnePoint(QString name, QString nameKey);
    inline void createPoint();
    inline void createSlider();
    void updatePoint();
    void resizeContent(QSize size);
    void saveSettings();
    void setValue(bool isDefault = true);

protected:
    void resizeEvent(QResizeEvent *event);

public:
    explicit SettingsInterface(QWidget *parent = nullptr);
};

#endif // SETTINGINTERFACE_H
