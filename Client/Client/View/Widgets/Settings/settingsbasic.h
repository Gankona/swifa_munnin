#ifndef SETTINGSBASIC_H
#define SETTINGSBASIC_H

#include "ApplicationBind"

#include <QtWidgets/QWidget>

class SettingsBasic : public QWidget
{
    Q_OBJECT
public:
    SettingsBasic(QWidget *parent = nullptr);
};

#endif // SETTINGSBASIC_H
