#ifndef UPPANELWIDGET_H
#define UPPANELWIDGET_H

#include "ApplicationBind"

#include "sm_slashbuttonpanel.h"

class UpPanelWidget : public SM_SlashButtonPanel
{
    Q_OBJECT
private:
    void catchLink(QString link);

public:
    UpPanelWidget(QWidget *parent = nullptr);

signals:
    void openAccount();
    void closeAccount();
};

#endif // UPPANELWIDGET_H
