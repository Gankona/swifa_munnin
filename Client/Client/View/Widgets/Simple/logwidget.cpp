#include "logwidget.h"

LogWidget::LogWidget(QWidget *parent)
    : QFrame(parent),
      buffer(70),
      fullScreen(false)
{
    QSize bS = QSize (20, 20);
    senderBox = new QComboBox(this);
    receiverBox = new QComboBox(this);
    area = new QLabel(this);
    area->setAlignment(Qt::AlignBottom);
    scrollArea = new QScrollArea(this);
    scrollArea->setWidget(area);
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    panel = new QFrame(this);
    logTitle = SM_WF::label("Log", Qt::AlignVCenter, panel);
    debugButton = SM_WF::iconButton(style()->standardIcon(QStyle::SP_MessageBoxInformation),
                                    "debug", panel, bS, false, true);
    warningButton = SM_WF::iconButton(style()->standardIcon(QStyle::SP_MessageBoxWarning), "warning",
                                      panel, bS, false, true);
    criticalButton = SM_WF::iconButton(style()->standardIcon(QStyle::SP_MessageBoxCritical), "critical",
                                       panel, bS, false, true);
    timeButton = SM_WF::iconButton(QIcon(":/Images/time.png"), "critical",
                                   panel, bS, false, true);
    closeButton = SM_WF::iconButton(style()->standardIcon(QStyle::SP_DialogCloseButton), "close Log", panel);
    fullButton = SM_WF::iconButton(style()->standardIcon(QStyle::SP_ArrowUp), "critical", panel);
    leftTimeEdit = new QDateTimeEdit(QDateTime::currentDateTime(), panel);
    rightTimeEdit = new QDateTimeEdit(QDateTime::currentDateTime(), panel);
    leftTimeEdit->setVisible(false);
    rightTimeEdit->setVisible(false);
    warningButton->setChecked(true);
    criticalButton->setChecked(true);
    debugButton->setChecked(true);
    senderBox->addItem("All");
    receiverBox->addItem("All");

    QObject::connect(appBind, &SM_BindManager::logUpdate, this, &LogWidget::rewriteLog);
    QObject::connect(closeButton, &QPushButton::clicked, this, &LogWidget::closeLog);
    QObject::connect(fullButton, &QPushButton::clicked, this, [=](){
        fullScreen ? fullButton->setIcon(style()->standardIcon(QStyle::SP_ArrowUp))
                   : fullButton->setIcon(style()->standardIcon(QStyle::SP_ArrowDown));
        fullScreen = !fullScreen;
        emit reSizeLog();
    });
}

void LogWidget::rewriteLog(SM::TypeMsg type,
                           QString message,
                           QString key,
                           QString sender,
                           QString receiver)
{
    switch (type){
    case SM::TypeMsg::critical:
        if (! criticalButton->isChecked())
            return;
        break;
    case SM::TypeMsg::debug:
        if (! debugButton->isChecked())
            return;
        break;
    case SM::TypeMsg::warning:
        if (! warningButton->isChecked())
            return;
        break;
        default:;
    }
    if (senderBox->currentText() != "All")
        if (senderBox->currentText() != sender)
            return;
    if (receiverBox->currentText() != "All")
        if (receiverBox->currentText() != receiver)
            return;
    if (timeButton->isChecked())
        if (rightTimeEdit->dateTime().secsTo(QDateTime::currentDateTime()) > 0)
            return;

    QString strTime(QDateTime::currentDateTime().toString("hh:mm:ss dd/MM/yy").leftJustified(18, '.'));
    QString strType(SM::typeMsgToString(type).leftJustified(9, '.'));
    QString strKey(key.leftJustified(15, '.'));
    QString strSender(sender.leftJustified(15, '.'));
    QString strReceiver(receiver.leftJustified(15, '.'));
    QString strMessage(message.leftJustified(50, '.'));
    area->setText(area->text() + '\n'
                  + strTime + strType + strKey
                  + strSender + strReceiver + strMessage);
}

void LogWidget::resizeEvent(QResizeEvent *event)
{
    resizeContent(event->size());
}

void LogWidget::resizeContent(QSize size)
{
    QSize bS = QSize(20, 20);
    panel->setFixedSize(size.width(), bS.height());
    panel->move(0, 0);
    closeButton->setFixedSize(bS);
    closeButton->move(size.width() - bS.width(), 0);
    fullButton->setFixedSize(bS);
    fullButton->move(closeButton->pos().x() - bS.width(), 0);
    senderBox->setFixedSize((size.width()-bS.width()*6-50)/4, bS.height());
    senderBox->move(fullButton->pos().x() - senderBox->width(), 0);
    receiverBox->setFixedSize(senderBox->size());
    receiverBox->move(senderBox->pos().x() - receiverBox->width(), 0);
    criticalButton->setFixedSize(bS);
    criticalButton->move(receiverBox->pos().x() - bS.width(), 0);
    warningButton->setFixedSize(bS);
    warningButton->move(criticalButton->pos().x() - bS.width(), 0);
    debugButton->setFixedSize(bS);
    debugButton->move(warningButton->pos().x() - bS.width(), 0);
    timeButton->setFixedSize(bS);
    timeButton->move(debugButton->pos().x() - bS.width(), 0);
    rightTimeEdit->setFixedSize(senderBox->size());
    rightTimeEdit->move(timeButton->pos().x() - rightTimeEdit->width(), 0);
    leftTimeEdit->setFixedSize(senderBox->size());
    leftTimeEdit->move(rightTimeEdit->pos().x() - leftTimeEdit->width(), 0);
    logTitle->setFixedSize(50, bS.height());
    logTitle->move(5, 0);
    area->setFixedSize(size.width(), 2000);
    scrollArea->setFixedSize(size.width(), size.height() - bS.height());
    scrollArea->move(0, bS.height());
}

void LogWidget::setBuffer(int buffer)
{
    this->buffer = buffer;
}

bool LogWidget::isLogFullScreen()
{
    return fullScreen;
}
