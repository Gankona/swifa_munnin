#include "uppanelwidget.h"

UpPanelWidget::UpPanelWidget(QWidget *parent)
    : SM_SlashButtonPanel(parent)
{
    QObject::connect(appBind, &SM_BindManager::signalInternal, this, [=]
                    (QString sendKey,
                     QString message,
                     QString deliveryKey){
        if (SM_CF::getTier(sendKey) == "account"
                && SM_CF::getTier(sendKey, 1) == "login"){
            QString login("");
            if (SM_CF::getTier(message) == "NoName")
                login = "NoName";
            else
                login = SM_CF::getTier(message) + " ("
                        + SM_CF::getTier(message, 1) + ")";
            clearSecondList();
            addSecondSlashButton(login, "login");
            closeAccount();
        }
        else if (SM_CF::getTier(sendKey) == "set_current_way"){
            if (deliveryKey == "")
                deliveryKey = message;
            clearFirstList();
            for (int i = 0; i < SM_CF::getTierLenght(message); i++)
                addFirstSlashButton(SM_CF::getTier(message, i),
                                    SM_CF::getTierInterval(deliveryKey, 0, i));
        }
    });
    QObject::connect(this, &UpPanelWidget::linkActivate, this, &UpPanelWidget::catchLink);
}

void UpPanelWidget::catchLink(QString link)
{
    if (link == "login")
        openAccount();
    appBind->setLink(link, SM_CF::unite({"client", "up_panel_widget"}));
}
