#ifndef LOGWIDGET_H
#define LOGWIDGET_H

#include <QWidget>
#include <QFrame>
#include <QStyle>
#include <QtWidgets>
#include "sm_staticwidgetfunction.h"
#include "ApplicationBind"

class LogWidget : public QFrame
{
    Q_OBJECT
private:

    int buffer;
    bool fullScreen;

    QLabel *logTitle;
    QLabel *area;
    QFrame *panel;
    QScrollArea *scrollArea;
    QComboBox *senderBox;
    QComboBox *receiverBox;
    QPushButton *debugButton;
    QPushButton *warningButton;
    QPushButton *criticalButton;
    QPushButton *closeButton;
    QPushButton *fullButton;
    QPushButton *timeButton;
    QDateTimeEdit *leftTimeEdit;
    QDateTimeEdit *rightTimeEdit;

    void rewriteLog(SM::TypeMsg type,
                    QString message,
                    QString key,
                    QString sender,
                    QString receiver);

    void resizeContent(QSize size);

    void resizeEvent(QResizeEvent *event);

public:
    explicit LogWidget(QWidget *parent = nullptr);

    bool isLogFullScreen();
    void setBuffer(int buffer);

signals:
    void closeLog();
    void reSizeLog();
};

#endif // LOGWIDGET_H
