#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore/QDateTime>
#include <QtCore/QTimer>
#include <QtGui/QKeyEvent>
#include <QtGui/QShowEvent>
#include <QtWidgets/QApplication>
#include <QtWidgets/QStyle>
#include <QtWidgets/QtWidgets>

#include "View/Widgets/Account/accountmanagerwidget.h"
#include "View/Widgets/Simple/homewidget.h"
#include "View/Widgets/Simple/hotkeywidget.h"
#include "View/Widgets/Simple/logwidget.h"
#include "View/Widgets/Simple/uppanelwidget.h"
#include "View/Widgets/Settings/settingswidget.h"
#include "View/Widgets/Info/infowidget.h"

#include "ApplicationBind"

#include "sm_contentwidget.h"
#include "sm_panelbarwidget.h"
#include "sm_staticcorefunction.h"
#include "sm_staticwidgetfunction.h"

class MainWindow : public QWidget
{
    Q_OBJECT
private:
    enum class EnumWidget {
        null = 0,
        home = 1,
        info = 2,
        hot_key = 3,
        settings = 4,
        content = 5,
    };

    EnumWidget widgetStatus;
    QSettings settings;
    UpPanelWidget *upPanelWidget;
    LogWidget *logWidget;
    AccountManagerWidget *accountWidget;
    QWidget *mainWidget;

    InfoWidget *infoWidget;
    HomeWidget *homeWidget;
    HotKeyWidget *hotKeyWidget;
    SettingsWidget *settingsWidget;
    SM_ContentWidget *contentWidget;

    QPushButton *settingsButton;
    QPushButton *hotKeyButton;
    QPushButton *homeButton;
    QPushButton *exitButton;
    QPushButton *infoButton;
    QPushButton *logButton;
    int space;
    QSize bSize;

    SM_PanelBarWidget *leftPanel;

    inline void createConnect();
    inline void createLeftPanel();
    bool closeAccountWidget();
    void openLink(QString link);
    void updatePalette();
    void updateWindow();

protected:
    void showEvent(QShowEvent*);
    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent*);
    void keyPressEvent(QKeyEvent *event){ qDebug() << event->nativeModifiers() << event->nativeVirtualKey() << event->modifiers(); }

public:
    explicit MainWindow(QWidget *parent = nullptr);

signals:
    SM_AccountInfo getAccountInfo();
};

#endif // MAINWINDOW_H
