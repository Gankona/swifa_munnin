#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent),
      widgetStatus(EnumWidget::null),
      settings(),
      logWidget(nullptr),
      accountWidget(nullptr),
      infoWidget(nullptr),
      homeWidget(nullptr),
      hotKeyWidget(nullptr),
      settingsWidget(nullptr),
      space(2),
      bSize(25, 25)
{
    mainWidget = new QWidget(this);

    homeButton = SM_WF::iconButton(style()->standardIcon(QStyle::SP_DirHomeIcon), "home", this);
    homeButton->setFlat(true);

    upPanelWidget = new UpPanelWidget(this);

    contentWidget = new SM_ContentWidget(mainWidget);
    contentWidget->setStyleSheet("background: navy");

    createLeftPanel();
    createConnect();
    updatePalette();
    updateWindow();

    this->setMinimumSize(500, 520);
    this->resize(500, 520);
    this->move(650, 100);
    this->setWindowTitle("Swifa Munnin");
}

void MainWindow::updatePalette()
{
    /*QPalette pal;

    pal.setColor(QPalette::Button, appPalette->getMainPanel()->button());
    homeButton->setPalette(pal);

    pal.setColor(QPalette::Button, appPalette->getOtherPanel()->button());
    pal.setColor(QPalette::ButtonText, appPalette->getOtherPanel()->text());
    pal.setColor(QPalette::WindowText, appPalette->getOtherPanel()->text());
    pal.setColor(QPalette::Background, appPalette->getOtherPanel()->background());
    leftPanel->setPalette(pal);

    pal.setColor(QPalette::Button, appPalette->getMainPanel()->button());
    pal.setColor(QPalette::ButtonText, appPalette->getMainPanel()->text());
    pal.setColor(QPalette::WindowText, appPalette->getMainPanel()->text());
    pal.setColor(QPalette::Background, appPalette->getMainPanel()->background());
    upPanelWidget->setPalette(pal);

    repaint();*/
}

void MainWindow::createConnect()
{
    QObject::connect(homeButton, &QPushButton::clicked, this, [=](){
        widgetStatus == EnumWidget::home
                ? widgetStatus = EnumWidget::null
                : widgetStatus = EnumWidget::home;
        updateWindow();
    });
    QObject::connect(settingsButton, &QPushButton::clicked, this, [=](){
        widgetStatus == EnumWidget::settings
                ? widgetStatus = EnumWidget::home
                : widgetStatus = EnumWidget::settings;
        updateWindow();
    });
    QObject::connect(infoButton, &QPushButton::clicked, this, [=](){
        widgetStatus == EnumWidget::info
                ? widgetStatus = EnumWidget::home
                : widgetStatus = EnumWidget::info;
        updateWindow();
    });
    QObject::connect(hotKeyButton, &QPushButton::clicked, this, [=](){
        widgetStatus == EnumWidget::hot_key
                ? widgetStatus = EnumWidget::home
                : widgetStatus = EnumWidget::hot_key;
        updateWindow();
    });

    QObject::connect(upPanelWidget, &UpPanelWidget::openAccount, this, [=](){
        if (! closeAccountWidget()){
            accountWidget = new AccountManagerWidget(true, getAccountInfo(), this);
            accountWidget->show();
            accountWidget->raise();
            resizeEvent(new QResizeEvent(this->size(), this->size()));
            resizeEvent(new QResizeEvent(this->size(), this->size()));
            QObject::connect(accountWidget, &AccountManagerWidget::repaintContent, this, [=](){
                resizeEvent(new QResizeEvent(this->size(), this->size()));
            });
            QObject::connect(accountWidget, &AccountManagerWidget::closeAccountWidget, this, &MainWindow::closeAccountWidget);
            appBind->setMessage(SM::TypeMsg::debug,
                                SM_CF::unite({"client", "main_window", "open_account"}),
                                "account widget create");
        }
    });
    QObject::connect(upPanelWidget, &UpPanelWidget::closeAccount, this, &MainWindow::closeAccountWidget);
    QObject::connect(exitButton, &QPushButton::clicked, qApp, &QApplication::quit);
    QObject::connect(logButton, &QPushButton::clicked, this, [=](){
        if (logWidget == nullptr){
            logWidget = new LogWidget(this);
            logWidget->show();
            logWidget->raise();
            QObject::connect(logWidget, &LogWidget::closeLog, logButton, &QPushButton::click);
            QObject::connect(logWidget, &LogWidget::reSizeLog, this, [=](){
                resizeEvent(new QResizeEvent(this->size(), this->size()));
            });
        }
        else {
            delete logWidget;
            logWidget = nullptr;
        }
        resizeEvent(new QResizeEvent(this->size(), this->size()));
    });
    QObject::connect(appBind, &SM_BindManager::openLink, this, &MainWindow::openLink);
}

void MainWindow::updateWindow()
{
    if (widgetStatus == EnumWidget::content){
        if (contentWidget->isEmpty())
            widgetStatus = EnumWidget::null;
        else if (! contentWidget->isVisible())
            contentWidget->setVisible(true);
    }
    else if (contentWidget->isVisible())
            contentWidget->setVisible(false);

    if (widgetStatus == EnumWidget::home){
        if (homeWidget == nullptr){
            appBind->setSignal("set_current_way",
                               SM_CF::unite({"client", "main_window", "update_window"}),
                               SM_CF::unite({"home"}));
            homeWidget = new HomeWidget(mainWidget);
            homeWidget->show();
        }
    }
    else {
        if (homeWidget != nullptr){
            delete homeWidget;
            homeWidget = nullptr;
        }
    }

    if (widgetStatus == EnumWidget::hot_key){
        if (hotKeyWidget == nullptr){
            appBind->setSignal("set_current_way",
                               SM_CF::unite({"client", "main_window", "update_window"}),
                               SM_CF::unite({"hot key"}));
            hotKeyWidget = new HotKeyWidget(mainWidget);
            hotKeyWidget->show();
        }
    }
    else {
        if (hotKeyWidget != nullptr){
            delete hotKeyWidget;
            hotKeyWidget = nullptr;
        }
    }

    if (widgetStatus == EnumWidget::info){
        if (infoWidget == nullptr){
            appBind->setSignal("set_current_way",
                               SM_CF::unite({"client", "main_window", "update_window"}),
                               SM_CF::unite({"info"}));
            infoWidget = new InfoWidget(mainWidget);
            infoWidget->show();
        }
    }
    else {
        if (infoWidget != nullptr){
            delete infoWidget;
            infoWidget = nullptr;
        }
    }

    if (widgetStatus == EnumWidget::settings){
        if (settingsWidget == nullptr){
            appBind->setSignal("set_current_way",
                               SM_CF::unite({"client", "main_window", "update_window"}),
                               SM_CF::unite({"settings"}));
            settingsWidget = new SettingsWidget(mainWidget);
            settingsWidget->show();
        }
    }
    else {
        if (settingsWidget != nullptr){
            delete settingsWidget;
            settingsWidget = nullptr;
        }
    }

    if (widgetStatus == EnumWidget::null){
        if (contentWidget->isEmpty()){
            if (! contentWidget->isVisible())
                contentWidget->setVisible(true);
        }
        else {
            widgetStatus = EnumWidget::home;
            updateWindow();
        }
    }

    //closeAccountWidget();
    resizeEvent(new QResizeEvent(this->size(), this->size()));
}

void MainWindow::createLeftPanel()
{
    leftPanel = new SM_PanelBarWidget(this);
    leftPanel->setAutoFillBackground(true);

    logButton = SM_WF::iconButton(QIcon(":/Images/log.png"), "open Log", this);
    exitButton = SM_WF::iconButton(QIcon(":/Images/power.png"), "exit App", this);
    infoButton = SM_WF::iconButton(QIcon(":/Images/info.png"), "info", this);
    hotKeyButton = SM_WF::iconButton(QIcon(":/Images/hotkey.png"), "hot key", this);
    settingsButton = SM_WF::iconButton(QIcon(":/Images/settings.png"), "settings", this);

    leftPanel->addToExtraList("exit", exitButton, 0);
    leftPanel->addToExtraList("settings", settingsButton, 1);
    leftPanel->addToExtraList("info", infoButton, 2);
    leftPanel->addToExtraList("hotkey", hotKeyButton, 3);
    leftPanel->addToExtraList("log", logButton, 4);
}

void MainWindow::openLink(QString link)
{
    if (SM_CF::getTierLenght(link) != 1)
        return;

    if (link == "home")
        widgetStatus = EnumWidget::home;
    else if (link == "hot_key")
        widgetStatus = EnumWidget::hot_key;
    else if (link == "info")
        widgetStatus = EnumWidget::info;
    else if (link == "settings")
        widgetStatus = EnumWidget::settings;
    else if (link == "null")
        widgetStatus = EnumWidget::null;
    else {
        // проверка на модули и аддоны
    }

    updateWindow();
}

bool MainWindow::closeAccountWidget()
{
    if (accountWidget != nullptr){
        delete accountWidget;
        accountWidget = nullptr;
        appBind->setMessage(SM::TypeMsg::debug,
                            SM_CF::unite({"client", "main_window", "close_account"}),
                            "account widget delete");
        return true;
    }
    return false;
}

void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    QColor c = this->palette().base().color();
    c.convertTo(QColor::Hsl);
    c.setHsl(c.hslHue(), c.hslSaturation(), 255 - c.lightness());

    p.setPen(QPen(QBrush(c), space, Qt::SolidLine));

    p.drawLine(QPoint(bSize.width() + space/2, 0),
               QPoint(bSize.width() + space/2, this->height()));

    p.drawLine(QPoint(this->width() - space/2, 0),
               QPoint(this->width() - space/2, this->height()));

    p.drawLine(QPoint(-4, bSize.height() + space/2),
               QPoint(this->width(), bSize.height() + space/2));

    p.drawLine(QPoint(-4, this->height() - space/2),
               QPoint(this->width(), this->height() - space/2));

    if (logWidget != nullptr)
        p.drawLine(bSize.width() + space, this->height() - 1.5*space - logWidget->height(),
                   this->width(), this->height() - 1.5*space - logWidget->height());

    if (accountWidget != nullptr)
        if (! accountWidget->getFloatEffectFlag())
            p.drawLine(QPoint(this->width() - accountWidget->width() - 1.5*space, bSize.height() + space),
                       QPoint(this->width() - accountWidget->width() - 1.5*space, this->height()));
}

void MainWindow::showEvent(QShowEvent*)
{
    resizeEvent(new QResizeEvent(this->size(), this->size()));
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    // main panel and home button
    homeButton->setFixedSize(bSize);
    homeButton->move(0, 0);
    upPanelWidget->setFixedSize(event->size().width() - bSize.width() - space, bSize.height() + space);
    upPanelWidget->move(bSize.width() + space/2, 0);
    leftPanel->setFixedSize(bSize.width(), event->size().height() - 2*space - bSize.height());
    leftPanel->move(0, bSize.height() + space);

    QSize tempPlace(this->size() - 2 * QSize(space, space) - bSize);

    // log widget
    if (logWidget != nullptr){
        if (logWidget->isLogFullScreen()){
            logWidget->setFixedSize(tempPlace);
            logWidget->move(bSize.width() + space, bSize.height() + space);
            return;
        }
        else {
            logWidget->setFixedSize(tempPlace.width(), 120);
            logWidget->move(bSize.width() + space, this->height() - logWidget->height() - space);
            tempPlace -= QSize(0, logWidget->height() + space);
        }
    }

    if (accountWidget != nullptr){
        if (accountWidget->getFloatEffectFlag()){
            accountWidget->setFixedSize(accountWidget->getSize());
            accountWidget->move(event->size().width() - accountWidget->width() - 10 - space,
                                10 + upPanelWidget->height() + space);
            accountWidget->repaint();
            mainWidget->setFixedSize(tempPlace);
        }
        else {
            accountWidget->setFixedSize(accountWidget->getSize().width(), tempPlace.height());
            accountWidget->move(event->size().width() - accountWidget->width() - space, upPanelWidget->height() + space);
            tempPlace -= QSize(accountWidget->width() + space, 0);
            mainWidget->setFixedSize(tempPlace);
        }
    }
    else {
        mainWidget->setFixedSize(tempPlace);
    }
    mainWidget->move(bSize.width() + space, bSize.height() + space);

    if (homeWidget != nullptr)
        homeWidget->setFixedSize(mainWidget->size());
    if (infoWidget != nullptr)
        infoWidget->setFixedSize(mainWidget->size());
    if (hotKeyWidget != nullptr)
        hotKeyWidget->setFixedSize(mainWidget->size());
    if (settingsWidget != nullptr)
        settingsWidget->setFixedSize(mainWidget->size());
    contentWidget->setFixedSize(mainWidget->size());
    repaint();
}
