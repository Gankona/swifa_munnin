#ifndef TRAY_H
#define TRAY_H

#include <QtCore/QObject>
#include <QtWidgets/QMenu>
#include <QtWidgets/QApplication>
#include <QtWidgets/QSystemTrayIcon>

class Tray : public QSystemTrayIcon
{
    Q_OBJECT
public:
    Tray();
    QMenu *menu;
        QList <QAction*> listAction;

    signals:
        void signalSwitchVisibleWindow();

    public slots:
        void slotClickToMenuButton();
};

#endif // TRAY_H
