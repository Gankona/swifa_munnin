TEMPLATE = subdirs

SUBDIRS += \
    SvifaMunninCore \
    SvifaMunninWidget \
    Client \
    Server

QT += gui

DISTFILES += $${PWD}/path.pri
