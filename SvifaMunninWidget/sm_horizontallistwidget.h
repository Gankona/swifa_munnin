#ifndef SM_HORIZONTALLISTWIDGET_H
#define SM_HORIZONTALLISTWIDGET_H

#include <initializer_list>

#include <QtGui/QResizeEvent>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStyle>
#include <QtWidgets/QWidget>

#include "sm_staticwidgetfunction.h"
#include "svifamunninwidget_global.h"

class SM_HorizontalListWidget : public QWidget
{
    Q_OBJECT
private:
    QString current;
    QStringList list;
    QPushButton *leftButton;
    QPushButton *rightButton;
    QLabel *valueLabel;
    bool isCircle;
    void refreshItems();

protected:
    void resizeEvent(QResizeEvent *event);

public:
    explicit SM_HorizontalListWidget(QWidget *parent = nullptr, bool isCircle = false);

    void addItem(QString item);
    void addItems(std::initializer_list <QString> items);
    void addItems(QStringList list);
    void removeItem(QString item);
    void removeItems(std::initializer_list <QString> items);
    void removeItems(QStringList list);
    void setCircleStatus(bool isCircle);

    QString currentItem();
};

#endif // SM_HORIZONTALLISTWIDGET_H
