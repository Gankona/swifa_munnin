#ifndef SM_SWITCHERBUTTON_H
#define SM_SWITCHERBUTTON_H

#include <QtGui/QResizeEvent>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QFrame>

#include "sm_staticwidgetfunction.h"
#include "svifamunninwidget_global.h"

class SMWIDGETSHARED_EXPORT SM_SwitcherButton : public QFrame
{
    Q_OBJECT
private:
    QPushButton *noButton;
    QPushButton *yesButton;
    QLabel *titleLabel;
    bool value;
    bool visibleSwitcher;

protected:
    void resizeEvent(QResizeEvent *event);

public:
    SM_SwitcherButton(QWidget *parent = nullptr, bool value = false);
    SM_SwitcherButton(QString text, QWidget *parent = nullptr, bool value = false);

    bool getValue();
    void setValue(bool value);
    void setText(QString text);
    QString getText();
    bool getVisibleSwitcher();
    void setVisibleSwitcher(bool visible);

signals:
    void valueChanged(bool);
};

#endif // SM_SWITCHERBUTTON_H
