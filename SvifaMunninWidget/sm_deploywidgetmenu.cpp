#include "sm_deploywidgetmenu.h"

SM_DeployWidgetMenu::SM_DeployWidgetMenu(QWidget *parent)
    : QFrame(parent),
      anchors(SM::Anchors::anchorsTop),
      spaces(2)
{}

void SM_DeployWidgetMenu::updateContent(QSize size)
{
    if (currentChooseKey == ""
            || ! buttonList.keys().contains(currentChooseKey)){
        if (buttonList.isEmpty())
            return;
        else {
            currentChooseKey = buttonList.firstKey();
            emit clickedMenu(currentChooseKey);
        }
    }
    int shift(0);
    for (auto s : buttonList.keys()){
        if (s == currentChooseKey)
            buttonList.value(s)->setText(" " + s);
        else
            buttonList.value(s)->setText("");
    }
    if (SM::isAnchorsHorizontal(anchors)){
        for (auto s : buttonList.keys()){
            if (SM::isAnchorsBottom(anchors))
                buttonList.value(s)->move(shift, spaces);
            else
                buttonList.value(s)->move(shift, 0);
            if (s == currentChooseKey)
                buttonList.value(s)->setFixedSize(size.width() - (size.height() + spaces)*(buttonList.keys().length() - 1),
                                                  size.height());
            else
                buttonList.value(s)->setFixedSize(size.height(), size.height());
            shift += buttonList.value(s)->width();
            shift += spaces;
        }
    }
    else {
        for (auto s : buttonList.keys()){
            if (SM::isAnchorsRight(anchors))
                buttonList.value(s)->move(spaces, shift);
            else
                buttonList.value(s)->move(0, shift);
            if (s == currentChooseKey)
                buttonList.value(s)->setFixedSize(size.width(),
                                                  size.height() - (size.width() + spaces)*(buttonList.keys().length() - 1));
            else
                buttonList.value(s)->setFixedSize(size.width(), size.width());
            shift += buttonList.value(s)->height();
            shift += spaces;
        }
    }
    repaint();
}

void SM_DeployWidgetMenu::resizeEvent(QResizeEvent *event)
{
    updateContent(event->size());
}

void SM_DeployWidgetMenu::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    QColor c = this->palette().base().color();
    c.convertTo(QColor::Hsl);
    c.setHsl(c.hslHue(), c.saturation(), 255 - c.lightness());

    QPainter painter(this);
    painter.setBrush(QBrush(c));
    painter.drawRect(this->rect());
}

void SM_DeployWidgetMenu::setAnchors(SM::Anchors anchors)
{
    this->anchors = anchors;
    updateContent(this->size());
}

SM::Anchors SM_DeployWidgetMenu::getAnchors()
{
    return anchors;
}

void SM_DeployWidgetMenu::addButtonToMenu(QPushButton *button, QString key)
{
    button->setParent(this);
    buttonList.insert(key, button);
    updateContent(this->size());
    button->setAutoFillBackground(true);
    QObject::connect(buttonList.value(key), &QPushButton::clicked, this, [=](){
        clickedMenu(key);
        currentChooseKey = key;
        updateContent(this->size());
    });
}

void SM_DeployWidgetMenu::deleteButtonFromMenu(QPushButton *button, bool deleteButton)
{
    for (auto b : buttonList)
        if (b == button){
            if (deleteButton)
                delete b;
            buttonList.remove(buttonList.key(b));
            updateContent(this->size());
            return;
        }
}

void SM_DeployWidgetMenu::deleteButtonFromMenu(QString *key, bool deleteButton)
{
    for (auto s : buttonList.keys())
        if (s == key){
            if (deleteButton)
                delete buttonList.value(s);
            buttonList.remove(s);
            updateContent(this->size());
            return;
        }
}
