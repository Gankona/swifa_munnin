#include "sm_sliderwidget.h"

SM_SliderWidget::SM_SliderWidget(QWidget *parent, QString text) : QFrame(parent)
{
    this->setFrameShape(QFrame::StyledPanel);
}

SM_SliderWidget::~SM_SliderWidget()
{
    removeAllSliders();
}

void SM_SliderWidget::resizeEvent(QResizeEvent *event){}

void SM_SliderWidget::addSlider(QSlider* slider, QString title){}

void SM_SliderWidget::addSlider(QString key, QString title, int max, int min, int interval){}

void SM_SliderWidget::addSlider(QString key, int max, int min, int interval){}

void SM_SliderWidget::removeAllSliders()
{
    for (auto i : sliderList)
        if (i->slider != nullptr){
            delete i->slider;
            delete i->label;
            delete i;
        }
    sliderList.clear();
    resizeEvent(new QResizeEvent(this->size(), this->size()));
}

void SM_SliderWidget::removeSliderByTitle(QString title)
{
    for (auto i : sliderList)
        if (i->title == title){
            delete i->slider;
            delete i->label;
            delete i;
            sliderList.removeOne(i);
            break;
        }
    resizeEvent(new QResizeEvent(this->size(), this->size()));
}

void SM_SliderWidget::removeSliderByKey(QString key)
{
    for (auto i : sliderList)
        if (i->key == key){
            delete i->label;
            delete i->slider;
            delete i;
            sliderList.removeOne(i);
            break;
        }
    resizeEvent(new QResizeEvent(this->size(), this->size()));
}

void SM_SliderWidget::removeSliderByPosition(int number)
{
    for (auto i : sliderList)
        if (i->position == number){
            delete i->label;
            delete i->slider;
            delete i;
            sliderList.removeOne(i);
            break;
        }
    resizeEvent(new QResizeEvent(this->size(), this->size()));
}

void SM_SliderWidget::setRelation(float value, int first, int second)
{
    relatio = value;
    firstRelatioSlider = first;
    secondRelatioSlider = second;
    setRelationStatus(true);
}

void SM_SliderWidget::setRelationStatus(bool isRelation)
{
    this->isRelation = isRelation;
    if (isRelation)
        if (isPositionPresent(secondRelatioSlider)){
            SliderList *s = getSliderItem(secondRelatioSlider);
            if (s == nullptr)
                return;
            s->slider->setValue(getSliderItem(firstRelatioSlider)->slider->value() * relatio);
        }
}

void SM_SliderWidget::setText(QString text)
{
    this->text = text;
    titleLabel.setText(this->text);
    resizeEvent(new QResizeEvent(this->size(), this->size()));
}

bool SM_SliderWidget::setAnchors(SM::Anchors anchors)
{
    if (! SM::isAnchorsOneWay(anchors))
        return false;

    this->anchors = anchors;
    return true;
}

bool SM_SliderWidget::isPositionPresent(int position)
{
    for (auto s : sliderList)
        if (s->position == position)
            return true;
    return false;
}

bool SM_SliderWidget::setValue(int value, int position)
{
    for (auto s : sliderList)
        if (s->position == position)
            if (value > s->slider->minimum())
                if (value < s->slider->maximum()){
                    s->slider->setValue(value);
                    return true;
                }
    return false;
}

bool SM_SliderWidget::setValue(int value, QString key)
{
    for (auto s : sliderList)
        if (s->key == key)
            if (value > s->slider->minimum())
                if (value < s->slider->maximum()){
                    s->slider->setValue(value);
                    return true;
                }
    return false;
}

int SM_SliderWidget::getFreePosition()
{
    int i(0);
    for (i = 0; i < INT_MAX; i++){
        if (! isPositionPresent(i))
            break;
    }
    return i;
}

int SM_SliderWidget::getValue(QString key)
{
    for (auto i : sliderList)
        if (i->key == key)
            return i->slider->value();
    return -1;
}

int SM_SliderWidget::getValue(int position)
{
    for (auto i : sliderList)
        if (i->position == position)
            return i->slider->value();
    return -1;
}

int SM_SliderWidget::sliderLenght()
{
    return sliderList.length();
}

SM::Anchors SM_SliderWidget::getAnshors()
{
    return anchors;
}

QString SM_SliderWidget::getText()
{
    return text;
}

SliderList* SM_SliderWidget::getSliderItem(QString key)
{
    for (auto s : sliderList)
        if (s->key == key)
            return s;
    return nullptr;
}

SliderList* SM_SliderWidget::getSliderItem(int position)
{
    for (auto s : sliderList)
        if (s->position == position)
            return s;
    return nullptr;
}
