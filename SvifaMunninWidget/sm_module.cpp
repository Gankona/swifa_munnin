#include "sm_module.h"

SM_Module::SM_Module(QString login)
    : login(login),
      status(SM::Module::moduleNotUse),
      mainWidget(nullptr)
{}

QIcon SM_Module::getIcon()
{
    return QIcon("://images/none_picture.png");
}

QString SM_Module::getLogin()
{
    return login;
}

QString SM_Module::getModuleName(QString language)
{
    return QString("NoName" + QString::number(rand()&1000) + language);
}

SM::Module SM_Module::getStatus()
{
    return status;
}

bool SM_Module::setWidget(QWidget *widget)
{
    try {
        mainWidget = widget;//->setWidget(widget);
    }
    catch (...) {
        emit setLog(SM::TypeMsg::warning,
                    "setWidget",
                    "in module " + getModuleName()
                    + " view not present, widget dosn`t set",
                    "get_status");
        return false;
    }
    return true;
}

void SM_Module::setStatus(SM::Module status)
{
    if (this->status == status)
        return;

    switch (status){
    case SM::Module::moduleBackgroundUse:
        if (this->status != SM::Module::moduleNotUse)
            start();
        try {
            delete mainWidget;
        } catch (...){}
        mainWidget = nullptr;
        break;

    case SM::Module::moduleSimplifiedUse:
    case SM::Module::moduleEntireUse:
        if (this->status == SM::Module::moduleNotUse)
            start();
        if (mainWidget == nullptr){
            mainWidget = new QWidget;//SM_BaseWidget(getModuleName());
            emit createWidget(mainWidget);
//            QObject::connect(mainWidget, &SM_BaseWidget::changeWidgetStatus,
//                             this, &SM_Module::setStatus);
            QObject::connect(mainWidget, SIGNAL(changeWidgetStatus(SM::Module)),
                             this, SLOT(setStatus(SM::Module)));
        }
        break;

    case SM::Module::moduleNotUse:
        stop();
        try {
            delete mainWidget;
        }
        catch (...){}
        break;
    default:;
    }

    this->status = status;
    emit changeStatus(this->status);
}
