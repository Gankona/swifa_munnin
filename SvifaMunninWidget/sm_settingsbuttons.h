#ifndef SM_SETTINGSBUTTONS_H
#define SM_SETTINGSBUTTONS_H

#include <QtGui/QResizeEvent>
#include <QtWidgets/QFrame>
#include <QtWidgets/QPushButton>

#include "sm_staticwidgetfunction.h"
#include "svifamunninwidget_global.h"

class SMWIDGETSHARED_EXPORT SM_SettingsButtons : public QFrame
{
    Q_OBJECT
private:
    QPushButton *saveButton;
    QPushButton *defaultButton;
    QPushButton *cancelButton;
    QPushButton *backButton;

protected:
    void resizeEvent(QResizeEvent *event);

public:
    SM_SettingsButtons(QWidget *parent = nullptr);

signals:
    void clickBack();
    void clickCancel();
    void clickDefault();
    void clickApply();
};

#endif // SM_SETTINGSBUTTONS_H
