#include "sm_panelbarwidget.h"

SM_PanelBarWidget::SM_PanelBarWidget(QWidget *parent,
                                     SM::Anchors anchors)
    : QFrame(parent),
      anchors(anchors)
{
    threePointButton = new QPushButton("...", this);
    addonsButton = new QPushButton("a", this);
    backButton = new QPushButton("b", this);
    prevButton = new QPushButton("p", this);
    nextButton = new QPushButton("n", this);
    clearModuleList();
    setButtonSize();

    QObject::connect(threePointButton, &QPushButton::clicked, this, [=](){
        currentTopMainButton = 0;
        currentTopMinorButton = -1;

        resize(this->size());
    });
    QObject::connect(addonsButton, &QPushButton::clicked, this, [=](){
        currentTopMinorButton = 0;
        currentTopMainButton = -1;

        resize(this->size());
    });
    QObject::connect(backButton, &QPushButton::clicked, this, [=](){
        currentTopMainButton  = -1;
        currentTopMinorButton = -1;
        resize(this->size());
    });
    QObject::connect(nextButton, &QPushButton::clicked, this, [=](){
        int bizySquard(2);
        if (addonsButton->isVisible())
            bizySquard++;
        if (threePointButton->isVisible())
            bizySquard++;

        if (currentTopMainButton != -1){
            currentTopMainButton += (maxContentButtons - bizySquard);
            if (currentTopMainButton >= listMainButton.keys().length())
                currentTopMainButton = listMainButton.keys().length() - 1;
        }
        if (currentTopMinorButton != -1){
            currentTopMinorButton += (maxContentButtons - bizySquard);
            if (currentTopMinorButton >= listMinorButton.keys().length())
                currentTopMinorButton = listMinorButton.keys().length() - 1;
        }

        resize(this->size());
    });
    QObject::connect(prevButton, &QPushButton::clicked, this, [=](){
        int bizySquard(1);
        if (nextButton->isVisible())
            bizySquard++;
        if (addonsButton->isVisible())
            bizySquard++;
        if (threePointButton->isVisible())
            bizySquard++;

        if (currentTopMainButton != -1){
            currentTopMainButton -= (maxContentButtons - bizySquard);
            if (currentTopMainButton < 0)
                currentTopMainButton = 0;
        }
        if (currentTopMinorButton != -1){
            currentTopMinorButton -= (maxContentButtons - bizySquard);
            if (currentTopMinorButton < 0)
                currentTopMinorButton = 0;
        }

        resize(this->size());
    });
}

void SM_PanelBarWidget::addToMainList(QString name,
                                      QPushButton *barButton,
                                      SM::Module status)
{
    int maxNumber(-1);
    for (auto a : listMainButton.keys())
        if (listMainButton.value(a)->position > maxNumber)
            maxNumber = listMainButton.value(a)->position;
    listMainButton.insert(name, new FloatedDual{barButton, ++maxNumber, status});
    listMainButton.value(name)->button->setFixedSize(sizeButton);
    listMainButton.value(name)->button->setParent(this);
    resize(this->size());
    QObject::connect(listMainButton.value(name)->button, &QPushButton::clicked, this, [=](){
        deleteAllGroupWidget();
        listMainButton.value(name)->isFloat = true;
        listMainButton.value(name)->groupWidget = new SM_GroupButtonWidget(static_cast<QWidget*>(this), sizeButton, 0.6, anchors);
        resize(this->size());
        QObject::connect(listMainButton.value(name)->groupWidget, &SM_GroupButtonWidget::changeWidgetStatus, this, [=]
                         (SM::Module status){
            listMainButton.value(name)->status = status;
            emit callChangeStatus(name, status);
        });
    });
}

void SM_PanelBarWidget::addToMinorList(QString name,
                                       QPushButton *barButton,
                                       SM::Module status)
{
    int maxNumber(-1);
    for (auto a : listMinorButton.keys())
        if (listMinorButton.value(a)->position > maxNumber)
            maxNumber = listMinorButton.value(a)->position;
    listMinorButton.insert(name, new FloatedDual{barButton, ++maxNumber, status});
    listMinorButton.value(name)->button->setFixedSize(sizeButton);
    listMinorButton.value(name)->button->setParent(this);
    resize(this->size());
    QObject::connect(listMinorButton.value(name)->button, &QPushButton::clicked, this, [=](){
        deleteAllGroupWidget();
        listMinorButton.value(name)->isFloat = true;
        listMinorButton.value(name)->groupWidget = new SM_GroupButtonWidget(static_cast<QWidget*>(this), sizeButton, 0.6, anchors);
        resize(this->size());
        QObject::connect(listMinorButton.value(name)->groupWidget, &SM_GroupButtonWidget::changeWidgetStatus, this, [=]
                         (SM::Module status){
            listMinorButton.value(name)->status = status;
            emit callChangeStatus(name, status);
        });
    });
}

bool SM_PanelBarWidget::addToExtraList(QString name,
                                       QPushButton *barButton,
                                       int position)
{
    if (position < 0 || position > 4)
        return false;
    for (auto a : listExtraButton)
        if (a->position == position)
            return false;
    listExtraButton.insert(name, new FloatedDual{barButton, position});
    listExtraButton.value(name)->button->setFixedSize(sizeButton);
    listExtraButton.value(name)->button->setParent(this);
    resize(this->size());
    QObject::connect(listExtraButton.value(name)->button, &QPushButton::clicked, this, [=](){
        emit clickToExtraButton(name);
    });
    return true;
}

void SM_PanelBarWidget::deleteAllGroupWidget()
{
    for (auto f : listMainButton){
        f->isFloat = false;
        try {
            delete f->groupWidget;
        } catch (...){}
    }
    for (auto f : listMinorButton){
        f->isFloat = false;
        try {
            delete f->groupWidget;
        } catch (...){}
    }
}

void SM_PanelBarWidget::clearModuleList()
{
    for (auto a : listMainButton){
        delete a->button;
        try {
            delete a->groupWidget;
        } catch(...){}
    }
    listMainButton.clear();
    for (auto a : listMinorButton){
        delete a->button;
        try {
            delete a->groupWidget;
        } catch(...){}
    }
    listMinorButton.clear();
    currentTopMainButton = -1;
    currentTopMinorButton = -1;
    resize(this->size());
}

void SM_PanelBarWidget::resize(QSize newSize)
{
    //find maximum include button in panel
    int maxButtonIncludeNumber(0);
    if (SM::Anchors::anchorsLeft == anchors
            || SM::Anchors::anchorsRight == anchors)
        maxButtonIncludeNumber = (newSize.height() - sizeButton.height()/2)/sizeButton.height();
    else
        maxButtonIncludeNumber = (newSize.width() - sizeButton.width()/2)/sizeButton.width();

    //put extra button
    for (auto a : listExtraButton){
        switch (anchors){
        case SM::Anchors::anchorsBottom:
            a->button->move(newSize.width()  - sizeButton.width() * (a->position + 1),
                            newSize.height() - sizeButton.height());
            break;
        case SM::Anchors::anchorsRight:
            a->button->move(newSize.width()  - sizeButton.width(),
                            newSize.height() - sizeButton.height() * (a->position + 1));
            break;
        case SM::Anchors::anchorsTop:
            a->button->move(newSize.width()  - sizeButton.width() * (a->position + 1), 0);
            break;
        case SM::Anchors::anchorsLeft:
        default:
            a->button->move(0, newSize.height() - sizeButton.height() * (a->position + 1));
        }
        maxButtonIncludeNumber--;
        maxContentButtons = maxButtonIncludeNumber;
    }

    //reset visible all second buttons
    threePointButton->setVisible(false);
    addonsButton->setVisible(false);
    backButton->setVisible(false);
    prevButton->setVisible(false);
    nextButton->setVisible(false);

    //test list to opening
    if (currentTopMainButton != -1){
        if (listMinorButton.keys().length() != 0){
            maxButtonIncludeNumber--;
            addonsButton->setVisible(true);
        }
        resizeScrollingList(listMainButton, currentTopMainButton, maxButtonIncludeNumber - 1, 0);
        resizeScrollingList(listMinorButton, -1, 0, 1);
        if (listMinorButton.keys().length() != 0){
            int mainCount(0);
            for (auto a : listMainButton)
                if (a->button->isVisible())
                    mainCount++;
            setOneButton(addonsButton, computeShift(), mainCount);
        }
    }
    else if (currentTopMinorButton != -1){
        if (listMainButton.keys().length() != 0){
            maxButtonIncludeNumber--;
            threePointButton->setVisible(true);
            setOneButton(threePointButton, 0/*computeShift(newSize)*/, 0);
        }
        resizeScrollingList(listMainButton, -1, 0, 0);
        resizeScrollingList(listMinorButton, currentTopMinorButton, maxButtonIncludeNumber - 1, 1);
    }
    else if (listMainButton.keys().length() + listMinorButton.keys().length() > maxButtonIncludeNumber){
        //it mean that buttons not include full
        if (listMinorButton.keys().length() == 0){
            //put from main what must be visible
            //other in [...]button
            resizeListContentAll(listMainButton, 0, maxButtonIncludeNumber - 1);
            threePointButton->setVisible(true);
            setOneButton(threePointButton, 0, maxButtonIncludeNumber - 1);
        }
        //we have one addons button, and for this we minus 1
        else if (listMainButton.keys().length() + 1 <= maxButtonIncludeNumber){
            //set all main buttons, we have a place for this,
            //addons hide in addonsButton
            resizeListContentAll(listMainButton, 0, maxButtonIncludeNumber - 1);
            resizeListContentAll(listMinorButton, 0, 0);
            addonsButton->setVisible(true);
            setOneButton(addonsButton, computeShift(), 0/*maxButtonIncludeNumber - 1*/);
        }
        else if (listMainButton.keys().length() + 1 > maxButtonIncludeNumber){
            resizeListContentAll(listMainButton, 0, maxButtonIncludeNumber - 2);
            resizeListContentAll(listMinorButton, 0, 0);
            //... and A buttons set
            threePointButton->setVisible(true);
            addonsButton->setVisible(true);
            setOneButton(threePointButton, 0, maxButtonIncludeNumber - 2);
            setOneButton(addonsButton, computeShift(), 1/*maxButtonIncludeNumber - 1*/);
        }
    }
    else {
        //we set all buttons on it
        resizeListContentAll(listMainButton, 0);

        if (listMinorButton.keys().length() != 0){
            //set minor button
            resizeListContentAll(listMinorButton, computeShift());
        }
    }
}

int SM_PanelBarWidget::computeShift()
{
    int shift;
    int mainCount(0);
    for (auto a : listMainButton)
        if (a->button->isVisible())
            mainCount++;
    if (threePointButton->isVisible())
        mainCount++;
    if (SM::Anchors::anchorsBottom == anchors
            || SM::Anchors::anchorsTop == anchors){
        shift = size().width()
                - listExtraButton.keys().length() * sizeButton.width()
                - mainCount * sizeButton.width()
                - sizeButton.width() / 2;
        if (shift > sizeButton.width())
            shift = sizeButton.width() / 4;
        shift += mainCount * sizeButton.width();
    }
    else {
        shift = size().height()
                - listExtraButton.keys().length() * sizeButton.height()
                - mainCount * sizeButton.height()
                - sizeButton.height() / 2;
        if (shift > sizeButton.height())
            shift = sizeButton.height() / 4;
        shift += mainCount * sizeButton.height();
    }
    return shift;
}

void SM_PanelBarWidget::resizeScrollingList(QMap<QString, FloatedDual *> list, int currentPosition,
                                            int maxContent, int listStart)
{
    int shift(0);
    if (listStart == 1 && listMainButton.keys().length() != 0)
        shift = computeShift();
    if (currentPosition == 0){
        backButton->setVisible(true);
        setOneButton(backButton, shift, 0);
    }
    else if (currentPosition != -1){
        prevButton->setVisible(true);
        setOneButton(prevButton, shift, 0);
    }
    //compare count possible buttons
    int lSide(currentPosition);
    int rSide(-1);
    if (list.keys().length() <= maxContent - 1 + currentPosition)
        rSide = list.keys().length() - 1;
    else if (currentPosition != -1){
        rSide = maxContent - 2 + currentPosition;
        nextButton->setVisible(true);
        setOneButton(nextButton, shift, maxContent/*list.keys().length() - currentPosition*/);
    }

    //set ALL LIST WHIS FILTER
    for (auto f : list){
        if (f->position >= lSide
                && f->position <= rSide){
            f->button->setVisible(true);
            setOneButton(f->button, shift, 1 + f->position - lSide);
            if (f->isFloat){
                try {
                    int position = 1 + f->position - lSide;
                    switch (anchors) {
                    case SM::Anchors::anchorsBottom:
                        f->groupWidget->setFixedSize(sizeButton.width(),
                                                     size().height() - sizeButton.height());
                        f->groupWidget->move(static_cast<QWidget*>(this)->pos().x() + shift + sizeButton.width() * position,
                                             static_cast<QWidget*>(this)->pos().y());

                        break;
                    case SM::Anchors::anchorsRight:
                        f->groupWidget->setFixedSize(size().width() - sizeButton.width(),
                                                     sizeButton.height());
                        f->groupWidget->move(static_cast<QWidget*>(this)->pos().x(),
                                             static_cast<QWidget*>(this)->pos().y() + shift + sizeButton.height() * position);

                        break;
                    case SM::Anchors::anchorsTop:
                        f->groupWidget->setFixedSize(sizeButton.width(),
                                                     size().height() - sizeButton.height());
                        f->groupWidget->move(static_cast<QWidget*>(this)->pos().x() + shift + sizeButton.width() * position,
                                             static_cast<QWidget*>(this)->pos().y() + sizeButton.height());

                        break;
                    case SM::Anchors::anchorsLeft:
                    default:
                        f->groupWidget->setFixedSize(size().width() - sizeButton.width(),
                                                     sizeButton.height());
                        f->groupWidget->move(static_cast<QWidget*>(this)->pos().x() + sizeButton.width(),
                                             static_cast<QWidget*>(this)->pos().y() + shift + sizeButton.height() * position);
                        ;
                    }
                } catch (...){}
            }
        }
        else {
            f->button->setVisible(false);
            if (f->isFloat){
                try {
                    delete f->groupWidget;
                    f->isFloat = false;
                } catch (...){}
            }
        }
    }
}

void SM_PanelBarWidget::resizeListContentAll(QMap<QString, FloatedDual *> list, int shift,
                                             int maxPosition){
    for (auto f : list){
        f->button->setVisible(f->position < maxPosition
                              || maxPosition == -1);
        if (f->position < maxPosition
                || maxPosition == -1){
            switch (anchors){
            case SM::Anchors::anchorsBottom:
                f->button->move(shift + sizeButton.width() * f->position,
                                size().height() - sizeButton.height());
                if (f->isFloat){
                    try {
                        f->groupWidget->setFixedSize(sizeButton.width(),
                                                     size().height() - sizeButton.height());
                        f->groupWidget->move(static_cast<QWidget*>(this)->pos().x() + shift + sizeButton.width() * f->position,
                                             static_cast<QWidget*>(this)->pos().y());
                    } catch (...){}
                }
                break;
            case SM::Anchors::anchorsRight:
                f->button->move(size().width() - sizeButton.width(),
                                shift + sizeButton.height() * f->position);
                if (f->isFloat){
                    try {
                        f->groupWidget->setFixedSize(size().width() -  sizeButton.width(),
                                                     sizeButton.height());
                        f->groupWidget->move(static_cast<QWidget*>(this)->pos().x(),
                                             static_cast<QWidget*>(this)->pos().y() + shift + sizeButton.height() * f->position);
                    } catch (...){}
                }
                break;
            case SM::Anchors::anchorsTop:
                f->button->move(shift + sizeButton.width() * f->position, 0);
                if (f->isFloat){
                    try {
                        f->groupWidget->setFixedSize(sizeButton.width(),
                                                     size().height() - sizeButton.height());
                        f->groupWidget->move(static_cast<QWidget*>(this)->pos().x() + shift + sizeButton.width() * f->position,
                                             static_cast<QWidget*>(this)->pos().y() + sizeButton.height());
                    } catch (...){}
                }
                break;
            case SM::Anchors::anchorsLeft:
            default:
                f->button->move(0, shift + sizeButton.height() * f->position);
                if (f->isFloat){
                    try {
                        f->groupWidget->setFixedSize(size().width() -  sizeButton.width(),
                                                     sizeButton.height());
                        f->groupWidget->move(static_cast<QWidget*>(this)->pos().x() + sizeButton.width(),
                                             static_cast<QWidget*>(this)->pos().y() + shift + sizeButton.height() * f->position);
                    } catch (...){}
                }
            }
        }
        if (f->isFloat){
            try {
                f->groupWidget->setVisible(false);
            } catch (...){}
        }
    }
}

void SM_PanelBarWidget::setOneButton(QPushButton *p,
                                     int shift, int buttonCount)
{
    switch (anchors){
    case SM::Anchors::anchorsBottom:
        p->move(shift + sizeButton.width() * buttonCount,
                size().height() - sizeButton.height());
        break;
    case SM::Anchors::anchorsRight:
        p->move(size().width() - sizeButton.width(),
                shift + sizeButton.height() * buttonCount);
        break;
    case SM::Anchors::anchorsTop:
        p->move(shift + sizeButton.width() * buttonCount, 0);
        break;
    case SM::Anchors::anchorsLeft:
    default:
        p->move(0, shift + sizeButton.height() * buttonCount);
    }
}

void SM_PanelBarWidget::setChangedStatus(QString moduleName,
                                         SM::Module status)
{
    if (listMainButton.contains(moduleName)){
        listMainButton.value(moduleName)->status = status;
        if (listMainButton.value(moduleName)->isFloat){
            try {
                listMainButton.value(moduleName)->groupWidget->setStatus(status);
            } catch (...){}
        }
    }
    else if (listMinorButton.contains(moduleName)){
        listMinorButton.value(moduleName)->status = status;
        if (listMinorButton.value(moduleName)->isFloat){
            try {
                listMinorButton.value(moduleName)->groupWidget->setStatus(status);
            } catch (...){}
        }
    }
}

void SM_PanelBarWidget::setButtonSize()
{
    QSize nsizeButton;
    if (anchors == SM::Anchors::anchorsBottom
            || anchors == SM::Anchors::anchorsTop)
        nsizeButton = QSize(this->height(), this->height());
    else
        nsizeButton = QSize(this->width(), this->width());

    if (nsizeButton == sizeButton)
        return;
    else
        sizeButton = nsizeButton;

    threePointButton->setFixedSize(sizeButton);
    addonsButton->setFixedSize(sizeButton);
    backButton->setFixedSize(sizeButton);
    prevButton->setFixedSize(sizeButton);
    nextButton->setFixedSize(sizeButton);
    for (auto f : listExtraButton)
        f->button->setFixedSize(sizeButton);
    for (auto f : listMainButton)
        f->button->setFixedSize(sizeButton);
    for (auto f : listMinorButton)
        f->button->setFixedSize(sizeButton);
    resize(this->size());
}

void SM_PanelBarWidget::setAnchors(SM::Anchors anchors)
{
    this->anchors = anchors;
    resize(size());
}

void SM_PanelBarWidget::resizeEvent(QResizeEvent *event)
{
    setButtonSize();
    resize(event->size());
}

SM::Anchors SM_PanelBarWidget::getAnchors()
{
    return anchors;
}
