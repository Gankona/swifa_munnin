#ifndef SM_GROUPBUTTONWIDGET_H
#define SM_GROUPBUTTONWIDGET_H

#include <QStyle>
#include <QtWidgets/QFrame>
#include <QPushButton>
#include <QHBoxLayout>
#include "sm_namespace.h"
#include <QGraphicsOpacityEffect>
#include <QResizeEvent>
#include "sm_staticwidgetfunction.h"
#include "sm_opacitywidget.h"
#include "svifamunninwidget_global.h"

class SMWIDGETSHARED_EXPORT SM_GroupButtonWidget : public SM_OpacityWidget
{
    Q_OBJECT
private:
    QPushButton *rollUpButton;
    QPushButton *minimizeButton;
    QPushButton *closeButton;
    QPushButton *fullViewButton;
    SM::Anchors anchors;
    SM::Module status;

    void setChecked();
    void resizeContent(QSize size);

protected:
    void resizeEvent(QResizeEvent *event);

public:
    SM_GroupButtonWidget(QWidget *parent,
                         QSize buttonSize = QSize(20, 20),
                         qreal opacityEffect = 1,
                         SM::Anchors anchors = SM::Anchors::anchorsTop);

    SM_GroupButtonWidget(QSize buttonSize,
                         QWidget *parent = nullptr,
                         qreal opacityEffect = 1,
                         SM::Anchors anchors = SM::Anchors::anchorsTop);

    void setAnchors(SM::Anchors anchors);
    SM::Anchors getAnchors();
    void setStatus(SM::Module status);
    SM::Module getStatus();

signals:
    void changeWidgetStatus(SM::Module newStatus);
};

#endif // SM_GROUPBUTTONWIDGET_H
