#include "sm_labeledit.h"

SM_LabelEdit::SM_LabelEdit(QWidget *parent) :
    SM_LabelEdit("", "", parent, false)
{}

SM_LabelEdit::SM_LabelEdit(QString text, QWidget *parent)
    : SM_LabelEdit("", text, parent, false)
{}

SM_LabelEdit::SM_LabelEdit(QString title,
                           QString text,
                           QWidget *parent,
                           bool isTitlePresent)
    : QWidget(parent),
      editMode(false),
      titleMode(isTitlePresent)
{
    this->text = text;
    this->title = new QLabel(title, this);
    label = new QLabel(this->text, this);
    edit = new QLineEdit(this);
    edit->setVisible(false);
    button = new QPushButton(this);

    setTitleMode(titleMode);

    QObject::connect(button, &QPushButton::clicked, this, [=](){
        if (editMode)
            closeEdit();
        else
            openEdit();
    });
    QObject::connect(edit, &QLineEdit::returnPressed, this, &SM_LabelEdit::closeEdit);
}

void SM_LabelEdit::resizeContent(QSize size)
{
    if (size.width() + size.height() < 120)
        return;
    button->setFixedSize(size.height(), size.height());
    button->move(size.width() - size.height(), 0);
    if (titleMode){
        int shift(120);
        if (size.width() > 350)
            shift = size.width()/3;
        title->setFixedSize(size.width() - shift - size.height(), size.height());
        label->setFixedSize(shift, size.height());
        edit->setFixedSize(shift, size.height());
        title->move(0, 0);
        label->move(title->width(), 0);
        edit->move(title->width(), 0);
    }
    else {
        label->setFixedSize(size.width() - size.height(), size.height());
        edit->setFixedSize(size.width() - size.height(), size.height());
    }
}

void SM_LabelEdit::resizeEvent(QResizeEvent *event)
{
    resizeContent(event->size());
}

void SM_LabelEdit::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape){
        returnOldText();
        closeEdit();
    }
}

void SM_LabelEdit::openEdit()
{
    editMode = true;
    buffer = text;
    edit->setText(text);
    edit->setVisible(true);
    label->setVisible(false);
    button->setToolTip("apply");
    button->setIcon(QIcon(":/sm_w/images/accept.png"));
}

void SM_LabelEdit::closeEdit()
{
    editMode = false;
    text = edit->text();
    if (text != buffer)
        oldText = text;
    label->setText(text);
    label->setVisible(true);
    edit->setVisible(false);
    button->setToolTip("edit");
    button->setIcon(QIcon(":/sm_w/images/edit.png"));
}

void SM_LabelEdit::returnOldText()
{
    text = oldText;
    label->setText(text);
}

QString SM_LabelEdit::getOldText()
{
    return oldText;
}

QString SM_LabelEdit::getCurrentText()
{
    return text;
}

QString SM_LabelEdit::getTitle()
{
    return title->text();
}

void SM_LabelEdit::setTitle(QString title)
{
    this->title->setText(title);
    setTitleMode(true);
}

void SM_LabelEdit::setTitleMode(bool isTitlePresent)
{
    titleMode = isTitlePresent;
    title->setVisible(titleMode);
    resizeContent(this->size());
    if (titleMode){
        label->setAlignment(Qt::AlignHCenter | Qt::AlignRight);
        label->setText(text + " ");
    }
    else {
        label->setAlignment(Qt::AlignHCenter | Qt::AlignLeft);
        label->setText(text);
    }
}

bool SM_LabelEdit::isTitleMode()
{
    return titleMode;
}
