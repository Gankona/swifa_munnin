#ifndef SM_SLASHBUTTON_H
#define SM_SLASHBUTTON_H

#include <QtGui/QBitmap>
#include <QtGui/QFontMetrics>
#include <QtGui/QPainter>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

#include <cmath>
#include <QDebug>

#include "sm_namespace.h"
#include "svifamunninwidget_global.h"

class SMWIDGETSHARED_EXPORT SM_SlashButton : public QPushButton
{
    Q_OBJECT
private:
    SM::Anchors anchors;
    QPixmap *m_Cache;
    QRect textRect;
    QRect squareRect;

    const double Pi = 3.14159265358979323846;
    bool isRectUpdate;
    int bigShift;
    int littleShift;
    int margin;
    int slashAngel;

    SM::Anchors firstSide;
    SM::Anchors secondSide;

    void reCalculateShifts();

protected:
    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *);

public:
    SM_SlashButton(QWidget *parent = nullptr);

    SM_SlashButton(SM::Anchors anchors,
                  QWidget *parent = nullptr);

    SM_SlashButton(QString text,
                   QWidget *parent = nullptr,
                   SM::Anchors anchors = SM::Anchors::anchorsLeftRight,
                   SM::Anchors first = SM::Anchors::anchorsRight,
                   SM::Anchors second = SM::Anchors::anchorsRight);

    int shift();

    int calculateTextPixelLenght(const QFont &font);

    bool setFirstSlashType(SM::Anchors type);
    bool setSecondSlashType(SM::Anchors type);

    void setTextRectangle(QRect textRectangle);
    void setSquareRectanle(QSize size);
    void setSquareRectanle(int w, int h);

    bool setMargin(int m);
    int getMargin();

    SM::Anchors getFirstSlashType();
    SM::Anchors getSecondSlashType();

    int getLittleShift();
    int getBigShift();

    bool setSlashAnchors(const SM::Anchors &anchors);
    SM::Anchors getSlashAnchors();
    bool setSlashAngel(int angel);
    int getSlashAngel();
};

#endif // SM_SLASHBUTTON_H
