#ifndef SM_MARGINWIDGET_H
#define SM_MARGINWIDGET_H

#include "sm_namespace.h"
#include "svifamunninwidget_global.h"
#include <QtGui/QResizeEvent>
#include <QtWidgets/QFrame>
#include <QtWidgets/QWidget>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>

class SMWIDGETSHARED_EXPORT SM_MarginWidget : public QWidget
{
private:
    float scalablyValue;
    SM::Anchors anchors;

    QWidget *frame;
    void updateContent(QSize size);

protected:
    void resizeEvent(QResizeEvent *event);

public:
    SM_MarginWidget(QWidget *parent = nullptr,
                    float scalablyValue = 0.7,
                    SM::Anchors anchors = SM::Anchors::anchorsLeftRight);

    void setWidget(QWidget* frame);
    void setMarginAnchors(SM::Anchors anchors);
    SM::Anchors getMarginAnchors();

    void setScalablyValue(float value);
    float getScalablyValue();
};

#endif // SM_MARGINWIDGET_H
