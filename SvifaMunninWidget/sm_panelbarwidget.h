#ifndef SM_PANELBARWIDGET_H
#define SM_PANELBARWIDGET_H

#include <QtCore/QObject>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QFrame>

#include "sm_groupbuttonwidget.h"
#include "sm_namespace.h"
#include "sm_staticwidgetfunction.h"
#include "svifamunninwidget_global.h"

class SMWIDGETSHARED_EXPORT SM_PanelBarWidget : public QFrame
{
    Q_OBJECT
private:
    SM::Anchors anchors;
    QSize sizeButton;
    int currentTopMainButton;
    int currentTopMinorButton;
    int maxContentButtons;

    QPushButton *threePointButton;
    QPushButton *addonsButton;
    QPushButton *backButton;
    QPushButton *prevButton;
    QPushButton *nextButton;

    //some struct
    struct FloatedDual {
        QPushButton *button;
        int position;
        SM::Module status = SM::Module::moduleNotLoad;
        SM_GroupButtonWidget *groupWidget = nullptr;
        bool isFloat = false;
    };

    //list contentButton
    QMap <QString, FloatedDual*> listMainButton;
    QMap <QString, FloatedDual*> listMinorButton;
    QMap <QString, FloatedDual*> listExtraButton;

    inline void resizeListContentAll(QMap <QString, FloatedDual*> list,
                                     int shift, int maxPosition = -1);
    inline void setOneButton(QPushButton *p, int shift, int buttonCount);
    inline void resizeScrollingList(QMap <QString, FloatedDual*> list,
                                    int currentPosition,
                                    int maxContent, int listStart);
    int computeShift();
    void deleteAllGroupWidget();
    void resizeEvent(QResizeEvent *event);
    void resize(QSize newSize);
    void setButtonSize();

public:
    SM_PanelBarWidget(QWidget *parent = nullptr,
                      SM::Anchors anchors = SM::Anchors::anchorsLeft);

    void addToMainList(QString name,
                       QPushButton *barButton,
                       SM::Module status);

    void addToMinorList(QString name,
                        QPushButton *barButton,
                        SM::Module status);

    bool addToExtraList(QString name,
                        QPushButton *barButton,
                        int position);

    void setChangedStatus(QString moduleName, SM::Module status);
    void setAnchors(SM::Anchors anchors);

    void clearModuleList();
    SM::Anchors getAnchors();

signals:
    void clickToExtraButton(QString nameButton);
    void callChangeStatus(QString moduleName, SM::Module status);
};

#endif // SM_PANELBARWIDGET_H
