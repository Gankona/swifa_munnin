#ifndef SM_ITEMPALETTE_H
#define SM_ITEMPALETTE_H

#include <QtCore/QDataStream>
#include <QtCore/QObject>
#include <QtGui/QColor>

class SM_ItemPalette : public QObject
{
    Q_OBJECT
private:
    QColor buttonColor;
    QColor textColor;
    QColor backgroundColor;

public:
    explicit SM_ItemPalette(QObject *parent = 0);

    virtual QColor background() final;
    virtual QColor button() final;
    virtual QColor text() final;

    virtual void setBackgroundColor(QColor color) final;
    virtual void setBackgroundColor(int red, int green, int blue, int opacity = 255) final;

    virtual void setButtonColor(QColor color) final;
    virtual void setButtonColor(int red, int green, int blue, int opacity = 255) final;

    virtual void setTextColor(QColor color) final;
    virtual void setTextColor(int red, int green, int blue, int opacity = 255) final;

    friend QDataStream& operator << (QDataStream &d, SM_ItemPalette &p);
    friend QDataStream& operator >> (QDataStream &d, SM_ItemPalette &p);

signals:
    void colorChange();
};

#endif // SM_ITEMPALETTE_H
