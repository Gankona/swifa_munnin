#include "sm_switcherbutton.h"

SM_SwitcherButton::SM_SwitcherButton(QString text, QWidget *parent, bool value)
    : QFrame(parent),
      value(value),
      visibleSwitcher(true)
{
    noButton = SM_WF::button("no", this, false, true, false);
    yesButton = SM_WF::button("yes", this, false, true, false);
    titleLabel = new QLabel(text, this);
    setValue(value);
    this->setFrameShape(QFrame::StyledPanel);

    QObject::connect(noButton,  &QPushButton::clicked, this, [=](){ setValue(false);});
    QObject::connect(yesButton, &QPushButton::clicked, this, [=](){ setValue(true); });
}

SM_SwitcherButton::SM_SwitcherButton(QWidget *parent, bool value)
    : SM_SwitcherButton("", parent, value)
{}

void SM_SwitcherButton::resizeEvent(QResizeEvent *event)
{
    QSize buttonSize = QSize(event->size().height()*1.5, event->size().height());
    yesButton->setFixedSize(buttonSize);
    noButton->setFixedSize(buttonSize);
    if (visibleSwitcher)
        titleLabel->setFixedSize(event->size().width() - 2 * buttonSize.width() - 5, buttonSize.height());
    else
        titleLabel->setFixedSize(event->size().width() - 5, event->size().height());
    titleLabel->move(5, 0);
    yesButton->move(event->size().width() - buttonSize.width(), 0);
    noButton->move(event->size().width() - 2*buttonSize.width(), 0);
    yesButton->setVisible(visibleSwitcher);
    noButton->setVisible(visibleSwitcher);
}

bool SM_SwitcherButton::getValue()
{
    return value;
}

void SM_SwitcherButton::setValue(bool value)
{
    this->value = value;
    noButton->setChecked(!value);
    yesButton->setChecked(value);
    emit valueChanged(value);
}

void SM_SwitcherButton::setText(QString text)
{
    titleLabel->setText(text);
}

QString SM_SwitcherButton::getText()
{
    return titleLabel->text();
}

bool SM_SwitcherButton::getVisibleSwitcher()
{
    return visibleSwitcher;
}

void SM_SwitcherButton::setVisibleSwitcher(bool visible)
{
    visibleSwitcher = visible;
    resizeEvent(new QResizeEvent(this->size(), this->size()));
}
