#include "sm_horizontallistwidget.h"

SM_HorizontalListWidget::SM_HorizontalListWidget(QWidget *parent, bool isCircle)
    : QWidget(parent),
      isCircle(isCircle)
{
    leftButton = SM_WF::iconButton(style()->standardIcon(QStyle::SP_ArrowLeft), "left", this);
    rightButton = SM_WF::iconButton(style()->standardIcon(QStyle::SP_ArrowRight), "left", this);
    valueLabel = SM_WF::label(this, Qt::AlignCenter);
    list.clear();
    current = "";

    QObject::connect(leftButton, &QPushButton::clicked, this, [=](){
        if (current == list.first())
            current = list.last();
        else {
            QString prev("");
            for (auto s : list){
                if (s == current)
                    break;
                else
                    prev = s;
            }
            current = prev;
        }
        refreshItems();
    });
    QObject::connect(rightButton, &QPushButton::clicked, this, [=](){
        for (auto i = list.begin(); i != list.end(); i++)
            if (*i == current){
                if (++i != list.end())
                    current = *(++i);
                else
                    current = list.first();
                break;
            }
        refreshItems();
    });
}

void SM_HorizontalListWidget::refreshItems()
{
    if (list.length() != 0){
        if (current == "")
            current = list.first();
        if (isCircle){
            rightButton->setVisible(list.length() > 1);
            leftButton->setVisible(list.length() > 1);
        }
        else {
            leftButton->setVisible(list.first() != current);
            rightButton->setVisible(list.last() != current);
        }
    }
    else {
        current = "";
        rightButton->setVisible(false);
        leftButton->setVisible(false);
    }
    valueLabel->setText(current);
}

void SM_HorizontalListWidget::resizeEvent(QResizeEvent *event)
{
    QSize sizeButton = QSize(event->size().height(), event->size().height());
    leftButton->setFixedSize(sizeButton);
    rightButton->setFixedSize(sizeButton);
    valueLabel->setFixedSize(event->size().width() - 2*sizeButton.width(), sizeButton.height());
    leftButton->move(0, 0);
    valueLabel->move(0, sizeButton.width());
    rightButton->move(0, event->size().width() - sizeButton.width());
}

void SM_HorizontalListWidget::addItem(QString item)
{
    list.push_back(item);
    refreshItems();
}

void SM_HorizontalListWidget::addItems(std::initializer_list <QString> items)
{
    for (auto i = items.begin(); i != items.end(); i++)
        list.push_back(*i);
    refreshItems();
}

void SM_HorizontalListWidget::addItems(QStringList list)
{
    this->list += list;
    refreshItems();
}

void SM_HorizontalListWidget::removeItem(QString item)
{
    list.removeOne(item);
    refreshItems();
}

void SM_HorizontalListWidget::removeItems(std::initializer_list <QString> items)
{
    for (auto i = items.begin(); i != items.end(); i++)
        list.removeOne(*i);
    refreshItems();
}

void SM_HorizontalListWidget::removeItems(QStringList list)
{
    for (auto s : list)
        if (list.contains(s))
            this->list.removeOne(s);
    refreshItems();
}

void SM_HorizontalListWidget::setCircleStatus(bool isCircle)
{
    this->isCircle = isCircle;
    refreshItems();
}

QString SM_HorizontalListWidget::currentItem()
{
    return current;
}
