#ifndef SM_APPPALETTE_H
#define SM_APPPALETTE_H

#include <QDebug>
#include <QtCore/QDataStream>
#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtGui/QColor>

#include "sm_itempalette.h"
#include "svifamunninwidget_global.h"

class SMWIDGETSHARED_EXPORT SM_AppPalette : public QObject
{
    Q_OBJECT
private:
    QTimer *timer;

    SM_ItemPalette *mainPanel;
    SM_ItemPalette *otherPanel;
    SM_ItemPalette *mainPalette;
    SM_ItemPalette *secondPalette;

    QColor lineColor;

    int firstLine;
    int secondLine;
    int thirdLine;

    void tryUpdate();

public:
    SM_AppPalette(QObject *parent = nullptr);
    SM_AppPalette(int msIntervalTimer, QObject *parent = nullptr);

    virtual SM_ItemPalette* getMainPanel() final;
    virtual SM_ItemPalette* getOtherPanel() final;
    virtual SM_ItemPalette* getMainPalette() final;
    virtual SM_ItemPalette* getSecondPalette() final;

    virtual QColor getLineColor() final;
    virtual void setLineColor(QColor color) final;
    virtual void setLineColor(int red, int green, int blue, int opacity = 255) final;

    virtual int getFirstLine() final;
    virtual int getSecondLine() final;
    virtual int getThirdLine() final;

    virtual void setFirstLine(int v) final;
    virtual void setSecondLine(int v) final;
    virtual void setThirdLine(int v) final;

    friend QDataStream& operator << (QDataStream &s, SM_AppPalette &p);
    friend QDataStream& operator >> (QDataStream &s, SM_AppPalette &p);

signals:
    void updatePalette();
};

#endif // SM_APPPALETTE_H
