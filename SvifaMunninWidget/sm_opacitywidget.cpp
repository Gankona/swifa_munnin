#include "sm_opacitywidget.h"

SM_OpacityWidget::SM_OpacityWidget(qreal opacityEffect,
                                   QWidget *parent)
    : QFrame(parent)
{
    opacity = new QGraphicsOpacityEffect(this);
    opacity->setOpacity(opacityEffect);
    this->setGraphicsEffect(opacity);
}

void SM_OpacityWidget::setOpacity(qreal opacity)
{
    this->opacity->setOpacity(opacity);
    repaint();
}

qreal SM_OpacityWidget::getOpacityEffect()
{
    return opacity->opacity();
}
