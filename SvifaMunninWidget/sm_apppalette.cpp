#include "sm_apppalette.h"

SM_AppPalette::SM_AppPalette(QObject *parent)
    : SM_AppPalette(400, parent)
{}

SM_AppPalette::SM_AppPalette(int msIntervalTimer, QObject *parent)
    : QObject(parent)
{
    timer = new QTimer(this);
    timer->setInterval(msIntervalTimer);

    setLineColor(0xa0, 0xa0, 0xa0);
    setFirstLine(2);
    setSecondLine(1);
    setThirdLine(1);

    mainPanel = new SM_ItemPalette(this);
    otherPanel = new SM_ItemPalette(this);
    mainPalette = new SM_ItemPalette(this);
    secondPalette = new SM_ItemPalette(this);

    QObject::connect(timer, &QTimer::timeout, this, [=](){
        emit updatePalette();
        timer->stop();
    });

    QObject::connect(mainPanel,     &SM_ItemPalette::colorChange, this, &SM_AppPalette::tryUpdate);
    QObject::connect(otherPanel,    &SM_ItemPalette::colorChange, this, &SM_AppPalette::tryUpdate);
    QObject::connect(mainPalette,   &SM_ItemPalette::colorChange, this, &SM_AppPalette::tryUpdate);
    QObject::connect(secondPalette, &SM_ItemPalette::colorChange, this, &SM_AppPalette::tryUpdate);
}

void SM_AppPalette::tryUpdate()
{
    if (! timer->isActive())
        timer->start();
}

QColor SM_AppPalette::getLineColor()
{
    return lineColor;
}

void SM_AppPalette::setLineColor(QColor color)
{
    lineColor = color;
    tryUpdate();
}

void SM_AppPalette::setLineColor(int red, int green, int blue, int opacity)
{
    setLineColor(QColor(red, green, blue, opacity));
}

int SM_AppPalette::getFirstLine()
{
    return firstLine;
}

int SM_AppPalette::getSecondLine()
{
    return secondLine;
}

int SM_AppPalette::getThirdLine()
{
    return thirdLine;
}

void SM_AppPalette::setFirstLine(int v)
{
    firstLine = v;
    tryUpdate();
}

void SM_AppPalette::setSecondLine(int v)
{
    secondLine = v;
    tryUpdate();
}

void SM_AppPalette::setThirdLine(int v)
{
    thirdLine = v;
    tryUpdate();
}

SM_ItemPalette* SM_AppPalette::getMainPanel()
{
    return mainPanel;
}

SM_ItemPalette* SM_AppPalette::getOtherPanel()
{
    return otherPanel;
}

SM_ItemPalette* SM_AppPalette::getMainPalette()
{
    return mainPalette;
}

SM_ItemPalette* SM_AppPalette::getSecondPalette()
{
    return secondPalette;
}

QDataStream& operator << (QDataStream &s, SM_AppPalette &p)
{
    return s << p.firstLine << p.secondLine << p.thirdLine
             << *p.mainPanel << *p.otherPanel
             << *p.mainPalette << *p.secondPalette;
}

QDataStream& operator >> (QDataStream &s, SM_AppPalette &p)
{
    return s >> p.firstLine >> p.secondLine >> p.thirdLine
             >> *p.mainPanel >> *p.otherPanel
             >> *p.mainPalette >> *p.secondPalette;
}
