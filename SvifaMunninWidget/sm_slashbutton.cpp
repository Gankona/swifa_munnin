#include "sm_slashbutton.h"

SM_SlashButton::SM_SlashButton(QWidget *parent)
    : SM_SlashButton("", parent){}

SM_SlashButton::SM_SlashButton(SM::Anchors anchors, QWidget *parent)
    : SM_SlashButton("", parent, anchors){}

SM_SlashButton::SM_SlashButton(QString text,
                               QWidget *parent,
                               SM::Anchors anchors,
                               SM::Anchors first,
                               SM::Anchors second)
    : QPushButton(text, parent),
      m_Cache(nullptr),
      isRectUpdate(false),
      bigShift(0),
      littleShift(0),
      margin(3),
      slashAngel(25)
{
    if (! setSlashAnchors(anchors))
        setSlashAnchors(SM::Anchors::anchorsLeftRight);
    if (! setFirstSlashType(first))
        setFirstSlashType(SM::Anchors::anchorsNull);
    if (! setSecondSlashType(second))
        setSecondSlashType(SM::Anchors::anchorsNull);
}

void SM_SlashButton::paintEvent(QPaintEvent*)
{
    if(m_Cache != nullptr){
      QPainter painter(this);
      painter.drawPixmap(0, 0, *m_Cache);
    }
}

void SM_SlashButton::resizeEvent(QResizeEvent *event)
{
    if (! isRectUpdate)
        anchors == SM::Anchors::anchorsLeftRight
                ? setSquareRectanle(event->size() - QSize(bigShift, 0))
                : setSquareRectanle(event->size() - QSize(0, bigShift));
}

void SM_SlashButton::reCalculateShifts()
{
    QSize size = this->size();
    if (anchors == SM::Anchors::anchorsLeftRight){
        if (firstSide != SM::Anchors::anchorsNull)
            size.setWidth(size.width() - shift());
        if (secondSide != SM::Anchors::anchorsNull)
            size.setWidth(size.width() - shift());
        size.setWidth(size.width() - 2*margin);
    }
    else {
        if (firstSide != SM::Anchors::anchorsNull)
            size.setHeight(size.height() - shift());
        if (secondSide != SM::Anchors::anchorsNull)
            size.setHeight(size.height() - shift());
        size.setHeight(size.height() - 2*margin);
    }
    if (squareRect.size() != size)
        setSquareRectanle(size);
}

void SM_SlashButton::setTextRectangle(QRect textRectangle)
{
    if (anchors == SM::Anchors::anchorsLeftRight)
        setSquareRectanle(textRectangle.width() + margin*2, this->height());
    else
        setSquareRectanle(this->width(), textRectangle.height() + margin*2);
}

void SM_SlashButton::setSquareRectanle(QSize size)
{
    squareRect = QRect(QPoint(0, 0), size);
    isRectUpdate = true;

    // resized widget, add slash place -------------------------
    littleShift = 0;
    if (firstSide != SM::Anchors::anchorsNull){
        anchors == SM::Anchors::anchorsLeftRight
                ? size.setWidth(size.width() + shift())
                : size.setHeight(size.height() + shift());
        littleShift = shift();
    }
    bigShift = littleShift;
    if (secondSide != SM::Anchors::anchorsNull){
        anchors == SM::Anchors::anchorsLeftRight
                ? size.setWidth(size.width() + shift())
                : size.setHeight(size.height() + shift());
        bigShift += shift();
    }
    anchors == SM::Anchors::anchorsLeftRight
            ? squareRect = QRect(QPoint(littleShift, 0), squareRect.size())
            : squareRect = QRect(QPoint(0, littleShift), squareRect.size());
    this->setFixedSize(size);

    // set text rect -------------------------------------------
    if (anchors == SM::Anchors::anchorsLeftRight)
        textRect = QRect(QPoint(squareRect.topLeft().x() + margin,     squareRect.size().height()*0.25),
                         QPoint(squareRect.bottomRight().x() - margin, squareRect.size().height()*0.75));
    else
        textRect = QRect(QPoint(squareRect.size().width()*0.25, squareRect.topLeft().y() + margin),
                         QPoint(squareRect.size().width()*0.75, squareRect.bottomRight().y() - margin));

    // build slash polygon -------------------------------------
    QPolygon polygon;
    if (anchors == SM::Anchors::anchorsLeftRight)
        polygon << this->rect().bottomLeft()
                << this->rect().topLeft()
                << this->rect().topRight()
                << this->rect().bottomRight();
    else
        polygon << this->rect().topLeft()
                << this->rect().topRight()
                << this->rect().bottomRight()
                << this->rect().bottomLeft();

    bool isLeftRight(SM::Anchors::anchorsLeftRight == anchors);
    // first
    if (firstSide == SM::Anchors::anchorsLeft){
        if (isLeftRight)
            polygon[0].setX(polygon[0].x() + shift());
        else
            polygon[0].setY(polygon[0].y() + shift());
    }
    else if (firstSide == SM::Anchors::anchorsRight){
        if (isLeftRight)
            polygon[1].setX(polygon[1].x() + shift());
        else
            polygon[1].setY(polygon[1].y() + shift());
    }
    //second
    if (secondSide == SM::Anchors::anchorsLeft){
        if (isLeftRight)
            polygon[2].setX(polygon[2].x() - shift());
        else
            polygon[2].setY(polygon[2].y() - shift());
    }
    else if (secondSide == SM::Anchors::anchorsRight){
        if (isLeftRight)
            polygon[3].setX(polygon[3].x() - shift());
        else
            polygon[3].setY(polygon[3].y() - shift());
    }

    // work with palette ---------------------------------------
    if (m_Cache != nullptr)
        delete m_Cache;
    m_Cache = new QPixmap(size);
    m_Cache->fill(Qt::transparent);

    QPainter painter(m_Cache);
    painter.setRenderHint( QPainter::Antialiasing );
    painter.setBrush(this->palette().base());
    painter.setPen(QPen(this->palette().text(), 2));

    // draw ----------------------------------------------------
    painter.drawPolygon(polygon);
    painter.setFont(this->font());
    painter.drawText(textRect, this->text(), QTextOption(Qt::AlignCenter));
    this->setMask(m_Cache->mask());

    repaint();
    isRectUpdate = false;
}

int SM_SlashButton::calculateTextPixelLenght(const QFont &font)
{
    QRect r;
    int ret;
    if (anchors == SM::Anchors::anchorsLeftRight){
        r = QFontMetrics(font).boundingRect(this->text());
        ret = r.width()/* - r.y()*/;
    }
    else {
        r = QFontMetrics(font).boundingRect(this->text());
        ret = r.height() - r.x();
    }
    return ret + 2*margin;
}

void SM_SlashButton::setSquareRectanle(int w, int h)
{
    setSquareRectanle(QSize(w, h));
}

bool SM_SlashButton::setMargin(int m)
{
    if (m > 0){
        margin = m;
        reCalculateShifts();
        return true;
    }
    return false;
}

int SM_SlashButton::getMargin()
{
    return margin;
}

int SM_SlashButton::shift()
{
    if (anchors == SM::Anchors::anchorsTopBottom)
        return static_cast<int> (this->width() * tan(slashAngel * Pi / 180));
    else if (anchors == SM::Anchors::anchorsLeftRight)
        return static_cast<int> (this->height() * tan(slashAngel * Pi / 180));
    return 0;
}

bool SM_SlashButton::setSlashAnchors(const SM::Anchors &anchors)
{
    if (SM::isAnchorsOpposite(anchors)){
        this->anchors = anchors;
        reCalculateShifts();
        return true;
    }
    return false;
}

SM::Anchors SM_SlashButton::getSlashAnchors()
{
    return anchors;
}

bool SM_SlashButton::setSlashAngel(int angel)
{
    if (angel < 0 || angel > 65)
        return false;
    slashAngel = angel;
    reCalculateShifts();
    return true;
}

int SM_SlashButton::getSlashAngel()
{
    return slashAngel;
}

bool SM_SlashButton::setFirstSlashType(SM::Anchors type)
{
    if ( ! (SM::isAnchorsVertical(type)
            || type == SM::Anchors::anchorsNull))
        return false;
    firstSide = type;
    reCalculateShifts();
    return true;
}

bool SM_SlashButton::setSecondSlashType(SM::Anchors type)
{
    if ( ! (SM::isAnchorsVertical(type)
            || type == SM::Anchors::anchorsNull))
        return false;
    secondSide = type;
    reCalculateShifts();
    return true;
}

SM::Anchors SM_SlashButton::getFirstSlashType()
{
    return firstSide;
}

SM::Anchors SM_SlashButton::getSecondSlashType()
{
    return secondSide;
}

int SM_SlashButton::getBigShift()
{
    return bigShift;
}

int SM_SlashButton::getLittleShift()
{
    return littleShift;
}
