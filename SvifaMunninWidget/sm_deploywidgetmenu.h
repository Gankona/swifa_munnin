#ifndef SM_DEPLOYWIDGETMENU_H
#define SM_DEPLOYWIDGETMENU_H

#include <QFrame>
#include <QPushButton>
#include <QMap>
#include <QResizeEvent>
#include <QPainter>
#include <QPaintEvent>

#include "svifamunninwidget_global.h"
#include "sm_namespace.h"

class SMWIDGETSHARED_EXPORT SM_DeployWidgetMenu : public QFrame
{
    Q_OBJECT
private:
    QMap <QString, QPushButton*> buttonList;
    SM::Anchors anchors;
    QString currentChooseKey;
    int spaces ;

    void updateContent(QSize size);

protected:
    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

public:
    explicit SM_DeployWidgetMenu(QWidget *parent = nullptr);

    void setAnchors(SM::Anchors anchors);
    SM::Anchors getAnchors();

    void addButtonToMenu(QPushButton *button, QString key);
    void deleteButtonFromMenu(QPushButton *button, bool deleteButton = false);
    void deleteButtonFromMenu(QString *key, bool deleteButton = false);

signals:
    void clickedMenu(QString key);
};

#endif // SM_DEPLOYWIDGETMENU_H
