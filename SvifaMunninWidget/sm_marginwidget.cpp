#include "sm_marginwidget.h"

SM_MarginWidget::SM_MarginWidget(QWidget *parent,
                                 float scalablyValue,
                                 SM::Anchors anchors)
    : QWidget(parent),
      scalablyValue(scalablyValue),
      anchors(anchors),
      frame(nullptr)
{}

void SM_MarginWidget::updateContent(QSize size)
{
    if (frame == nullptr)
        return;

    try {
        frame->hasFocus();
    } catch (...){ return; }

    if (SM::isAnchorsLeft(anchors)
            || SM::isAnchorsRight(anchors))
        frame->setFixedWidth(size.width()*scalablyValue);
    else
        frame->setFixedWidth(size.width());
    if (SM::isAnchorsTop(anchors)
            || SM::isAnchorsBottom(anchors))
        frame->setFixedHeight(size.height()*scalablyValue);
    else
        frame->setFixedHeight(size.height());

    int x(0);
    int y(0);

    if (! SM::isAnchorsRight(anchors))
        x = size.width() - frame->width();
    else if (SM::isAnchorsLeft(anchors)
             && SM::isAnchorsRight(anchors))
        x = (size.width() - frame->width())/2;
    if (! SM::isAnchorsBottom(anchors))
        y = size.height() - frame->height();
    else if (SM::isAnchorsBottom(anchors)
             && SM::isAnchorsTop(anchors))
        y = (size.height() - frame->height())/2;

    frame->raise();
    frame->move(x, y);
}

void SM_MarginWidget::resizeEvent(QResizeEvent *event)
{
    updateContent(event->size());
}

void SM_MarginWidget::setWidget(QWidget *frame)
{
    this->frame = frame;
    this->frame->setParent(this);
    this->frame->show();
    updateContent(this->size());
}

void SM_MarginWidget::setMarginAnchors(SM::Anchors anchors)
{
    this->anchors = anchors;
    updateContent(this->size());
}

SM::Anchors SM_MarginWidget::getMarginAnchors()
{
    return anchors;
}

void SM_MarginWidget::setScalablyValue(float value)
{
    this->scalablyValue = value;
    updateContent(this->size());
}

float SM_MarginWidget::getScalablyValue()
{
    return scalablyValue;
}
