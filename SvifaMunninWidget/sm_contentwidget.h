#ifndef SM_CONTENTWIDGET_H
#define SM_CONTENTWIDGET_H

#include <QtWidgets/QFrame>
#include <sm_basewidget.h>
#include "svifamunninwidget_global.h"

class SMWIDGETSHARED_EXPORT SM_ContentWidget : public QFrame
{
    Q_OBJECT
private:
    QPushButton *nextButton;
    QPushButton *prevButton;
    QLabel *titleLabel;
    bool isMinimalFlag;
    int currentPosition;

    struct WidgetOne {
        QWidget *widget;
        int position;
        QString title;
    };

    QList  <WidgetOne*> widgetList;

    void reCompurePosition();
    int getFreePosition();

    void resizeEvent(QResizeEvent *event);
    void resizeContent(QSize size);


public:
    explicit SM_ContentWidget(QWidget *parent = nullptr);

    void addNewWidget(QString title, QWidget *widget);
    void deleteWidget(QWidget *widget);
    void deleteWidget(QString title);
    bool isEmpty();

signals:
    void s_deleteWidget(QWidget *widget);
};

#endif // SM_CONTENTWIDGET_H
