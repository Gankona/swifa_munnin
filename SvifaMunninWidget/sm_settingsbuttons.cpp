#include "sm_settingsbuttons.h"

SM_SettingsButtons::SM_SettingsButtons(QWidget *parent)
    : QFrame(parent)
{
    backButton = new QPushButton("back", this);
    cancelButton = new QPushButton("cancel", this);
    defaultButton = new QPushButton("default", this);
    saveButton = new QPushButton("save", this);

    QObject::connect(backButton,    &QPushButton::clicked, this, &SM_SettingsButtons::clickBack);
    QObject::connect(cancelButton,  &QPushButton::clicked, this, &SM_SettingsButtons::clickCancel);
    QObject::connect(defaultButton, &QPushButton::clicked, this, &SM_SettingsButtons::clickDefault);
    QObject::connect(saveButton,    &QPushButton::clicked, this, &SM_SettingsButtons::clickApply);
}

void SM_SettingsButtons::resizeEvent(QResizeEvent *event)
{
    QSize buttonSize(event->size().height()*4, event->size().height());
    if (4*buttonSize.width() > event->size().width())
        buttonSize.setWidth(event->size().width()/4);
    backButton->setFixedSize(buttonSize);
    cancelButton->setFixedSize(buttonSize);
    defaultButton->setFixedSize(buttonSize);
    saveButton->setFixedSize(buttonSize);

    saveButton->move(event->size().width() - buttonSize.width(), 0);
    cancelButton->move(event->size().width() - buttonSize.width()*2, 0);
    defaultButton->move(event->size().width() - buttonSize.width()*3, 0);
    backButton->move(event->size().width() - buttonSize.width()*4, 0);
}
