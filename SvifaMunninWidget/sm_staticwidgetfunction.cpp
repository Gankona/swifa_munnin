#include "sm_staticwidgetfunction.h"

// CheckBox
QCheckBox* SM_WF::checkBox(QString text, QSize size,
                                 QWidget* parent, int fontSize)
{
    QCheckBox *b = SM_WF::checkBox(text, parent, fontSize);
    b->setFixedSize(size);
    return b;
}

QCheckBox* SM_WF::checkBox(QString text, int w, int h,
                                 QWidget* parent, int fontSize)
{
    QCheckBox *b = SM_WF::checkBox(text, parent, fontSize);
    b->setFixedSize(w, h);
    return b;
}

QCheckBox* SM_WF::checkBox(QString text, QWidget* parent, int fontSize)
{
    QCheckBox *b = new QCheckBox(text, parent);

    QFont font;
    if (fontSize > 0)
        font.setPixelSize(fontSize);
    else if (parent != nullptr)
        font = parent->font();
    else
        font.setPointSize(14);
    b->setFont(font);

    return b;
}

// Label
QLabel* SM_WF::label(Qt::AlignmentFlag align)
{
    QLabel *l = new QLabel;
    l->setAlignment(align);
    return l;
}

QLabel* SM_WF::label(QWidget *parent, Qt::AlignmentFlag align)
{
    QLabel *l = new QLabel(parent);
    l->setAlignment(align);
    return l;
}

QLabel* SM_WF::label(QString text, QWidget* parent, QSize size)
{
    QLabel *l = new QLabel(text, parent);
    l->setFixedSize(size);
    return l;
}

QLabel* SM_WF::label(QString text, int w, int h, QWidget* parent)
{
    return SM_WF::label(text, parent, QSize(w, h));
}

QLabel* SM_WF::label(QString text, QSize size, QWidget* parent)
{
    return SM_WF::label(text, parent, size);
}

QLabel* SM_WF::label(QString text, QWidget* parent, int w, int h)
{
    return SM_WF::label(text, parent, QSize(w, h));
}

QLabel* SM_WF::label(QString text, Qt::AlignmentFlag align, QWidget* parent, QSize size)
{
    QLabel *l = SM_WF::label(text, parent, size);
    l->setAlignment(align);
    return l;
}

QLabel* SM_WF::label(QString text, Qt::AlignmentFlag align, QWidget *parent, int w, int h)
{
    return SM_WF::label(text, align, parent, QSize(w, h));
}

// LineEdit
QLineEdit* SM_WF::lineEdit(QWidget *parent, int w, int h, bool isFocus)
{
    QLineEdit *l = new QLineEdit(parent);
    l->setFixedSize(w, h);
    if (! isFocus)
        l->setFocusPolicy(Qt::NoFocus);
    return l;
}

QLineEdit* SM_WF::lineEdit(QWidget *parent, QSize size, bool isFocus)
{
    QLineEdit *l = new QLineEdit(parent);
    l->setFixedSize(size);
    if (! isFocus)
        l->setFocusPolicy(Qt::NoFocus);
    return l;
}

// Button
QPushButton* SM_WF::button(QString title, QWidget* parent,
                           bool isFlat, bool isChekable, bool isFocus)
{
    QPushButton *p = new QPushButton(title, parent);
    p->setFlat(isFlat);
    p->setCheckable(isChekable);
    isFocus ? p->setFocusPolicy(Qt::WheelFocus)
            : p->setFocusPolicy(Qt::NoFocus);
    return p;
}

QPushButton* SM_WF::button(QString title, QSize size,
                           bool isFlat, bool isChekable, bool isFocus)
{
    return SM_WF::button(title, nullptr, size, isFlat, isChekable, isFocus);
}

QPushButton* SM_WF::button(QString title, QWidget* parent, QSize size,
                           bool isFlat, bool isChekable, bool isFocus)
{
    QPushButton *p = SM_WF::button(title, parent, isFlat, isChekable, isFocus);
    p->setFixedSize(size);
    return p;
}

QPushButton* SM_WF::button(QString title, QWidget* parent, int w, int h,
                           bool isFlat, bool isChekable, bool isFocus)
{
    QPushButton *p = SM_WF::button(title, parent, isFlat, isChekable, isFocus);
    p->setFixedSize(w, h);
    return p;
}

// Button icon
QPushButton* SM_WF::iconButton(QIcon icon, QWidget *parent, QSize size,
                               bool isFlat, bool isChekable, bool isFocus)
{
    QPushButton *p = SM_WF::iconButton(icon, parent, isFlat, isChekable, isFocus);
    p->setFixedSize(size);
    return p;
}

QPushButton* SM_WF::iconButton(QIcon icon, QWidget *parent,
                               int  w, int h,
                               bool isFlat, bool isChekable, bool isFocus)
{
    QPushButton *p = SM_WF::iconButton(icon, parent, isFlat, isChekable, isFocus);
    p->setFixedSize(w, h);
    return p;
}

QPushButton* SM_WF::iconButton(QIcon icon, QWidget *parent,
                               bool isFlat, bool isChekable, bool isFocus)
{
    return SM_WF::iconTextButton(icon, "", parent, isFlat, isChekable, isFocus);
}

QPushButton* SM_WF::iconButton(QIcon icon, QString toolTip, QWidget *parent,
                               QSize size, bool isFlat, bool isChekable, bool isFocus)
{
    QPushButton *p = SM_WF::iconButton(icon, toolTip, parent, isFlat, isChekable, isFocus);
    p->setFixedSize(size);
    return p;
}

QPushButton* SM_WF::iconButton(QIcon icon, QString toolTip, QWidget *parent,
                               int  w, int h,
                               bool isFlat, bool isChekable, bool isFocus)
{
    QPushButton *p = SM_WF::iconButton(icon, toolTip, parent, isFlat, isChekable, isFocus);
    p->setFixedSize(w, h);
    return p;
}

QPushButton* SM_WF::iconButton(QIcon icon, QString toolTip, QWidget *parent,
                               bool isFlat, bool isChekable, bool isFocus)
{
    QPushButton *p = SM_WF::iconTextButton(icon, "", parent, isFlat, isChekable, isFocus);
    p->setToolTip(toolTip);
    return p;
}

QPushButton* SM_WF::iconTextButton(QIcon icon, QString title, QWidget *parent,
                                   QSize size, bool isFlat, bool isChekable, bool isFocus)
{
    QPushButton *p = SM_WF::iconTextButton(icon, title, parent, isFlat, isChekable, isFocus);
    p->setFixedSize(size);
    return p;
}

QPushButton* SM_WF::iconTextButton(QIcon icon, QString title, QWidget *parent,
                                   int w, int h, bool isFlat, bool isChekable, bool isFocus)
{
    QPushButton *p = SM_WF::iconTextButton(icon, title, parent, isFlat, isChekable, isFocus);
    p->setFixedSize(w, h);
    return p;
}

QPushButton* SM_WF::iconTextButton(QIcon icon, QString title, QWidget *parent,
                                   bool isFlat, bool isChekable, bool isFocus)
{
    QPushButton *p = new QPushButton(icon, title, parent);
    p->setFlat(isFlat);
    p->setCheckable(isChekable);
    isFocus ? p->setFocusPolicy(Qt::WheelFocus)
            : p->setFocusPolicy(Qt::NoFocus);
    return p;
}

QPushButton* SM_WF::iconTextButton(QIcon icon, QString title, bool isFlat, bool isChekable, bool isFocus)
{
    return SM_WF::iconTextButton(icon, title, nullptr, isFlat, isChekable, isFocus);
}
