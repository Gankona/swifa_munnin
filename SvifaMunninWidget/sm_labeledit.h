#ifndef SM_LABELEDIT_H
#define SM_LABELEDIT_H

#include <QtGui/QKeyEvent>
#include <QtGui/QResizeEvent>

#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

#include "sm_staticwidgetfunction.h"

class SMWIDGETSHARED_EXPORT SM_LabelEdit : public QWidget
{
    Q_OBJECT
private:
    QLineEdit *edit;
    QLabel *label;
    QLabel *title;
    QString oldText;
    QString buffer;
    QString text;
    QPushButton *button;
    bool editMode;
    bool titleMode;

    void resizeContent(QSize size);

protected:
    void resizeEvent(QResizeEvent *event);
    void keyPressEvent(QKeyEvent  *event);

public:
    SM_LabelEdit(QString title,
                 QString text,
                 QWidget *parent = nullptr,
                 bool isTitlePresent = true);
    SM_LabelEdit(QWidget *parent = nullptr);

    SM_LabelEdit(QString text,
                 QWidget *parent = nullptr);

    void openEdit();
    void closeEdit();
    void returnOldText();
    QString getOldText();
    QString getCurrentText();
    QString getTitle();
    void setTitle(QString title);
    void setTitleMode(bool isTitlePresent = true);
    bool isTitleMode();


signals:
    void textChange(QString);
};

#endif // SM_LABELEDIT_H
