#ifndef SM_STATICFUNCTION_H
#define SM_STATICFUNCTION_H

#include <QtCore/QSize>
#include <QtCore/QString>
#include <QtGui/QFont>
#include <QtGui/QFontInfo>
#include <QtGui/QIcon>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

#include "svifamunninwidget_global.h"

class SMWIDGETSHARED_EXPORT SM_WF
{
public:
    // CheckBox
    static QCheckBox* checkBox(QString text, QSize size = QSize(100, 25),
                               QWidget* parent = nullptr, int fontSize = -1);

    static QCheckBox* checkBox(QString text, int w, int h,
                               QWidget* parent = nullptr, int fontSize = -1);

    static QCheckBox* checkBox(QString text, QWidget* parent = nullptr, int fontSize = -1);

    // Label
    static QLabel* label(Qt::AlignmentFlag align);
    static QLabel* label(QWidget *parent, Qt::AlignmentFlag align);
    static QLabel* label(QString text, QWidget* parent, int w, int h);
    static QLabel* label(QString text, QSize size, QWidget* parent = nullptr);
    static QLabel* label(QString text, int w, int h, QWidget* parent = nullptr);
    static QLabel* label(QString text, QWidget* parent = nullptr, QSize size = QSize(100, 25));
    static QLabel* label(QString text, Qt::AlignmentFlag align, QWidget* parent, QSize size);
    static QLabel* label(QString text, Qt::AlignmentFlag align, QWidget* parent, int w = 100, int h = 25);

    // LineEdit
    static QLineEdit* lineEdit(QWidget *parent, int w, int h, bool isFocus = true);
    static QLineEdit* lineEdit(QWidget *parent, QSize size, bool isFocus = true);

    // Button
    static QPushButton* button(QString title, QWidget* parent = nullptr,
                               bool isFlat = false, bool isChekable = false, bool isFocus = true);

    static QPushButton* button(QString title, QSize size = QSize(100, 25),
                               bool isFlat = false, bool isChekable = false, bool isFocus = true);

    static QPushButton* button(QString title, QWidget* parent = nullptr, QSize size = QSize(100, 25),
                               bool isFlat = false, bool isChekable = false, bool isFocus = true);

    static QPushButton* button(QString title, QWidget* parent = nullptr, int w = 100, int h = 25,
                               bool isFlat = false, bool isChekable = false, bool isFocus = true);

    // Button icon
    static QPushButton* iconButton(QIcon icon, QWidget *parent, QSize size,
                                   bool isFlat = false, bool isChekable = false, bool isFocus = true);

    static QPushButton* iconButton(QIcon icon, QWidget *parent, int  w, int h,
                                   bool isFlat = false, bool isChekable = false, bool isFocus = true);

    static QPushButton* iconButton(QIcon icon, QWidget *parent = nullptr,
                                   bool isFlat = false, bool isChekable = false, bool isFocus = true);

    static QPushButton* iconButton(QIcon icon, QString toolTip, QWidget *parent, QSize size,
                                   bool isFlat = false, bool isChekable = false, bool isFocus = true);

    static QPushButton* iconButton(QIcon icon, QString toolTip, QWidget *parent, int  w, int h,
                                   bool isFlat = false, bool isChekable = false, bool isFocus = true);

    static QPushButton* iconButton(QIcon icon, QString toolTip, QWidget *parent,
                                   bool isFlat = false, bool isChekable = false, bool isFocus = true);

    static QPushButton* iconTextButton(QIcon icon, QString title, QWidget *parent = nullptr,
                                       QSize size = QSize(25, 25), bool isFlat = false,
                                       bool isChekable = false, bool isFocus = true);

    static QPushButton* iconTextButton(QIcon icon, QString title, QWidget *parent = nullptr,
                                       int w = 25, int h = 25, bool isFlat = false,
                                       bool isChekable = false, bool isFocus = true);

    static QPushButton* iconTextButton(QIcon icon, QString title, QWidget *parent = nullptr,
                                       bool isFlat = false, bool isChekable = false, bool isFocus = true);

    static QPushButton* iconTextButton(QIcon icon, QString title, bool isFlat = false,
                                       bool isChekable = false, bool isFocus = true);
};

#endif // SM_STATICFUNCTION_H
