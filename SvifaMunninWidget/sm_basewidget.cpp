#include "sm_basewidget.h"

SM_BaseWidget::SM_BaseWidget(QString title,
                             QWidget *contentWidget,
                             QWidget *parent)
    : QWidget(parent),
      title(title)
{
    titleLabel = new QLabel(title, this);
    groupButton = new SM_GroupButtonWidget(this);
    setWidget(contentWidget);
}

//SM::Anchors SM_BaseWidget::getAnchors()
//{
//    return anchors;
//}

//void SM_BaseWidget::setAnchors(SM::Anchors anchors)
//{
//    this->anchors = anchors;
//}

void SM_BaseWidget::setWidget(QWidget *widget)
{
    currentWidget = widget;
    resizeContent(this->size());
}

void SM_BaseWidget::resizeEvent(QResizeEvent *event)
{
    resizeContent(event->size());
}

//void SM_BaseWidget::setWidgetStatus(SM::Module status){}

void SM_BaseWidget::resizeContent(QSize size)
{
    const int sizeButton = 20;
    groupButton->setFixedSize(sizeButton*4, sizeButton);
    groupButton->move(size.width() - 4*sizeButton, 0);
    titleLabel->setFixedSize(size.width() - 4.5*sizeButton, sizeButton);
    titleLabel->move(sizeButton/2 - 1, 0);
    try {
        currentWidget->setFixedSize(size.width(), size.height() - sizeButton);
        currentWidget->move(0, sizeButton);
    }
    catch (...){}
}
