#include "sm_slashbuttonpanel.h"

SM_SlashButtonPanel::SM_SlashButtonPanel(QWidget *parent,
                                         SM::Anchors anchors,
                                         SM::Anchors first_first_anchors,
                                         SM::Anchors first_second_anchors,
                                         SM::Anchors second_first_anchors,
                                         SM::Anchors second_second_anchors)
    : QWidget(parent)
{
    if (! setAnchors(anchors))
        setAnchors(SM::Anchors::anchorsTop);
    if (! setFirstAnchors(first_first_anchors, first_second_anchors))
        setFirstAnchors(SM::Anchors::anchorsRight, SM::Anchors::anchorsRight);
    if (! setSecondAnchors(second_first_anchors, second_second_anchors))
        setSecondAnchors(SM::Anchors::anchorsLeft, SM::Anchors::anchorsLeft);
}

bool SM_SlashButtonPanel::setFirstAnchors(SM::Anchors first, SM::Anchors second)
{
    if ( ! (SM::isAnchorsVertical(first)
            || first == SM::Anchors::anchorsNull))
        return false;
    if ( ! (SM::isAnchorsVertical(second)
            || second == SM::Anchors::anchorsNull))
        return false;
    first_first_anchors = first;
    first_second_anchors = second;
    for (auto b : firstList){
        b->setFirstSlashType(first_first_anchors);
        b->setSecondSlashType(first_second_anchors);
    }
    resizeEvent(new QResizeEvent(this->size(), this->size()));
    return true;
}

bool SM_SlashButtonPanel::setSecondAnchors(SM::Anchors first, SM::Anchors second)
{
    if ( ! (SM::isAnchorsVertical(first)
            || first == SM::Anchors::anchorsNull))
        return false;
    if ( ! (SM::isAnchorsVertical(second)
            || second == SM::Anchors::anchorsNull))
        return false;
    second_first_anchors = first;
    second_second_anchors = second;
    for (auto b : secondList){
        b->setFirstSlashType(second_first_anchors);
        b->setSecondSlashType(second_second_anchors);
    }
    resizeEvent(new QResizeEvent(this->size(), this->size()));
    return true;
}

void SM_SlashButtonPanel::resizeEvent(QResizeEvent*)
{
    defineFrontSize();
    replaceButtons();
}

void SM_SlashButtonPanel::addFirstSlashButton(QString title, QString link)
{
    firstList.push_back(new SM_SlashButton(title, this, SM::Anchors::anchorsLeftRight,
                                           first_first_anchors, first_second_anchors));
    if (firstList.length() == 1)
        firstList.first()->setFirstSlashType(SM::Anchors::anchorsNull);
    firstList.last()->show();
    resizeEvent(new QResizeEvent(this->size(), this->size()));
    QObject::connect(firstList.last(), &SM_SlashButton::clicked, this, [=](){ emit linkActivate(link); });
}

void SM_SlashButtonPanel::addSecondSlashButton(QString title, QString link)
{
    if (secondList.length() != 0)
        secondList.first()->setSecondSlashType(second_second_anchors);
    secondList.push_front(new SM_SlashButton(title, this, SM::Anchors::anchorsLeftRight,
                                            second_first_anchors, SM::Anchors::anchorsNull));
    secondList.first()->show();
    resizeEvent(new QResizeEvent(this->size(), this->size()));
    QObject::connect(secondList.first(), &SM_SlashButton::clicked, this, [=](){ emit linkActivate(link); });
}

bool SM_SlashButtonPanel::isFontFit(const QFont &font)
{
    int size(0);
    for (auto b : firstList){
        size += b->calculateTextPixelLenght(font);
        size += b->shift();
    }
    for (auto b : secondList){
        size += b->calculateTextPixelLenght(font);
        size += b->shift();
    }

    if (SM::isAnchorsHorizontal(anchors)){
        if (this->size().width() > size
                && this->height() >= QFontMetrics(font).height()*2)
            return true;
    }
    if (SM::isAnchorsVertical(anchors)){
        if (this->size().height() > size
                && this->width() >= QFontMetrics(font).height()*2)
            return true;
    }
    return false;
}

void SM_SlashButtonPanel::replaceButtons()
{
    int shift(0);
    if (SM::isAnchorsHorizontal(anchors)){
        for (auto b : firstList){
            int bShift = b->calculateTextPixelLenght(this->font());
            b->setSquareRectanle(bShift, this->height());
            b->move(shift, 0);
            shift += bShift;
            shift += b->getLittleShift();
        }
        shift = this->width();
        for (auto b : secondList){
            int bShift = b->calculateTextPixelLenght(this->font());
            b->setSquareRectanle(bShift, this->height());
            shift -= (b->getLittleShift() + bShift);
            b->move(shift, 0);
        }
    }
    else {
        for (auto b : firstList){
            int bShift = b->calculateTextPixelLenght(this->font());
            b->setSquareRectanle(this->width(), bShift);
            b->move(0, shift);
            shift += bShift;
            shift += b->getLittleShift();
        }
        shift = this->width();
        for (auto b : secondList){
            int bShift = b->calculateTextPixelLenght(this->font());
            b->setSquareRectanle(this->width(), bShift);
            shift -= (b->getBigShift() + b->getLittleShift() + bShift);
            b->move(0, shift);
        }
    }
}

void SM_SlashButtonPanel::defineFrontSize()
{
    QFont tfont = this->font();

    tfont.setPixelSize(1);
    while (isFontFit(tfont))
        tfont.setPixelSize(tfont.pixelSize() + 1);

    this->setFont(tfont);
    for (auto b : firstList)
        b->setFont(this->font());
    for (auto b : secondList)
        b->setFont(this->font());
}

SM::Anchors SM_SlashButtonPanel::getAnchors()
{
    return anchors;
}

bool SM_SlashButtonPanel::setAnchors(SM::Anchors anchors)
{
    if (SM::isAnchorsOneWay(anchors)){
        this->anchors = anchors;
        resizeEvent(new QResizeEvent(this->size(), this->size()));
        return true;
    }
    return false;
}

void SM_SlashButtonPanel::clearFirstList()
{
    for (auto b : firstList)
        delete b;
    firstList.clear();
    resizeEvent(new QResizeEvent(this->size(), this->size()));
}

void SM_SlashButtonPanel::clearSecondList()
{
    for (auto b : secondList)
        delete b;
    secondList.clear();
    resizeEvent(new QResizeEvent(this->size(), this->size()));
}

void SM_SlashButtonPanel::clearAll()
{
    for (auto b : firstList)
        delete b;
    firstList.clear();
    clearSecondList();
}
