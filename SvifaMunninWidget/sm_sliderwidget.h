#ifndef SM_SLIDERWIDGET_H
#define SM_SLIDERWIDGET_H

#include <QSlider>
#include <QFrame>
#include <QLabel>
#include <QResizeEvent>
#include "sm_namespace.h"

struct SliderList {
    QSlider *slider = nullptr;
    QLabel *label = nullptr;
    QString key = "";
    QString title = "";
    int position = -1;
};

class SM_SliderWidget : public QFrame
{
    Q_OBJECT
private:
    QLabel titleLabel;

    SM::Anchors anchors;
    QList <SliderList*> sliderList;
    QString text;

    bool isRelation;
    float relatio;
    int firstRelatioSlider;
    int secondRelatioSlider;

    int getFreePosition();
    bool isPositionPresent(int position);

    SliderList* getSliderItem(QString key);
    SliderList* getSliderItem(int position);

protected:
    void resizeEvent(QResizeEvent *event);

public:
    SM_SliderWidget(QWidget *parent = 0, QString text = "");
    ~SM_SliderWidget();

    void addSlider(QSlider* slider, QString title = "");
    void addSlider(QString key, QString title, int max, int min, int interval = 1);
    void addSlider(QString key, int max, int min, int interval = 1);

    void clear();
    void removeAllSliders();
    void removeSliderByTitle(QString title);
    void removeSliderByKey(QString key);
    void removeSliderByPosition(int number);
    void setRelation(float value, int first = 0, int second = 1);
    void setRelationStatus(bool isRelation);
    void setText(QString text);

    bool setAnchors(SM::Anchors anchors);
    bool setValue(int value, QString key);
    bool setValue(int value, int position = 0);

    int getValue(QString key);
    int getValue(int position);
    int sliderLenght();
    QString getText();
    SM::Anchors getAnshors();

signals:
    void valueUpdate(QString key, int value, int oldValue);
};

#endif // SM_SLIDERWIDGET_H
