#ifndef SM_OPACITYWIDGET_H
#define SM_OPACITYWIDGET_H

#include <QtWidgets/QGraphicsOpacityEffect>
#include <QtWidgets/QFrame>
#include "svifamunninwidget_global.h"

class SMWIDGETSHARED_EXPORT SM_OpacityWidget : public QFrame
{
private:
    QGraphicsOpacityEffect *opacity;

public:
    SM_OpacityWidget(qreal opacityEffect = 1,
                     QWidget *parent = nullptr);
    void setOpacity(qreal opacity);
    qreal getOpacityEffect();
};

#endif // SM_OPACITYWIDGET_H
