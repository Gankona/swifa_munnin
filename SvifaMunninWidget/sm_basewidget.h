#ifndef SM_BASEWIDGET_H
#define SM_BASEWIDGET_H

#include <QWidget>
#include <QResizeEvent>
#include <QtWidgets>
#include "sm_namespace.h"
#include "sm_groupbuttonwidget.h"
#include "svifamunninwidget_global.h"

class SMWIDGETSHARED_EXPORT SM_BaseWidget : public QWidget
{
    Q_OBJECT
private:
    QString title;
    SM::Anchors anchors;

    QLabel *titleLabel;
    QWidget *currentWidget;
    SM_GroupButtonWidget *groupButton;

    void resizeContent(QSize size);

protected:
    void resizeEvent(QResizeEvent *event);

public:
    explicit SM_BaseWidget(QString title,
                           QWidget *contentWidget = nullptr,
                           QWidget *parent = nullptr);
    void setWidget(QWidget* widget);
//    SM::Anchors getAnchors();
//    void setAnchors(SM::Anchors anchors);
//    void setWidgetStatus(SM::Module status);

signals:
    void changeWidgetStatus(SM::Module newStatus);
};

#endif // SM_BASEWIDGET_H
