#-------------------------------------------------
#
# Project created by QtCreator 2016-07-20T17:13:04
#
#-------------------------------------------------

include($$PWD/../path.pri)

QT += widgets
CONFIG += c++14

TARGET = SvifaMunninWidget
TEMPLATE = lib
DESTDIR = $$BIN_PATH/lib
CONFIG += build_all

LIBS += -L$$BIN_PATH/lib/ -lSvifaMunninCore
INCLUDEPATH += $$SRC_PATH/SvifaMunninCore
DEPENDPATH  += $$SRC_PATH/SvifaMunninCore

DEFINES += SVIFAMUNNINWIDGET_LIBRARY

SOURCES += \
    sm_module.cpp \
    sm_basewidget.cpp \
    sm_panelbarwidget.cpp \
    sm_contentwidget.cpp \
    sm_groupbuttonwidget.cpp \
    sm_opacitywidget.cpp \
    sm_staticwidgetfunction.cpp \
    sm_deploywidgetmenu.cpp \
    sm_marginwidget.cpp \
    sm_settingsbuttons.cpp \
    sm_labeledit.cpp \
    sm_horizontallistwidget.cpp \
    sm_switcherbutton.cpp \
    sm_apppalette.cpp \
    sm_itempalette.cpp \
    sm_slashbutton.cpp \
    sm_slashbuttonpanel.cpp \
    sm_sliderwidget.cpp

HEADERS += \
    svifamunninwidget_global.h \
    sm_module.h \
    sm_basewidget.h \
    sm_panelbarwidget.h \
    sm_contentwidget.h \
    sm_groupbuttonwidget.h \
    sm_opacitywidget.h \
    sm_staticwidgetfunction.h \
    sm_deploywidgetmenu.h \
    sm_marginwidget.h \
    sm_settingsbuttons.h \
    sm_labeledit.h \
    sm_horizontallistwidget.h \
    sm_switcherbutton.h \
    sm_apppalette.h \
    sm_itempalette.h \
    sm_slashbutton.h \
    sm_slashbuttonpanel.h \
    sm_sliderwidget.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

RESOURCES += \
    sm_w_res.qrc
