#ifndef SM_SLASHBUTTONPANEL_H
#define SM_SLASHBUTTONPANEL_H

#include <QtCore/QList>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QWidget>

#include "sm_namespace.h"
#include "sm_slashbutton.h"

class SM_SlashButtonPanel : public QWidget
{
    Q_OBJECT
private:
    // second parametr its a link key, emit when button clicked
    QList <SM_SlashButton*> firstList;
    QList <SM_SlashButton*> secondList;

    SM::Anchors anchors;
    SM::Anchors first_first_anchors;
    SM::Anchors first_second_anchors;
    SM::Anchors second_first_anchors;
    SM::Anchors second_second_anchors;

    inline void defineFrontSize();
    inline void replaceButtons();
    bool isFontFit(const QFont &font);

protected:
    void resizeEvent(QResizeEvent*);

public:
    SM_SlashButtonPanel(QWidget *parent = nullptr,
                        SM::Anchors anchors = SM::Anchors::anchorsTop,
                        SM::Anchors first_first_anchors = SM::Anchors::anchorsRight,
                        SM::Anchors first_second_anchors = SM::Anchors::anchorsRight,
                        SM::Anchors second_first_anchors = SM::Anchors::anchorsRight,
                        SM::Anchors second_second_anchors = SM::Anchors::anchorsRight);

    SM::Anchors getAnchors();
    bool setAnchors(SM::Anchors anchors);

    bool setFirstAnchors(SM::Anchors first, SM::Anchors second);
    bool setSecondAnchors(SM::Anchors first, SM::Anchors second);

    void addFirstSlashButton(QString title, QString link = "");
    void addSecondSlashButton(QString title, QString link = "");

    void clearFirstList();
    void clearSecondList();
    void clearAll();

signals:
    void linkActivate(QString link);
};

#endif // SM_SLASHBUTTONPANEL_H
