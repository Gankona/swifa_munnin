#ifndef SM_MODULE_H
#define SM_MODULE_H

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtGui/QIcon>

#include "sm_basewidget.h"
#include "sm_namespace.h"
#include "svifamunninwidget_global.h"

class SMWIDGETSHARED_EXPORT SM_Module : public QObject
{
    Q_OBJECT
private:
    QString login;
    SM::Module status;
    QWidget *mainWidget;
    //SM_BaseWidget *mainWidget;

protected:
    virtual bool setWidget(QWidget *widget) final;
    virtual QString getLogin() final;

    virtual void closeApp() = 0;
    virtual void start() = 0;
    virtual void stop() = 0;

public:
    SM_Module(QString login);
    virtual ~SM_Module() = default;

public slots:
    virtual QIcon getIcon();
    virtual QString getModuleName(QString language = "");

    virtual SM::Module getStatus() final;
    virtual void setStatus(SM::Module status) final;

    virtual void catchSignal(QString key,
                             QString message,
                             QString sender) = 0;

    virtual void setDataFromFile(QString title,
                                 QByteArray byteData) = 0;

signals:
    void setLog(SM::TypeMsg type,
                QString sender,
                QString message,
                QString sendKey = "",
                QString deliveryKey = "",
                QString receiver = "client");

    bool setDataByte(QString title,
                     QByteArray byteData,
                     QString keyEncrypt = "",
                     QString newTitle = "",
                     bool isDate = false,
                     QStringList tags = QStringList{});

    bool getDataByte(QString titles,
                     QString keyEncrypt = "");

    void changeStatus(SM::Module status);
    void createWidget(QWidget *widget);
    QStringList getNotificationFiles();
};

#define SM_Module_iid "org.gankona.SM.Module"
Q_DECLARE_INTERFACE(SM_Module, SM_Module_iid)

#endif // SM_MODULE_H
