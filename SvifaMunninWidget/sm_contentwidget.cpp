#include "sm_contentwidget.h"

SM_ContentWidget::SM_ContentWidget(QWidget *parent)
    : QFrame(parent),
      isMinimalFlag(false),
      currentPosition(-1)
{
    nextButton = new QPushButton(">", this);
    prevButton = new QPushButton("<", this);
    titleLabel = new QLabel(this);
    resizeContent(this->size());

    QObject::connect(nextButton, &QPushButton::clicked, this, [=](){
        if (widgetList.length() <= 1)
            return;
        int currentPos(++currentPosition);
        bool isPresent(false);
        for (auto w : widgetList)
            if (w->position == currentPos){
                w->widget->raise();
                titleLabel->setText(w->title);
                isPresent = true;
                break;
            }
        if (! isPresent){
            currentPos = 0;
            for (auto w : widgetList)
                if (w->position == 0){
                    w->widget->raise();
                    titleLabel->setText(w->title);
                }
        }
        currentPosition = currentPos;
        resizeContent(this->size());
    });
    QObject::connect(prevButton, &QPushButton::clicked, this, [=](){
        if (widgetList.length() <= 1)
            return;
        int currentPos(--currentPosition);
        bool isPresent(false);
        for (auto w : widgetList)
            if (w->position == currentPos){
                w->widget->raise();
                titleLabel->setText(w->title);
                isPresent = true;
                break;
            }
        if (! isPresent){
            currentPos = getFreePosition()-1;
            for (auto w : widgetList)
                if (w->position == currentPos){
                    w->widget->raise();
                    titleLabel->setText(w->title);
                }
        }
        currentPosition = currentPos;
        resizeContent(this->size());
    });
}

void SM_ContentWidget::reCompurePosition()
{
    for (int i = 0; i < INT_MAX; i++){
        bool isPresent(false);
        for (auto w : widgetList)
            if (w->position == i)
                isPresent = true;
        if (! isPresent){
            bool isMorePresent(false);
            for (auto w : widgetList)
                if (w->position > i){
                    w->position--;
                    isMorePresent = true;
                }
            if (isMorePresent)
                return;
        }
    }
}

bool SM_ContentWidget::isEmpty()
{
    return widgetList.length() == 0;
}

int SM_ContentWidget::getFreePosition()
{
    reCompurePosition();
    for (int i = 0; i < INT_MAX; i++){
        bool isPresent(false);
        for (auto w : widgetList)
            if (w->position == i)
                isPresent = true;
        if (! isPresent)
            return i;
    }
    return -1;
}

void SM_ContentWidget::resizeContent(QSize size)
{
    if (isMinimalFlag){}
    else {
        QSize bSize(20, 20);
        if (widgetList.length() > 1){
            nextButton->setVisible(true);
            nextButton->setFixedSize(bSize);
            nextButton->move(0, 0);
            prevButton->setVisible(true);
            prevButton->setFixedSize(bSize);
            prevButton->move(size.width() - bSize.width(), 0);
            titleLabel->setFixedSize(size.width() - 2*bSize.width(), bSize.height());
            titleLabel->move(bSize.width(), 0);
        }
        else {
            nextButton->setVisible(false);
            prevButton->setVisible(false);
            titleLabel->setFixedSize(size.width(), bSize.height());
            titleLabel->move(0, 0);
        }
        for (auto w : widgetList){
            w->widget->setFixedSize(size.width(), size.height() - bSize.height());
            w->widget->move(0, bSize.height());
        }
    }
}

void SM_ContentWidget::resizeEvent(QResizeEvent *event)
{
    resizeContent(event->size());
}

void SM_ContentWidget::addNewWidget(QString title, QWidget *widget)
{
    widgetList.push_back(new WidgetOne{widget, getFreePosition(), title});
    resizeContent(this->size());
}

void SM_ContentWidget::deleteWidget(QString title)
{
    for (auto w : widgetList)
        if (w->title == title){
            emit s_deleteWidget(w->widget);
            delete w;
            reCompurePosition();
            resizeContent(this->size());
            break;
        }
    resizeContent(this->size());
}

void SM_ContentWidget::deleteWidget(QWidget *widget)
{
    for (auto w : widgetList)
        if (w->widget == widget){
            emit s_deleteWidget(widget);
            delete w;
            reCompurePosition();
            resizeContent(this->size());
            break;
        }
    resizeContent(this->size());
}
