#include "sm_groupbuttonwidget.h"

SM_GroupButtonWidget::SM_GroupButtonWidget(QWidget *parent,
                                           QSize buttonSize,
                                           qreal opacityEffect,
                                           SM::Anchors anchors)
    : SM_GroupButtonWidget(buttonSize, parent, opacityEffect, anchors)
{}

SM_GroupButtonWidget::SM_GroupButtonWidget(QSize buttonSize,
                                           QWidget *parent,
                                           qreal opacityEffect,
                                           SM::Anchors anchors)
    : SM_OpacityWidget(opacityEffect, parent)
{
    this->setFixedSize(buttonSize.width()*4, buttonSize.height());
    closeButton    = SM_WF::iconButton(style()->standardIcon(QStyle::SP_DialogCloseButton),
                                       "close", this, QSize(25, 25), false, true, false);
    rollUpButton   = SM_WF::iconButton(style()->standardIcon(QStyle::SP_TitleBarMinButton),
                                       "down", this, QSize(25, 25), false, true, false);
    minimizeButton = SM_WF::iconButton(style()->standardIcon(QStyle::SP_TitleBarNormalButton),
                                       "minimum", this, QSize(25, 25), false, true, false);
    fullViewButton = SM_WF::iconButton(style()->standardIcon(QStyle::SP_TitleBarCloseButton),
                                       "full", this, QSize(25, 25), false, true, false);
    setAnchors(anchors);

    QObject::connect(rollUpButton, &QPushButton::clicked, this, [=](){
        status = SM::Module::moduleBackgroundUse;
        setChecked();
        emit changeWidgetStatus(SM::Module::moduleBackgroundUse);
    });
    QObject::connect(minimizeButton, &QPushButton::clicked, this, [=](){
        status = SM::Module::moduleSimplifiedUse;
        setChecked();
        emit changeWidgetStatus(SM::Module::moduleSimplifiedUse);
    });
    QObject::connect(fullViewButton, &QPushButton::clicked, this, [=](){
        status = SM::Module::moduleEntireUse;
        setChecked();
        emit changeWidgetStatus(SM::Module::moduleEntireUse);
    });
    QObject::connect(closeButton, &QPushButton::clicked, this, [=](){
        status = SM::Module::moduleNotUse;
        setChecked();
        emit changeWidgetStatus(SM::Module::moduleNotUse);
    });
}

void SM_GroupButtonWidget::resizeEvent(QResizeEvent *event)
{
    resizeContent(event->size());
}

void SM_GroupButtonWidget::resizeContent(QSize size)
{
    QSize s(size.width()/4, size.height());

    switch (anchors){
    case SM::Anchors::anchorsLeft:
    case SM::Anchors::anchorsRight:
        s = QSize(s.height(), s.width());
        rollUpButton->move(0, 0);
        minimizeButton->move(s.width(), 0);
        fullViewButton->move(s.width()*2, 0);
        closeButton->move(s.width()*3, 0);
    default:
        rollUpButton->move(0, 0);
        minimizeButton->move(0, s.width());
        fullViewButton->move(0, s.width()*2);
        closeButton->move(0, s.width()*3);
    }

    rollUpButton->setFixedSize(s);
    minimizeButton->setFixedSize(s);
    fullViewButton->setFixedSize(s);
    closeButton->setFixedSize(s);
}

void SM_GroupButtonWidget::setAnchors(SM::Anchors anchors)
{
    this->anchors = anchors;
    resizeContent(this->size());
}

SM::Anchors SM_GroupButtonWidget::getAnchors()
{
    return anchors;
}

void SM_GroupButtonWidget::setChecked()
{
    rollUpButton->setChecked(status == SM::Module::moduleBackgroundUse);
    minimizeButton->setChecked(status == SM::Module::moduleSimplifiedUse);
    fullViewButton->setChecked(status == SM::Module::moduleEntireUse);
    closeButton->setChecked(status == SM::Module::moduleNotUse);
}

void SM_GroupButtonWidget::setStatus(SM::Module status)
{
    this->status = status;
    setChecked();
}

SM::Module SM_GroupButtonWidget::getStatus()
{
    return status;
}
