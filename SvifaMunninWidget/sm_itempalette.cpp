#include "sm_itempalette.h"

SM_ItemPalette::SM_ItemPalette(QObject *parent)
    : QObject(parent)
{
    setBackgroundColor(0x88, 0x88, 0x88);
    setButtonColor(0xd0, 0xd0, 0xd0);
    setTextColor(0, 0, 0);
}

QColor SM_ItemPalette::background()
{
    return backgroundColor;
}

QColor SM_ItemPalette::button()
{
    return buttonColor;
}

QColor SM_ItemPalette::text()
{
    return textColor;
}

void SM_ItemPalette::setBackgroundColor(QColor color)
{
    backgroundColor = color;
    emit colorChange();
}

void SM_ItemPalette::setBackgroundColor(int red, int green, int blue, int opacity)
{
    setBackgroundColor(QColor(red, green, blue, opacity));
}

void SM_ItemPalette::setButtonColor(QColor color)
{
    buttonColor = color;
    emit colorChange();
}

void SM_ItemPalette::setButtonColor(int red, int green, int blue, int opacity)
{
    setButtonColor(QColor(red, green, blue, opacity));
}

void SM_ItemPalette::setTextColor(QColor color)
{
    textColor = color;
    emit colorChange();
}

void SM_ItemPalette::setTextColor(int red, int green, int blue, int opacity)
{
    setTextColor(QColor(red, green, blue, opacity));
}

QDataStream& operator << (QDataStream &d, SM_ItemPalette &p)
{
    return d << p.backgroundColor << p.buttonColor << p.textColor;
}

QDataStream& operator >> (QDataStream &d, SM_ItemPalette &p)
{
    return d >> p.backgroundColor >> p.buttonColor >> p.textColor;
}
