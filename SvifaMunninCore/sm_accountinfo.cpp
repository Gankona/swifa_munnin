#include "sm_accountinfo.h"

SM_AccountInfo::SM_AccountInfo(QString login,
                               QString firstName,
                               QString lastName,
                               QString email,
                               QString telephon,
                               QString country)
    : login(login),
      firstName(firstName),
      lastName(lastName),
      email(email),
      telephon(telephon),
      country(country){}

SM_AccountInfo::SM_AccountInfo(const SM_AccountInfo &a)
    : login(a.login),
      firstName(a.firstName),
      lastName(a.lastName),
      email(a.email),
      telephon(a.telephon),
      country(a.country){}

SM_AccountInfo::SM_AccountInfo(const QByteArray &b)
{
    QDataStream stream(b);
    stream >> *this;
}

QByteArray SM_AccountInfo::toByteArray()
{
    QByteArray byte;
    QDataStream stream(byte);
    stream << *this;
    return byte;
}

SM_AccountInfo SM_AccountInfo::getAccountFromTier(QString str)
{
    SM_AccountInfo account(SM_CF::getTier(str),
                           SM_CF::getTier(str, 1),
                           SM_CF::getTier(str, 2),
                           SM_CF::getTier(str, 3),
                           SM_CF::getTier(str, 4),
                           SM_CF::getTier(str, 5));
    return account;
}

QString SM_AccountInfo::getTierString(SM_AccountInfo account)
{
    return SM_CF::unite({account.login,    account.firstName,
                                             account.lastName, account.email,
                                             account.telephon, account.country});
}
