#include "sm_pushbuttoneventeater.h"

SM_PushButtonEventEater::SM_PushButtonEventEater(QObject *parent) : QObject(parent)
{

}

bool SM_PushButtonEventEater::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::Leave)
        emit leaveWidget();
    else if (event->type() == QEvent::Enter)
        emit enterWidget();
    else
        return QObject::eventFilter(obj, event);
    return true;
}
