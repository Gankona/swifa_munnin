#ifndef SM_BINDMANAGER_H
#define SM_BINDMANAGER_H

#include <QtCore/QDateTime>
#include <QtCore/QDebug>
#include <QtCore/QObject>
#include <QtCore/QString>

#include "sm_namespace.h"
#include "sm_staticcorefunction.h"

class SM_BindManager : public QObject
{
    Q_OBJECT
private:
    void setLog(SM::TypeMsg type,
                QString sender,
                QString message,
                QString sendKey = "",
                QString deliveryKey = "");

public:
    explicit SM_BindManager(QObject *parent = nullptr);

    virtual void setMessage(SM::TypeMsg type,
                            QString sender,
                            QString message) final;

    virtual void setResult(bool resultValue,
                           QString deliveryKey,
                           QString sender,
                           QString message = "") final;

    virtual void setError(QString deliveryKey,
                          QString sender,
                          QString message = "") final;

    virtual void setAright(QString deliveryKey,
                           QString sender,
                           QString message = "") final;

    virtual void setLink(QString link,
                         QString sender) final;

    virtual void setSignal(QString sendKey,
                           QString sender,
                           QString message = "",
                           QString deliveryKey = "") final;

    virtual void setGlobalSignal(QString sendKey,
                                 QString sender,
                                 QString message = "",
                                 QString deliveryKey = "") final;

signals:
    void openLink(QString link,
                  QString fullLink);

    void signalInternal(QString sendKey,
                        QString message,
                        QString deliveryKey);

    void signalGlobal(QString sendKey,
                      QString message,
                      QString deliveryKey);

    void result(bool result,
                QString deliveryKey,
                QString message);

    void logUpdate(SM::TypeMsg type,
                   QString message,
                   QString sendKey,
                   QString deliveryKey,
                   QString sender);
};

#endif // SM_BINDMANAGER_H
