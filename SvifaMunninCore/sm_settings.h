#ifndef SM_SETTINGS_H
#define SM_SETTINGS_H

#include <QtCore/QObject>
#include <QtCore/QVariant>
#include "sm_staticcorefunction.h"

class SM_Settings : public QObject
{
    Q_OBJECT
private:
    QString groupKey;
    QMap <QString /*key*/, QVariant/*value*/> settingsMap;

public:
    SM_Settings() = default;

    virtual void clear() final;
    virtual void setValue(QString key, QVariant value) final;
    virtual QVariant value(QString key, QVariant defaultValue = 0) final;
    virtual QStringList getAllKeys() final;

    virtual void beginGroup(QString name) final;
    virtual void endGroup() final;

    virtual QString getFullKey(QString key) final;
};

#endif // SM_SETTINGS_H
