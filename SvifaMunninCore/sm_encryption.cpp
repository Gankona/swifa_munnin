#include "sm_encryption.h"

QByteArray SM_Encryption::encryptFile(QByteArray array,
                                      QString key)
{
    QBitArray bitArray = toBitArray(array);
    QBitArray result = bitArray;
    int sizeCrypt = getSizeCrypt(key);
    //int keyEnterNumber = getKeyEnterNumber(key, bitArray.size()/sizeCrypt);
    QBitArray vectorCrypt = getVectorCrypt(key);
    for (int i = 0; i < bitArray.size()/sizeCrypt; i++){
        QBitArray b(sizeCrypt);
        for (int j = 0; j < sizeCrypt; j++)
            b.setBit(j, bitArray.at(i*sizeCrypt+j));
        b = unionVector(b, vectorCrypt);
        for (int j = 0; j < sizeCrypt; j++)
            result.setBit(i*sizeCrypt+j, b.at(j));
    }
    return toByteArray(result);
}

QByteArray SM_Encryption::decryptFile(QByteArray array,
                                      QString key)
{
    QBitArray bitArray = toBitArray(array);
    QBitArray result = bitArray;
    int sizeCrypt = getSizeCrypt(key);
    //int keyEnterNumber = getKeyEnterNumber(key, bitArray.size()/sizeCrypt);
    QBitArray vectorCrypt = getVectorCrypt(key);
    for (int i = 0; i < bitArray.size()/sizeCrypt; i++){
        QBitArray b(sizeCrypt);
        for (int j = 0; j < sizeCrypt; j++)
            b.setBit(j, bitArray.at(i*sizeCrypt+j));
        b = unionVector(b, vectorCrypt);
        for (int j = 0; j < sizeCrypt; j++)
            result.setBit(i*sizeCrypt+j, b.at(j));
    }
    return toByteArray(result);
}

int SM_Encryption::getSizeCrypt(QString key)
{
    int summa(0);
    foreach (QChar c, key)
        summa += c.unicode();
    return (summa%8)*2+4;
}

int SM_Encryption::getKeyEnterNumber(QString key,
                                     int sizeBlock)
{
    int res(0);
    foreach (QChar c, key)
        res += c.unicode();
    res /= key.length();
    if (res >= sizeBlock)
        res %= sizeBlock;
    return res;
}

QBitArray SM_Encryption::getVectorCrypt(QString key)
{
    QBitArray res(getSizeCrypt(key));
    //одиночные
    for (QChar c : key)
        res = unionVector(res, roundVector(res.size(), c.unicode(), false));

    //деленные на остаток от длины одиночные
    for (QChar c : key)
        res = unionVector(res, roundVector(res.size(), c.unicode()%key.length(), true));

    return res;
}

QBitArray SM_Encryption::roundVector(int size,
                                         int key,
                                         bool isReverse)
{
    QBitArray res(size);
    int cur(0);
    if (isReverse){
        cur = size-1;
        while (key != 0){
            res.setBit(cur, key % 2);
            key /= 2;
            cur--;
            if (cur < 0)
                cur = size -1;
        }
    }
    else {
        while (key != 0){
            res.setBit(cur, key % 2);
            key /= 2;
            cur++;
            cur %= size;
        }
    }
    return res;
}

QBitArray SM_Encryption::unionVector(QBitArray first,
                                     QBitArray second)
{
    if (first.size() == second.size())
        for (int i = 0; i < first.size(); i++)
            first.setBit(i, first[i] ^ second[i]);
    return first;
}

QBitArray SM_Encryption::toBitArray(QByteArray bytes)
{
    QBitArray bits(bytes.count()*8);

    // Convert from QByteArray to QBitArray
    for(int i = 0; i < bytes.count(); ++i)
        for(int b = 0; b < 8; ++b)
            bits.setBit(i*8+b, bytes.at(i)&(1<<b));
    return bits;
}

QByteArray SM_Encryption::toByteArray(QBitArray bits)
{
    QByteArray bytes;

    // Convert from QBitArray to QByteArray
    for(int b=0; b<bits.count(); ++b)
        bytes[b/8] = (bytes.at(b/8) | ((bits[b]?1:0)<<(b%8)));
    return bytes;
}
