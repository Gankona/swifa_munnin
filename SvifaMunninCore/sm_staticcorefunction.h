#ifndef SM_STATICCOREFUNCTION_H
#define SM_STATICCOREFUNCTION_H

#include <QtCore/QString>
#include <QtCore/QStringList>

#include <initializer_list>

#include "svifamunnincore_global.h"

class SMCORESHARED_EXPORT SM_CF
{
public:
    // work with tier string
    static int getTierLenght(QString text);
    static QString getTier(QString text, int tier = 0);
    static QString replaceTier(QString text, QString reText, int tier = 0);
    static QString unite(std::initializer_list<QString> list);
    static QString removeFirstTier(QString text);
    static QString getTierInterval(QString text, int second);
    static QString getTierInterval(QString text, int first, int second);
    static QStringList getTierList(QString text);

    static QString setWrapping(QString text, int lenght = 30);
    static QString getRandomKey(int lenght);
    static QString boolToString(bool v);
    static QString getKeyValue(int i, Qt::KeyboardModifiers m);
    static QString getKeyValue(int i);
};

#endif // SM_STATICCOREFUNCTION_H
