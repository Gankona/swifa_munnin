#include "sm_settings.h"

void SM_Settings::beginGroup(QString name)
{
    groupKey = SM_CF::unite({groupKey, name});
}

void SM_Settings::endGroup()
{
    groupKey = SM_CF::removeFirstTier(groupKey);
}

QString SM_Settings::getFullKey(QString key)
{
    return SM_CF::unite({groupKey, key});
}

void SM_Settings::clear()
{
    settingsMap.clear();
}

void SM_Settings::setValue(QString key, QVariant value)
{
    settingsMap.insert(key, value);
}

QVariant SM_Settings::value(QString key, QVariant defaultValue)
{
    return settingsMap.value(key, defaultValue);
}

QStringList SM_Settings::getAllKeys()
{
    return settingsMap.keys();
}
