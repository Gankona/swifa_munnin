#ifndef SM_ACCOUNTINFO_H
#define SM_ACCOUNTINFO_H

#include <QtCore/QDataStream>
#include <QtCore/QDateTime>
#include <QtCore/QObject>
#include <QtCore/QString>

#include "sm_staticcorefunction.h"
#include "svifamunnincore_global.h"

class SMCORESHARED_EXPORT SM_AccountInfo
{
public:
    SM_AccountInfo() = default;
    SM_AccountInfo(const QByteArray &b);
    SM_AccountInfo(const SM_AccountInfo &a);
    SM_AccountInfo(QString login,
                   QString firstName,
                   QString lastName,
                   QString email,
                   QString telephon = "",
                   QString country = "");

    QByteArray toByteArray();

    static SM_AccountInfo getAccountFromTier(QString str);
    static QString getTierString(SM_AccountInfo account);

    QString login;
    QString firstName;
    QString lastName;
    QString email;
    QString telephon;
    QString country;

    friend QDataStream& operator << (QDataStream &d, SM_AccountInfo &a){
        return d << a.country << a.email << a.firstName << a.lastName
                 << a.login << a.telephon;
    }

    friend QDataStream& operator >> (QDataStream &d, SM_AccountInfo &a)
    {
        return d >> a.country >> a.email >> a.firstName >> a.lastName
                 >> a.login >> a.telephon;
    }
};

#endif // SM_ACCOUNTINFO_H
