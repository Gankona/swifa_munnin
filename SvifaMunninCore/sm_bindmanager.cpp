#include "sm_bindmanager.h"

SM_BindManager::SM_BindManager(QObject *parent)
    : QObject(parent)
{}

void SM_BindManager::setLog(SM::TypeMsg type,
                            QString sender,
                            QString message,
                            QString sendKey,
                            QString deliveryKey)
{
    qDebug() << QDateTime::currentDateTime().toString("hh:mm:ss:zzz dd/MM/yyyy dddd");
    qDebug() << QString().leftJustified(5, ' ') + QString("type:").leftJustified(15, ' ') << SM::typeMsgToString(type);
    qDebug() << QString().leftJustified(5, ' ') + QString("sender:").leftJustified(15, ' ') << sender;
    qDebug() << QString().leftJustified(5, ' ') + QString("message:").leftJustified(15, ' ') << message;
    qDebug() << QString().leftJustified(5, ' ') + QString("send key:").leftJustified(15, ' ') << sendKey;
    qDebug() << QString().leftJustified(5, ' ') + QString("delivery key:").leftJustified(15, ' ') << deliveryKey;
    qDebug() << QString().leftJustified(100, '_');

    emit logUpdate(type, message, sendKey, deliveryKey, sender);
}

void SM_BindManager::setMessage(SM::TypeMsg type,
                                QString sender,
                                QString message)
{
    if (! (SM::TypeMsg::critical == type
        || SM::TypeMsg::warning  == type
        || SM::TypeMsg::debug    == type
        || SM::TypeMsg::info     == type))
        return;
    setLog(type, sender, message);
}

void SM_BindManager::setResult(bool resultValue,
                               QString deliveryKey,
                               QString sender,
                               QString message)
{
    if (resultValue)
        setLog(SM::TypeMsg::resultCorrect, sender, message, deliveryKey);
    else
        setLog(SM::TypeMsg::resultIncorrect, sender, message, deliveryKey);
    emit result(resultValue, deliveryKey, message);
}

void SM_BindManager::setLink(QString link,
                             QString sender)
{
    qDebug() << link << SM_CF::getTier(link);
    setLog(SM::TypeMsg::appLink, sender, link);
    for (int i = 0; i < SM_CF::getTierLenght(link); i++)
        emit openLink(SM_CF::getTierInterval(link, i), link);
}

void SM_BindManager::setSignal(QString sendKey,
                               QString sender,
                               QString message,
                               QString deliveryKey)
{
    setLog(SM::TypeMsg::signalInternal, sender, message, sendKey, deliveryKey);
    emit signalInternal(sendKey, message, deliveryKey);
}

void SM_BindManager::setGlobalSignal(QString sendKey,
                                     QString sender,
                                     QString message,
                                     QString deliveryKey)
{
    setLog(SM::TypeMsg::signalGlobal, sender, message, sendKey, deliveryKey);
    emit signalInternal(sendKey, message, deliveryKey);
    emit signalGlobal(sendKey, message, deliveryKey);
}

void SM_BindManager::setError(QString deliveryKey,
                              QString sender,
                              QString message)
{
    setLog(SM::TypeMsg::resultIncorrect, sender, message, deliveryKey);
    emit result(false, deliveryKey, message);
}

void SM_BindManager::setAright(QString deliveryKey,
                               QString sender,
                               QString message)
{
    setLog(SM::TypeMsg::resultCorrect, sender, message, deliveryKey);
    emit result(true, deliveryKey, message);
}
