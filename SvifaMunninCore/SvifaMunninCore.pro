#-------------------------------------------------
#
# Project created by QtCreator 2016-07-20T17:11:42
#
#-------------------------------------------------

include($$PWD/../path.pri)

QT -= gui

CONFIG += c++14

TARGET = SvifaMunninCore
TEMPLATE = lib
DESTDIR = $$BIN_PATH/lib
#CONFIG += build_all

DEFINES += SVIFAMUNNINCORE_LIBRARY

HEADERS += \
    svifamunnincore_global.h \
    sm_encryption.h \
    sm_namespace.h \
    sm_accountinfo.h \
    sm_staticcorefunction.h \
    sm_pushbuttoneventeater.h \
    sm_bindmanager.h \
    sm_settings.h \
    ApplicationBind \
    ApplicationSettings

SOURCES += \
    sm_encryption.cpp \
    sm_accountinfo.cpp \
    sm_staticcorefunction.cpp \
    sm_pushbuttoneventeater.cpp \
    sm_bindmanager.cpp \
    sm_settings.cpp

unix {
    target.path = /usr/lib
    INSTALLS += target
}

RESOURCES += \
    sm_c_res.qrc

DISTFILES +=
