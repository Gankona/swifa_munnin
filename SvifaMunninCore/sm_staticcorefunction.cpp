#include "sm_staticcorefunction.h"

QString SM_CF::getTier(QString text, int tier)
{
    int countColons(0);
    QString ret("");
    for (int i = 0; i < text.length(); i++){
        if (text[i] == ':'){
            countColons++;
            if (countColons > tier)
                break;
            i++;
        }
        else if (tier == countColons)
            ret += text[i];
    }
    return ret;
}

QString SM_CF::getKeyValue(int i, Qt::KeyboardModifiers m)
{
    if (m & Qt::AltModifier
            && m & Qt::ControlModifier
            && m & Qt::ShiftModifier)
        return "Ctrl+Alt+Shift+" + getKeyValue(i);
    if (m & Qt::AltModifier
            && m & Qt::ControlModifier)
        return "Ctrl+Alt+" + getKeyValue(i);
    if (m & (Qt::ShiftModifier
            && m & Qt::AltModifier))
        return "Alt+Shift+" + getKeyValue(i);
    if (m & Qt::ControlModifier
            && m & Qt::ShiftModifier)
        return "Ctrl+Shift+" + getKeyValue(i);
    if (m & Qt::ShiftModifier)
        return "Shift+" + getKeyValue(i);
    if (m & Qt::ControlModifier)
        return "Ctrl+" + getKeyValue(i);
    if (m & Qt::AltModifier)
        return "Alt+" + getKeyValue(i);
    return getKeyValue(i);
}

QString SM_CF::getKeyValue(int i)
{
    switch (i){
    case Qt::Key_0: return "0";
    case Qt::Key_1: return "1";
    case Qt::Key_2: return "2";
    case Qt::Key_3: return "3";
    case Qt::Key_4: return "4";
    case Qt::Key_5: return "5";
    case Qt::Key_6: return "6";
    case Qt::Key_7: return "7";
    case Qt::Key_8: return "8";
    case Qt::Key_9: return "9";
    case Qt::Key_A: return "A";
    case Qt::Key_B: return "B";
    case Qt::Key_C: return "C";
    case Qt::Key_D: return "D";
    case Qt::Key_E: return "E";
    case Qt::Key_F: return "F";
    case Qt::Key_G: return "G";
    case Qt::Key_H: return "H";
    case Qt::Key_I: return "I";
    case Qt::Key_K: return "K";
    case Qt::Key_L: return "L";
    case Qt::Key_M: return "M";
    case Qt::Key_N: return "N";
    case Qt::Key_O: return "O";
    case Qt::Key_P: return "P";
    case Qt::Key_Q: return "Q";
    case Qt::Key_R: return "R";
    case Qt::Key_S: return "S";
    case Qt::Key_T: return "T";
    case Qt::Key_U: return "U";
    case Qt::Key_V: return "V";
    case Qt::Key_W: return "W";
    case Qt::Key_X: return "X";
    case Qt::Key_Y: return "Y";
    case Qt::Key_Z: return "Z";
    case Qt::Key_Up: return "Up";
    case Qt::Key_Down: return "Down";
    case Qt::Key_Left: return "Left";
    case Qt::Key_Right: return "Right";
    case Qt::Key_F1: return "F1";
    case Qt::Key_F2: return "F2";
    case Qt::Key_F3: return "F3";
    case Qt::Key_F4: return "F4";
    case Qt::Key_F5: return "F5";
    case Qt::Key_F6: return "F6";
    case Qt::Key_F7: return "F7";
    case Qt::Key_F8: return "F8";
    case Qt::Key_F9: return "F9";
    case Qt::Key_F10: return "F10";
    case Qt::Key_F11: return "F11";
    case Qt::Key_F12: return "F12";
    case Qt::Key_Backslash: return "\\";
    case Qt::Key_Backspace: return "Backspace";
    case Qt::Key_Comma: return ",";
    case Qt::Key_Delete: return "Del";
    case Qt::Key_End: return "End";
    case Qt::Key_Enter: return "Enter";
    case Qt::Key_Escape: return "Esc";
    case Qt::Key_Home: return "Home";
    case Qt::Key_Minus: return "-";
    case Qt::Key_PageDown: return "PgDn";
    case Qt::Key_PageUp: return "PgUp";
    case Qt::Key_Plus: return "+";
    case Qt::Key_Return: return "Return";
    case Qt::Key_Slash: return "/";
    case Qt::Key_Space: return "' '";
    case Qt::Key_VolumeDown: return "Vdown";
    case Qt::Key_VolumeMute: return "Vup";
    case Qt::Key_VolumeUp: return "Mute";
    case Qt::Key_Period: return ".";
    case Qt::Key_Apostrophe: return "\"";
    case Qt::Key_QuoteLeft: return "~";
    default:;
    }
    return "";
}

QString SM_CF::replaceTier(QString text, QString reText, int tier)
{
    int countColons(0);
    QString ret("");
    bool isInsert(false);
    for (int i = 0; i < text.length(); i++){
        if (text[i] == ':'){
            countColons++;
            ret += "::";
            i++;
        }
        else if (countColons == tier){
            if (! isInsert){
                ret += reText;
                isInsert = true;
            }
        }
        else
            ret += text[i];
    }
    return ret;
}

QString SM_CF::setWrapping(QString text, int lenght)
{
    for (int i = 0; i < text.length()/lenght; i++)
        text.insert((i + 1) * lenght, '\n');
    return text;
}

QString SM_CF::getRandomKey(int lenght)
{
    QString ret("");
    for (int i = 0; i < lenght; i++){
        int t = rand()%62;
        if (t >= 36)
            ret += char(t + 61);
        else if (t < 10)
            ret += char(t + 48);
        else
            ret += char(t + 55);
    }
    return ret;
}

QString SM_CF::unite(std::initializer_list<QString> list)
{
    QString ret("");
    for (auto i = list.begin(); i != list.end(); i++){
        ret += *i;
        if (i+1 != list.end())
            ret += "::";
    }
    return ret;
}

QString SM_CF::boolToString(bool v)
{
    if (v)
        return "true";
    return "false";
}

QString SM_CF::getTierInterval(QString text, int second)
{
    return SM_CF::getTierInterval(text, 0, second);
}

QString SM_CF::removeFirstTier(QString text)
{
    QString ret("");
    for (int i = 1; i < SM_CF::getTierLenght(text); i++){
        if (i == 1)
            ret = SM_CF::getTier(text, i);
        else
            ret = SM_CF::unite({ret, SM_CF::getTier(text, i)});
    }
    return ret;
}

QString SM_CF::getTierInterval(QString text, int first, int second)
{
    QString ret("");
    for (int i = first; i <= second; i++){
        QString tmp = SM_CF::getTier(text, i);
        if (tmp != ""){
            if (ret == "")
                ret = tmp;
            else
                ret = SM_CF::unite({ret, tmp});
        }
    }
    return ret;
}

QStringList SM_CF::getTierList(QString text)
{
    QStringList ret;
    QString buffer("");
    for (int i = 0; i < text.length(); i++){
        if (text[i] == ':'){
            i++;
            ret.push_back(buffer);
            buffer = "";
        }
        else
            buffer += text[i];
    }
    if (buffer != "")
        ret.push_back(buffer);
    return ret;
}

int SM_CF::getTierLenght(QString text)
{
    int ret(0);
    if (text != "")
        ret++;
    else
        return 0;
    for (int i = 0; i < text.length(); i++){
        if (text[i] == ':'){
            i++;
            ret++;
        }
    }
    return ret;
}
