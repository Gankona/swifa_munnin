#ifndef SM_ENCRYPTION_H
#define SM_ENCRYPTION_H

#include <QtCore/QBitArray>
#include <QtCore/QByteArray>
#include <QtCore/QString>

#include "svifamunnincore_global.h"

class SMCORESHARED_EXPORT SM_Encryption
{
private:
    static int getSizeCrypt(QString key);
    static int getKeyEnterNumber(QString key, int sizeBlock);
    static QBitArray getVectorCrypt(QString key);
    static QBitArray roundVector(int size, int key, bool isReverse);
    static QBitArray unionVector(QBitArray first, QBitArray second);

public:
    static QByteArray encryptFile(QByteArray array, QString key);
    static QByteArray decryptFile(QByteArray array, QString key);

    static QBitArray  toBitArray (QByteArray bytes);
    static QByteArray toByteArray(QBitArray  bits);
};

#endif // SM_ENCRYPTION_H
