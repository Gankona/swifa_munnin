#ifndef SM_NAMESPACE_H
#define SM_NAMESPACE_H

#include <QtCore/QDataStream>
#include <QtCore/QDebug>
#include <QtCore/QMetaEnum>
#include <QtCore/QMetaObject>
#include <QtCore/QObject>

class SM : public QObject
{
public:
//--TypeMsg-start--------------------------------
    enum class TypeMsg {
        debug = 0x000,
        warning = 0x001,
        critical = 0x002,
        info = 0x003,

        signalInternal = 0x004,
        signalGlobal = 0x005,

        resultCorrect = 0x006,
        resultIncorrect = 0x007,

        appLink = 0x008,
    };
    Q_ENUM(TypeMsg)

    friend QDebug operator<<(QDebug dbg, TypeMsg enumValue)
    {
        const QMetaObject *mo = qt_getEnumMetaObject(enumValue);
        int enumIdx = mo->indexOfEnumerator(qt_getEnumName(enumValue));
        return dbg << mo->enumerator(enumIdx).valueToKey(int(enumValue));    
    }

    friend QDataStream& operator << (QDataStream &s, TypeMsg &t){
        return s << int(t);
    }

    friend QDataStream& operator >> (QDataStream &s, TypeMsg &t){
        int i;
        s >> i;
        t = SM::TypeMsg(i);
        return s;
    }
    static QString typeMsgToString(TypeMsg t){
        switch (t){
        case SM::TypeMsg::debug:
            return "debug";
        case SM::TypeMsg::warning:
            return "warning";
        case SM::TypeMsg::critical:
            return "critical";
        case SM::TypeMsg::info:
            return "info";
        case SM::TypeMsg::resultCorrect:
            return "result correct";
        case SM::TypeMsg::resultIncorrect:
            return "result incorrect";
        case SM::TypeMsg::signalInternal:
            return "signal internal";
        case SM::TypeMsg::signalGlobal:
            return "signal global";
        case SM::TypeMsg::appLink:
            return "app link";
        default:;
        }
        return "";
    }

//--TypeMsg-end----------------------------------


//--Key-start------------------------------------
    enum class Key {
        registration = 0x100,
        authorization = 0x101,
        editAccount  = 0x102,
        changePassword = 0x103,
        fileList = 0x104,
        sendFile = 0x105,
        setAccountInfo = 0x106,
        getAccountInfo = 0x107,
        server_ping = 0x108,
        serverNotFound = 0x109,
        testLogin = 0x10A,
        testEmail = 0x10B,
        recoverPassword = 0x10C,
        getSettings = 0x10D,
        setSettings = 0x10E,
    };
    Q_ENUM(Key)

    friend QDebug operator<<(QDebug dbg, Key enumValue)
    {
        const QMetaObject *mo = qt_getEnumMetaObject(enumValue);
        int enumIdx = mo->indexOfEnumerator(qt_getEnumName(enumValue));
        return dbg << mo->enumerator(enumIdx).valueToKey(int(enumValue));
    }

    friend QDataStream& operator << (QDataStream &s, Key &t){
        return s << static_cast<int>(t);
    }

    friend QDataStream& operator >> (QDataStream &s, Key &t){
        int i;
        s >> i;
        t = static_cast<SM::Key>(i);
        return s;
    }
//--Key-end--------------------------------------


//--Msg-start------------------------------------
    enum class Msg {
        //file
        fileNotFound = 0x200,
        fileSuccessfullCreate = 0x201,
        fileSuccessfullDelete = 0x202,
        fileSuccessfullEdit = 0x203,
        fileSuccessfullSend = 0x204,
        fileHasOlder = 0x205,
        fileIsNewest = 0x206,

        //directory
        directoryNotExist = 0x210,
        directoryCreate = 0x211,
        directoryDelete = 0x212,
        directoryDoNotOpen = 0x213,

        //registration-avtorization
        passwordIncorrect = 0x220,
        passwordSuccessfullChange = 0x221,
        successfullRegistration = 0x222,
        successfullAuthtorization = 0x223,
        successfullLogout = 0x224,
        emailNotPresent = 0x225,
        loginNotPresent = 0x226,
        loginPresent = 0x227,
        loginFree = 0x228,
        emailPresent = 0x229,
        emailFree = 0x22A,
        accountNotPresent = 0x22B,
        passwordDoNotChange = 0x22C,
        accountSuccessfullChange = 0x22D,
        accountDoNotChange = 0x22E,

        //unknown
        unknown = 0xFFF,
    };
    Q_ENUM(Msg)

    friend QDebug operator<<(QDebug dbg, Msg enumValue)
    {
        const QMetaObject *mo = qt_getEnumMetaObject(enumValue);
        int enumIdx = mo->indexOfEnumerator(qt_getEnumName(enumValue));
        return dbg << mo->enumerator(enumIdx).valueToKey(int(enumValue));
    }

    friend QDataStream& operator << (QDataStream &s, Msg &t){
        return s << static_cast<int>(t);
    }

    friend QDataStream& operator >> (QDataStream &s, Msg &t){
        int i;
        s >> i;
        t = static_cast<SM::Msg>(i);
        return s;
    }

    static QString getMsgToString(SM::Msg enumValue){
        switch (static_cast<int>(enumValue)){
        case static_cast<int>(Msg::accountDoNotChange):
            return "account do not change";
        case static_cast<int>(Msg::accountNotPresent):
            return "account not present";
        case static_cast<int>(Msg::accountSuccessfullChange):
            return "account successfull change";
        case static_cast<int>(Msg::directoryCreate):
            return "directory create";
        case static_cast<int>(Msg::directoryDelete):
            return "directory delete";
        case static_cast<int>(Msg::directoryDoNotOpen):
            return "directory do not open";
        case static_cast<int>(Msg::directoryNotExist):
            return "directory not exist";
        case static_cast<int>(Msg::emailFree):
            return "email free";
        case static_cast<int>(Msg::emailNotPresent):
            return "email not present";
        case static_cast<int>(Msg::emailPresent):
            return "email present";
        case static_cast<int>(Msg::fileHasOlder):
            return "file has older";
        case static_cast<int>(Msg::fileIsNewest):
            return "file is newest";
        case static_cast<int>(Msg::fileNotFound):
            return "file not found";
        case static_cast<int>(Msg::fileSuccessfullCreate):
            return "file successfull create";
        case static_cast<int>(Msg::fileSuccessfullDelete):
            return "file successfull delete";
        case static_cast<int>(Msg::fileSuccessfullEdit):
            return "file successfull edit";
        case static_cast<int>(Msg::fileSuccessfullSend):
            return "file successfull send";
        case static_cast<int>(Msg::loginFree):
            return "login free";
        case static_cast<int>(Msg::loginNotPresent):
            return "login not present";
        case static_cast<int>(Msg::loginPresent):
            return "login present";
        case static_cast<int>(Msg::passwordDoNotChange):
            return "password do not change";
        case static_cast<int>(Msg::passwordIncorrect):
            return "password incorrect";
        case static_cast<int>(Msg::passwordSuccessfullChange):
            return "password succesfull change";
        case static_cast<int>(Msg::successfullAuthtorization):
            return "successfull authorizate";
        case static_cast<int>(Msg::successfullLogout):
            return "successfull logout";
        case static_cast<int>(Msg::successfullRegistration):
            return "successfull registrate";
        case static_cast<int>(Msg::unknown):
            return "unknown";
        }
        return "";
//        const QMetaObject *mo = qt_getEnumMetaObject(enumValue);
//        int enumIdx = mo->indexOfEnumerator(qt_getEnumName(enumValue));
//        return QString(mo->enumerator(enumIdx).valueToKey(int(enumValue)));
    }
//--Msg-end--------------------------------------


//--Module-start---------------------------------
    enum class Module {
        moduleNotUse = 0x300,
        moduleBackgroundUse = 0x301,
        moduleSimplifiedUse = 0x302,
        moduleEntireUse = 0x303,

        moduleNotPresent = 0x304,
        moduleSuccessfullLoad = 0x305,
        moduleErrorLoad = 0x306,
        moduleNotLoad = 0x307,
    };
    Q_ENUM(SM::Module)

    friend QDebug operator<<(QDebug dbg, Module enumValue)
    {
        const QMetaObject *mo = qt_getEnumMetaObject(enumValue);
        int enumIdx = mo->indexOfEnumerator(qt_getEnumName(enumValue));
        return dbg << mo->enumerator(enumIdx).valueToKey(int(enumValue));
    }
//--Module-end-----------------------------------


//--Anchors-start--------------------------------
    enum class Anchors {
        anchorsNull = 0x400,
        anchorsLeft = 0x401,
        anchorsRight = 0x402,
        anchorsTop = 0x403,
        anchorsBottom = 0x404,
        anchorsLeftTop = 0x405,
        anchorsLeftBottom = 0x406,
        anchorsRightBottom = 0x407,
        anchorsRightTop = 0x408,
        anchorsNotTop = 0x409,
        anchorsNotRight = 0x40A,
        anchorsNotBottom = 0x40B,
        anchorsNotLeft = 0x40C,
        anchorsLeftRight = 0x40D,
        anchorsTopBottom = 0x40E,
        anchorsPositionFixed = 0x40F,
    };
    Q_ENUM(Anchors)

    static bool isAnchorsCorner(SM::Anchors a){
        return int(a) >= 0x405 && int(a) <= 0x40C;
    }

    static bool isAnchorsLeft(SM::Anchors a){
        if (a == SM::Anchors::anchorsLeft)
            return true;
        if (a == SM::Anchors::anchorsLeftBottom)
            return true;
        if (a == SM::Anchors::anchorsLeftTop)
            return true;
        if (a == SM::Anchors::anchorsNotBottom)
            return true;
        if (a == SM::Anchors::anchorsNotRight)
            return true;
        if (a == SM::Anchors::anchorsNotTop)
            return true;
        if (a == SM::Anchors::anchorsLeftRight)
            return true;
        return false;
    }

    static bool isAnchorsRight(SM::Anchors a){
        if (a == SM::Anchors::anchorsRight)
            return true;
        if (a == SM::Anchors::anchorsRightBottom)
            return true;
        if (a == SM::Anchors::anchorsRightTop)
            return true;
        if (a == SM::Anchors::anchorsNotBottom)
            return true;
        if (a == SM::Anchors::anchorsNotLeft)
            return true;
        if (a == SM::Anchors::anchorsNotTop)
            return true;
        if (a == SM::Anchors::anchorsLeftRight)
            return true;
        return false;
    }

    static bool isAnchorsTop(SM::Anchors a){
        if (a == SM::Anchors::anchorsTop)
            return true;
        if (a == SM::Anchors::anchorsRightTop)
            return true;
        if (a == SM::Anchors::anchorsLeftTop)
            return true;
        if (a == SM::Anchors::anchorsNotBottom)
            return true;
        if (a == SM::Anchors::anchorsNotRight)
            return true;
        if (a == SM::Anchors::anchorsNotLeft)
            return true;
        if (a == SM::Anchors::anchorsTopBottom)
            return true;
        return false;
    }

    static bool isAnchorsBottom(SM::Anchors a){
        if (a == SM::Anchors::anchorsBottom)
            return true;
        if (a == SM::Anchors::anchorsLeftBottom)
            return true;
        if (a == SM::Anchors::anchorsRightBottom)
            return true;
        if (a == SM::Anchors::anchorsNotLeft)
            return true;
        if (a == SM::Anchors::anchorsNotRight)
            return true;
        if (a == SM::Anchors::anchorsNotTop)
            return true;
        if (a == SM::Anchors::anchorsTopBottom)
            return true;
        return false;
    }

    static bool isAnchorsOpposite(SM::Anchors a){
        if (a == SM::Anchors::anchorsLeftRight)
            return true;
        if (a == SM::Anchors::anchorsTopBottom)
            return true;
        return false;
    }

    static bool isAnchorsOneWay(SM::Anchors a){
        if (a == SM::Anchors::anchorsBottom)
            return true;
        if (a == SM::Anchors::anchorsLeft)
            return true;
        if (a == SM::Anchors::anchorsRight)
            return true;
        if (a == SM::Anchors::anchorsTop)
            return true;
        return false;
    }

    static bool isAnchorsNoSides(SM::Anchors a){
        if (a == SM::Anchors::anchorsNull)
            return true;
        if (a == SM::Anchors::anchorsPositionFixed)
            return true;
        return false;
    }

    static bool isAnchorsHorizontal(SM::Anchors a)
    {
        if (a == SM::Anchors::anchorsBottom)
            return true;
        if (a == SM::Anchors::anchorsTop)
            return true;
        return false;
    }

    static bool isAnchorsVertical(SM::Anchors a)
    {
        if (a == SM::Anchors::anchorsLeft)
            return true;
        if (a == SM::Anchors::anchorsRight)
            return true;
        return false;
    }

//--Anchors-end----------------------------------
};

#endif // SM_NAMESPACE_H
