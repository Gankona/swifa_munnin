#ifndef SM_PUSHBUTTONEVENTEATER_H
#define SM_PUSHBUTTONEVENTEATER_H

#include <QObject>
#include <QtCore/QEvent>

class SM_PushButtonEventEater : public QObject
{
    Q_OBJECT
protected:
    bool eventFilter(QObject*obj, QEvent *event);

public:
    explicit SM_PushButtonEventEater(QObject *parent = 0);

signals:
    void leaveWidget();
    void enterWidget();
};

#endif // SM_PUSHBUTTONEVENTEATER_H
